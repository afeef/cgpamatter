﻿using System;

namespace Ideaworx.Cgpamatter.Common
{
	/// <summary>
	/// Represent a generic error on the authentication provider
	/// </summary>
	public class AuthenticationProviderException : Exception
	{
		/// <summary>
		/// Represent a generic error on the authentication provider
		/// </summary>
		public AuthenticationProviderException(string message)
			: base(message)
		{

		}
	}
}
