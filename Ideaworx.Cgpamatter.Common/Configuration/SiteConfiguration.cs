﻿using System;
using System.Configuration;
using System.IO;

namespace Ideaworx.Cgpamatter.Common.Configuration
{
	public class SiteConfiguration : ConfigurationSection
	{
		#region Current
		public static SiteConfiguration Current
		{
			get
			{
				var config = (SiteConfiguration)ConfigurationManager.GetSection("site");
				if (config == null)
				{
					throw new ConfigurationErrorsException("Siteconfiguration not set.");
				}
				return config;
			}
		}
		#endregion

		

		[ConfigurationProperty("dateFormat", IsRequired = false)]
		public string DateFormat
		{
			get
			{
				return (string)this["dateFormat"];
			}
			set
			{
				this["dateFormat"] = value;
			}
		}

		

        [ConfigurationProperty("timeZoneOffset", IsRequired = false)]
        private decimal? TimeZoneOffsetHours
        {
            get
            {
                return (decimal?)this["timeZoneOffset"];
            }
            set
            {
                this["timeZoneOffset"] = value;
            }
        }

        public TimeSpan? TimeZoneOffset
        {
            get
            {
                if (this.TimeZoneOffsetHours == null)
                {
                    return null;
                }
                return new TimeSpan(0, Convert.ToInt32(this.TimeZoneOffsetHours * 60), 0);
            }
        }

		/// <summary>
		/// Combines the current application configuration path with the fileName given
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public string CombinePath(string fileName)
		{
			return Path.Combine(Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile), fileName);
		}

		protected override void PostDeserialize()
		{
			ProcessMissingElements(this);
			base.PostDeserialize();
		}

		private void ProcessMissingElements(ConfigurationElement element)
		{

			foreach (PropertyInformation propertyInformation in element.ElementInformation.Properties)
			{

				var complexProperty = propertyInformation.Value as ConfigurationElement;

				if (complexProperty != null)
				{
					if (propertyInformation.IsRequired && !complexProperty.ElementInformation.IsPresent)
					{
						throw new ConfigurationErrorsException("Configuration element: [" + propertyInformation.Name + "] is required but not present");
					}
					else
					{
						ProcessMissingElements(complexProperty);
					}
				}
			}
		}
	}
}
