﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text.RegularExpressions;

namespace Ideaworx.Cgpamatter.Common
{
    public static class DataHelper
    {

        #region Private Fields

        private const string MinSqlDateValue = "01/01/1900";

        #endregion

        #region Helper Methods

        public static DbParameter CreateParameter(DbCommand command, String name, DbType type, Object value, ParameterDirection Direction)
        {

            var para = command.CreateParameter();
            para.ParameterName = "@" + name;
            para.DbType = type;
            para.Value = value;
            para.Direction = Direction;
            return para;
        }

        public static DateTime ToDateTime(object value)
        {
            var dateValue = DateTime.MinValue;
            if ((value != null) && (value != DBNull.Value))
            {
                if ((DateTime)value > DateTime.Parse(DataHelper.MinSqlDateValue))
                {
                    dateValue = (DateTime)value;
                }
            }
            return dateValue;
        }

        //public static Status ToStatus(object value)
        //{
        //    //if (value != null && !Convert.IsDBNull(value))
        //    //{                
        //    return (Status)Enum.Parse(typeof(Status), value.ToString());
        //    //}
        //    //return null;
        //}

        public static DateTime? ToNullableDateTime(object value)
        {
            DateTime? dateTimeValue = null;
            if (value != null && !Convert.IsDBNull(value))
            {

                DateTime dbDateTimeValue;
                if (DateTime.TryParse(value.ToString(), out dbDateTimeValue))
                {
                    dateTimeValue = dbDateTimeValue;
                }
            }
            return dateTimeValue;
        }

        public static int ToInteger(object value)
        {
            var integerValue = 0;
            if (value != null && !Convert.IsDBNull(value))
            {
                int.TryParse(value.ToString(), out integerValue);
            }
            return integerValue;
        }

        public static int? ToNullableInteger(object value)
        {
            int? integerValue = null;
            if (value != null && !Convert.IsDBNull(value))
            {

                var parseIntegerValue = 0;
                if (int.TryParse(value.ToString(), out parseIntegerValue))
                {
                    integerValue = parseIntegerValue;
                }
            }
            return integerValue;
        }

        public static decimal ToDecimal(object value)
        {
            decimal decimalValue = 0;
            if (value != null && !Convert.IsDBNull(value))
            {
                decimal.TryParse(value.ToString(), out decimalValue);
            }
            return decimalValue;
        }

        public static decimal? ToNullableDecimal(object value)
        {
            decimal? decimalValue = null;
            if (value != null && !Convert.IsDBNull(value))
            {

                decimal parseDecimalValue = 0;
                if (decimal.TryParse(value.ToString(), out parseDecimalValue))
                {
                    decimalValue = parseDecimalValue;
                }
            }
            return decimalValue;
        }

        public static double ToDouble(object value)
        {
            double doubleValue = 0;
            if (value != null && !Convert.IsDBNull(value))
            {
                double.TryParse(value.ToString(), out doubleValue);
            }
            return doubleValue;
        }

        public static double? ToNullableDouble(object value)
        {
            double? doubleValue = null;
            if (value != null && !Convert.IsDBNull(value))
            {
                double parseDoubleValue = 0;
                if (double.TryParse(value.ToString(), out parseDoubleValue))
                {
                    doubleValue = parseDoubleValue;
                }
            }

            return doubleValue;
        }

        public static long ToLong(object value)
        {
            long longValue = 0;
            if (value != null && !Convert.IsDBNull(value))
            {
                long.TryParse(value.ToString(), out longValue);
            }
            return longValue;
        }

        public static long? ToNullableLong(object value)
        {
            long? longValue = null;
            if (value != null && !Convert.IsDBNull(value))
            {
                long parseLongValue = 0;
                if (long.TryParse(value.ToString(), out parseLongValue))
                {
                    longValue = parseLongValue;
                }
            }

            return longValue;
        }

        public static Guid ToGuid(object value)
        {
            var guidValue = Guid.Empty;
            if (value != null && !Convert.IsDBNull(value))
            {
                try
                {
                    guidValue = new Guid(value.ToString());
                }
                catch
                {
                    // really do nothing, because we want to return a value for the guid = Guid.Empty;
                }
            }
            return guidValue;
        }

        public static Guid? ToNullableGuid(object value)
        {
            Guid? guidValue = null;
            if (value != null && !Convert.IsDBNull(value))
            {
                try
                {
                    guidValue = new Guid(value.ToString());
                }
                catch
                {
                    // really do nothing, because we want to return a value for the guid = null;
                }
            }
            return guidValue;
        }

        public static string ToString(object value)
        {
            var stringValue = string.Empty;
            if (value != null && !Convert.IsDBNull(value))
            {
                stringValue = value.ToString().Trim();
            }
            return stringValue;
        }

        public static bool ToBoolean(object value)
        {
            var bReturn = false;
            if (value != null && value != DBNull.Value)
            {
                bReturn = Convert.ToBoolean(value);
            }
            return bReturn;
        }

        public static bool? ToNullableBoolean(object value)
        {
            bool? bReturn = null;
            if (value != null && value != DBNull.Value)
            {
                bReturn = (bool)value;
            }

            return bReturn;
        }

        public static T ToEnumValue<T>(object databaseValue) where T : struct
        {
            var enumValue = default(T);

            if (databaseValue != null && databaseValue.ToString().Length > 0)
            {
                var parsedValue = Enum.Parse(typeof(T), databaseValue.ToString());
                if (parsedValue != null)
                {
                    enumValue = (T)parsedValue;
                }
            }

            return enumValue;
        }

        public static byte[] ToByteArrayValue(object value)
        {
            byte[] arrayValue = null;
            if (value != null && value != DBNull.Value)
            {
                arrayValue = (byte[])value;
            }
            return arrayValue;
        }

        public static string ToBase64String(object value)
        {
            return Convert.ToBase64String((byte[])value);
        }

        //public static string EntityListToDelimited<T>(IList<T> entities) where T : IEntity
        //{
        //    StringBuilder builder = new StringBuilder(20);
        //    if (entities != null)
        //    {
        //        for (int i = 0; i < entities.Count; i++)
        //        {
        //            if (i > 0)
        //            {
        //                builder.Append(",");
        //            }
        //            builder.Append(entities[i].Key.ToString());
        //        }
        //    }
        //    return builder.ToString();
        //}

        public static bool ReaderContainsColumnName(DataTable schemaTable, string columnName)
        {
            return schemaTable.Rows.Cast<DataRow>().Any(row => row["ColumnName"].ToString() == columnName);
        }

        public static object ToSqlValue(object value)
        {
            if (value == null)
                return "NULL";

            return value is Guid ? ToSqlValue((Guid)value) : value;
        }

        public static object ToSqlValue(string value)
        {
            return value != null ? string.Format("N'{0}'", value.Replace("'", "''")) : "NULL";
        }

        public static object ToSqlValue(DateTime value)
        {
            return string.Format("'{0}'", value == DateTime.MinValue ? DataHelper.MinSqlDateValue : value.ToString());
        }

        public static object ToSqlValue(DateTime? value)
        {
            return value.HasValue ? DataHelper.ToSqlValue(value.Value) : "NULL";
        }

        public static object ToSqlValue(bool value)
        {
            return value ? "1" : "0";
        }

        public static object ToSqlValue(bool? value)
        {
            return value != null ? string.Format("'{0}'", value) : "NULL";
        }

        public static object ToSqlValue(Guid value)
        {
            return string.Format("'{0}'", value);
        }

        public static object ToSqlValue(Enum value)
        {
            return value != null ? DataHelper.ToSqlValue(value.ToString()) : "NULL";
        }

        #endregion

        #region String helpers

        public static string ToUrlFragment(string value, int maxLength)
        {
            if (value == null)
            {
                return null;
            }
            value = value.ToLowerInvariant();
            value = Regex.Replace(value, @"[^a-z- ]+", "");
            value = Regex.Replace(value, @" ", "-");
            value = Regex.Replace(value, @"-+", "-");
            value = Regex.Replace(value, @"^-+|-+$", "");
            if (value.Length > maxLength)
            {
                value = value.Substring(0, maxLength);
            }
            return value;
        }

        /// <summary>
        /// Crops a given text
        /// </summary>
        /// <param name="value">the text to summarize</param>
        /// <param name="maxChars">maximum chars allowed</param>
        /// <param name="appendIfCropped">text to be appended if the text is cropped. For example: ...</param>
        /// <returns></returns>
        public static string Summarize(string value, int maxChars, string appendIfCropped)
        {
            if (value == null)
            {
                return null;
            }
            if (value.Length <= maxChars)
            {
                return value;
            }

            value = value.Substring(0, maxChars);
            Match match = Regex.Match(value, @"^.*\b(?=[ \.])", RegexOptions.Singleline);
            if (match.Success)
            {
                value = match.Value;
            }
            if (appendIfCropped != null)
            {
                value += appendIfCropped;
            }

            return value;
        }

        #region Html strings
        /// <summary>
        /// Crops a given html fragment
        /// </summary>
        /// <param name="value">the text to summarize</param>
        /// <param name="maxChars">maximum chars allowed</param>
        /// <param name="appendIfCropped">text to be appended if the text is cropped. For example: "..."</param>
        /// <returns></returns>
        public static string SummarizeHtml(string value, int maxChars, string appendIfCropped)
        {
            return Summarize(RemoveTags(value), maxChars, appendIfCropped);
        }

        public static bool IsHtmlFragment(string value)
        {
            return Regex.IsMatch(value, @"</?(p|div)>");
        }

        /// <summary>
        /// Remove tags from a html string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string RemoveTags(string value)
        {
            if (value != null)
            {
                value = CleanHtmlComments(value);
                value = CleanHtmlBehaviour(value);
                value = Regex.Replace(value, @"</[^>]+?>", " ");
                value = Regex.Replace(value, @"<[^>]+?>", "");
                value = value.Trim();
            }
            return value;
        }

        /// <summary>
        /// Clean script and styles html tags and content
        /// </summary>
        /// <returns></returns>
        public static string CleanHtmlBehaviour(string value)
        {
            value = Regex.Replace(value, "(<style.+?</style>)|(<script.+?</script>)", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            return value;
        }

        /// <summary>
        /// Replace the html commens (also html ifs of msword).
        /// </summary>
        public static string CleanHtmlComments(string value)
        {
            //Remove disallowed html tags.
            value = Regex.Replace(value, "<!--.+?-->", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            return value;
        }

        /// <summary>
        /// Adds rel=nofollow to html anchors
        /// </summary>
        public static string HtmlLinkAddNoFollow(string value)
        {
            return Regex.Replace(value, "<a[^>]+href=\"?'?(?!#[\\w-]+)([^'\">]+)\"?'?[^>]*>(.*?)</a>", "<a href=\"$1\" rel=\"nofollow\" target=\"_blank\">$2</a>", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        #region Sanitize Html
        /// <summary>
        /// sanitize any potentially dangerous tags from the provided raw HTML input using 
        /// a whitelist based approach, leaving the "safe" HTML tags
        /// CODESNIPPET:4100A61A-1711-4366-B0B0-144D1179A937 / http://refactormycode.com/codes/333-sanitize-html
        /// </summary>
        /// <param name="html">Html to sanitize</param>
        /// <param name="whiteListTags">Regex containing the allowed name of the html elements. For example: em|h(2|3|4)|strong|p</param>
        public static string SanitizeHtml(string html, string whiteListTags = "b(lockquote)?|code|d(d|t|l|el)|em|h(1|2|3|4)|i|kbd|li|ol|p(re)?|s(ub|up|trong|trike)?|ul|a|img")
        {
            #region Regex definitions
            Regex tagsRegex = new Regex("<[^>]*(>|$)",
                RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.Compiled);

            Regex cleanupRegex = new Regex("((?<=<\\w+[^>]*)(?!\\shref|\\sclass|\\srel|\\stitle|\\sclass|\\swidth|\\sheight|\\salt|\\ssrc)(\\s[\\w-]+)=[\"']?((?:.(?![\"']?\\s+(?:\\S+)=|[>\"']))+.)[\"']?)|((?<=<p[^>]*)\\sclass=\"MsoNormal\")",
                    RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase | RegexOptions.Compiled);

            Regex whitelistRegex = new Regex("^</?(" + whiteListTags + ")>$|^<(b|h)r\\s?/?>$",
                RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.IgnorePatternWhitespace);

            Regex whitelistAnchorRegex = new Regex(@"
			^<a\s
			href=""(\#\w+|(https?|ftp)://[-a-z0-9+&@#/%?=~_|!:,.;\(\)]+)""
			(
			(\sclass=""([\w-]+)"")|(\stitle=""[^""<>]+"")|
			(\srel=""nofollow""))*
			\s?>$|
			^</a>$",
                RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.IgnorePatternWhitespace);

            Regex whitelistImageRegex = new Regex(@"
			^<img\s
			src=""https?://[-a-z0-9+&@#/%?=~_|!:,.;\(\)]+""
			((\swidth=""\d{1,3}"")|
			(\sheight=""\d{1,3}"")|
			(\salt=""[^""<>]*"")|
			(\stitle=""[^""<>]*""))*
			\s?/?>$",
                RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.IgnorePatternWhitespace);
            #endregion

            if (String.IsNullOrEmpty(html))
                return html;

            //Do a previous cleanup, for not not allowed attributes included comming from word
            html = cleanupRegex.Replace(html, "");

            string tagname;
            Match tag;

            // match every HTML tag in the input
            MatchCollection tags = tagsRegex.Matches(html);
            for (int i = tags.Count - 1; i > -1; i--)
            {
                tag = tags[i];
                tagname = tag.Value.ToLowerInvariant();

                if (!(whitelistRegex.IsMatch(tagname) || whitelistAnchorRegex.IsMatch(tagname) || whitelistImageRegex.IsMatch(tagname)))
                {
                    html = html.Remove(tag.Index, tag.Length);
                    System.Diagnostics.Debug.WriteLine("tag sanitized: " + tagname);
                }
            }

            return html;
        }

        #endregion
        #endregion

        public static string NullToEmpty(string value)
        {
            if (value == null)
            {
                return "";
            }
            return value;
        }

        public static string EmptyToNull(string value)
        {
            if (value != null && value.Trim() == "")
            {
                return null;
            }
            return value;
        }

        public static bool IsNullOrEmpty(string value)
        {
            if (value == null)
            {
                return true;
            }
            else if (value == "" || value.Trim() == "")
            {
                return true;
            }
            return false;
        }

        public static bool IsValidEmailAddress(string value)
        {
            return Regex.IsMatch(value, @"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$");
        }

        /// <summary>
        /// It replaces line breaks with br tagsn and urls with links
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string TextToHtmlFragment(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            value = Regex.Replace(value, "\n", "<br />");
            value = Regex.Replace(value, @"\b(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]", "<a href=\"$0\" rel=\"nofollow\" target=\"_blank\">$0</a>", RegexOptions.IgnoreCase);

            return value;
        }

        public static string ReplaceBodyValues(string body, object container, string[] propertyNames)
        {
            var values = new Dictionary<string, string>();
            foreach (string property in propertyNames)
            {
                values.Add(property, Convert.ToString(GetPropertyValue(container, property)));
            }
            return ReplaceBodyValues(body, values);
        }

        public static string ReplaceBodyValues(string body, Dictionary<string, string> values)
        {
            if (values != null)
            {
                //replace all the template values with the object values.
                foreach (KeyValuePair<string, string> pair in values)
                {
                    body = body.Replace("<!--!" + pair.Key.ToUpper() + "!-->", pair.Value);
                }
            }
            return body;
        }

        /// <summary>
        /// Gets a property value from an object
        /// </summary>
        /// <param name="container">Container to extract the value from.</param>
        /// <param name="propName">Property name.</param>
        public static object GetPropertyValue(object container, string propName)
        {
            PropertyDescriptor descriptor = null;
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            if (string.IsNullOrEmpty(propName))
            {
                throw new ArgumentNullException("propName");
            }
            if (!propName.Contains('.'))
            {
                descriptor = TypeDescriptor.GetProperties(container).Find(propName, true);
            }
            else
            {
                string[] properties = propName.Split('.');
                container = GetPropertyValue(container, properties[0]);
                descriptor = TypeDescriptor.GetProperties(container).Find(properties[1], true);
            }
            if (descriptor == null)
            {
                throw new ArgumentException(propName + " property not found");
            }
            return descriptor.GetValue(container);
        }

        /// <summary>
        /// Gets a property value from an object
        /// </summary>
        /// <typeparam name="T">Type of the property.</typeparam>
        /// <param name="container">Container to extract the value from.</param>
        /// <param name="propName">Property name.</param>
        /// <returns></returns>
        public static T GetPropertyValue<T>(object container, string propName)
        {
            return (T)GetPropertyValue(container, propName);
        }

        #endregion
        //#region Web User Control Helper Methods

        //public static string GetTxBxValue( string controlName, FormView frm ) {
        //    var control = ( TextBox ) frm.FindControl( controlName );

        //    if ( control != null )
        //        return control.Text.Trim();

        //    throw new Exception( "could not find " + controlName + " control!" );
        //}

        //public static bool GetChkBxValue( string controlName, FormView frm ) {
        //    var control = ( CheckBox ) frm.FindControl( controlName );

        //    if ( control != null )
        //        return control.Checked;

        //    throw new Exception( "could not find " + controlName + " control!" );
        //}

        //public static string GetLabelValue( string controlName, FormView frm ) {
        //    var control = ( Label ) frm.FindControl( controlName );

        //    if ( control != null )
        //        return control.Text;

        //    throw new Exception( "could not find" + controlName + " control!" );
        //}

        //public static Guid? GetKey( FormView frm ) {
        //    const string lblKey = "lblKey";
        //    var control = ( Label ) frm.FindControl( lblKey );

        //    if ( control != null )
        //        return DataHelper.ToNullableGuid( control.Text );

        //    throw new Exception( "could not find key control!" );
        //}

        //public static string GetDDLValue( string controlName, FormView frm ) {
        //    var control = ( DropDownList ) frm.FindControl( controlName );

        //    if ( control != null )
        //        return control.SelectedValue;

        //    throw new Exception( "could not find " + controlName + " control!" );
        //}

        //public static bool GetDDLBoolean(string controlName, FormView frm)
        //{
        //    var control = (DropDownList) frm.FindControl(controlName);

        //    if (control != null)
        //        return control.SelectedValue == "Yes";

        //    throw new Exception("could not find " + controlName + " control!");
        //}

        //#endregion
    }
}
