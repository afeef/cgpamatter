﻿namespace Ideaworx.Cgpamatter.Common
{
    public static class Schema
    {
         static string _security = "[security].";

        public static string Security
        {
            get { return _security; }
            set { _security = value; }
        }
        

        private static string _reports = "[reports].";

        public static string Reports
        {
            get { return _reports; }
            set { _reports = value; }
        }        

        private static string _property = "[properties].";

        public static string Property {
            get { return _property; }
            set { _property = value; }
        }
    }
}
