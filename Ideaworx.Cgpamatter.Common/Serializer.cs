﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Ideaworx.Cgpamatter.Common
{
    public static class Serializer
    {
        public static byte[] Serialize(object graph)
        {
            byte[] serializedData = null;
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, graph);
                serializedData = stream.ToArray();
            }
            return serializedData;
        }

        public static object Deserialize(byte[] serializedData)
        {
            object graph = null;
            if (serializedData != null)
            {
                using (var stream = new MemoryStream())
                {
                    foreach (byte t in serializedData)
                        stream.WriteByte(t);

                    stream.Position = 0;
                    var formatter = new BinaryFormatter();
                    graph = formatter.Deserialize(stream);
                }
            }
            return graph;
        }
    }
}
