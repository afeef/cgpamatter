﻿namespace Ideaworx.Cgpamatter.Common.Validation
{
	public interface IEnsureValidation
	{
		void ValidateFields();
	}
}
