﻿using System;

namespace Ideaworx.Cgpamatter.Common.Validation
{
	public class ValidationError : Exception
	{
		public ValidationError()
		{

		}

		public ValidationError(string fieldName, ValidationErrorType type)
		{
			this.FieldName = fieldName;
			this.Type = type;
		}

		public string FieldName
		{
			get;
			set;
		}

		public ValidationErrorType Type
		{
			get;
			set;
		}
	}
}
