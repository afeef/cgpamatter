﻿namespace Ideaworx.Cgpamatter.Common.Validation
{
	public enum ValidationErrorType
	{
		NullOrEmpty,
		Format,
		Range,
		MaxLength,
		MinLength,
		CompareNotMatch,
		DuplicateNotAllowed,
		FileFormat,
		AccessRights
	}
}
