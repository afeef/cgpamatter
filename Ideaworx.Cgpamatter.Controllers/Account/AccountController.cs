﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using Ideaworx.Cgpamatter.Controllers.Filters;
using Ideaworx.Cgpamatter.Web;
using System.Web.Mvc;
using DotNetOpenAuth.AspNet;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Services.OAuth;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using Ideaworx.Cgpamatter.Web.Mailers;
using Ideaworx.Cgpamatter.Web.ViewModels.Account;
using Recaptcha.Web;
using Recaptcha.Web.Mvc;

namespace Ideaworx.Cgpamatter.Controllers.Account
{
    [RequireAuthorization]
    public class AccountController : BaseController
    { 
        private readonly IUserService _userService;
        private bool requireConfirmationToken = true;

        private IUserMailer _userMailer = new UserMailer();

        public IUserMailer UserMailer
        {
            get { return _userMailer; }
            set { _userMailer = value; }
        }

        public AccountController(IUserService userService)
        {
            this._userService = userService;
        }

        #region Forms Authentication

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult Login(string username, string password, string returnUrl)
        //{

        //    var model = new LoginModel();

        //    model.UserName = username;
        //    model.Password = password;

        //    return this.Login(model, returnUrl);
        //}

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = this._userService.SignIn(model.UserName, model.Password, model.RememberMe);

            if (user == null)
            {
                this.FlashError("Incorrect username or password!");

                ViewBag.ReturnUrl = returnUrl;
                return View(model);
            }

            this.Session.User = user;

            if (!String.IsNullOrEmpty(returnUrl))
                return RedirectToLocal(returnUrl);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout(string returnUrl)
        {
            this.Session.User = null;

            this._userService.SignOut();

            return Redirect(returnUrl);
        }

        #endregion


        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult RegisterPartial()
        {
            return PartialView("_RegisterPartial");
        }

        [AllowAnonymous]
        public ActionResult Activate(string token)
        {
            var user = this._userService.FindByToken(token);

            if (user == null)
                return RedirectToAction("Index", "Home");

            if (!user.IsConfirmed)
            {
                user.IsConfirmed = true;
                this._userService.Save(user);

                return View();
            }

            this.FlashError("User is already activated");

            return RedirectToAction("Login");
        }

        //
        // GET: /Account/PasswordReset

        [AllowAnonymous]
        public ActionResult PasswordReset(string token)
        {

            var user = this._userService.FindByPasswordVerificationToken(token);

            if (user == null)
            {
                this.FlashWarning("You already used this token, Please generate new token for password recovery.");
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        //
        // POST: /Account/PasswordReset

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PasswordReset(PasswordResetModel model, string token)
        {
            this._userService.ResetPassword(model.NewPassword, model.ConfirmPassword, token);

            this.FlashSuccess("Password is changed successfully");

            return RedirectToAction("Login");
        }


        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    var token = this._userService.CreateUserAndAccount(model.UserName, model.Password, null, requireConfirmationToken);

                    if (this.requireConfirmationToken)
                    {
                        this.UserMailer.ActivateAccount(token, model.UserName).Send();

                        this.FlashInfo(string.Format("Account confirmation email sent to: {0}. Kindly open this email in your mailbox and click on activate button", model.UserName));

                        return RedirectToAction("Index", "Home");
                    }

                    var user = this._userService.SignIn(model.UserName, model.Password);

                    if (user == null)
                    {
                        this.FlashError("error occured during registration");

                        return View(model);
                    }

                    this.Session.User = user;

                    return RedirectToAction("Index", "Home");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        //
        // GET: /Account/ForgotPassword

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();

            if (String.IsNullOrEmpty(recaptchaHelper.Response))
            {
                ModelState.AddModelError("", "Captcha answer cannot be empty.");
                return View(model);
            }

            RecaptchaVerificationResult recaptchaResult = await recaptchaHelper.VerifyRecaptchaResponseTaskAsync();

            if (recaptchaResult != RecaptchaVerificationResult.Success)
            {
                ModelState.AddModelError("", "Incorrect captcha answer.");
            }

            if (ModelState.IsValid)
            {

                // Attempt to change password the user
                try
                {
                    // verify email by calling validateEmail here...

                    if (this._userService.ValidateEmail(model.Email))
                    {

                        var isValid = this._userService.FindByEmail(model.Email);

                        if (isValid != null)
                        {
                            var user = this._userService.FindByEmail(model.Email);
                            user.PasswordVerificationToken = this._userService.PasswordToken();

                            this._userService.Save(user);

                            this.UserMailer.PasswordReset(user.PasswordVerificationToken, model.Email).Send();

                            this.FlashInfo(
                                string.Format(
                                    "Account confirmation email sent to: {0}. Kindly open this email in your mailbox and click on change password button",
                                    model.Email));
                        }
                        this.FlashError(string.Format("You entered an invalid email address. Get registered to cgpamatter.com "));
                    }
                    else
                    {
                        this.FlashError(string.Format("You entered an invalid email address. Get registered to cgpamatter.com "));
                    }
                    return RedirectToAction("Index", "Home");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }

                return RedirectToAction("Index", "Home");
            }

            return View(model);

        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = this._userService.GetUserName(provider, providerUserId);

            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == HttpContext.User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    var hasLocalAccount = this._userService.HasLocalAccount(this._userService.GetUserId(HttpContext.User.Identity.Name));

                    if (hasLocalAccount || this._userService.GetAccountsFromUserName(HttpContext.User.Identity.Name).Count > 1)
                    {
                        var username = this._userService.GetUserName(provider, providerUserId);

                        if (!String.IsNullOrEmpty(username))
                            this._userService.DeleteOAuthAccount(provider, providerUserId);

                        scope.Complete();

                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";

            ViewBag.HasLocalPassword = this._userService.HasLocalAccount(this._userService.GetUserId(HttpContext.User.Identity.Name));

            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            var hasLocalAccount = this._userService.HasLocalAccount(this._userService.GetUserId(HttpContext.User.Identity.Name));

            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");

            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;

                    try
                    {
                        changePasswordSucceeded = this._userService.ChangePassword(HttpContext.User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });

                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];

                if (state != null)
                    state.Errors.Clear();

                if (ModelState.IsValid)
                {
                    try
                    {
                        this._userService.CreateAccount(HttpContext.User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin

        //[HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(this._userService, provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult result = this._userService.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));

            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            var user = this._userService.SignInOAuth(result.Provider, result.ProviderUserId,
                                                    createPersistentCookie: false);
            if (user != null)
            {
                this.Session.User = user;

                if (!String.IsNullOrEmpty(returnUrl))
                    return RedirectToLocal(returnUrl);

                return RedirectToAction("Index", "Home");
            }

            if (HttpContext.User.Identity.IsAuthenticated)
            {
                // If the current user is logged in add the new account
                this._userService.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, HttpContext.User.Identity.Name);

                return RedirectToLocal(returnUrl);
            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = this._userService.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = this._userService.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
        {
            string provider = null;
            string providerUserId = null;

            if (HttpContext.User.Identity.IsAuthenticated || !this._userService.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
                return RedirectToAction("Manage");

            if (ModelState.IsValid)
            {
                // Insert a new user into the database

                var user = this._userService.FindBy(model.UserName);

                // Check if user already exists
                if (user == null)
                {
                    // Insert name into the profile table
                    this._userService.Save(new User() { UserName = model.UserName });

                    this._userService.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                    this._userService.Login(provider, providerUserId, persistCookie: false);

                    return RedirectToLocal(returnUrl);
                }

                ModelState.AddModelError("UserName", "User name already exists. Please enter a different user name.");
            }

            ViewBag.ProviderDisplayName = this._userService.GetOAuthClientData(provider).DisplayName;
            ViewBag.ReturnUrl = returnUrl;

            return View(model);
        }

        //
        // GET: /Account/ExternalLoginFailure

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", this._userService.RegisteredClientData);
        }

        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = this._userService.GetAccountsFromUserName(HttpContext.User.Identity.Name);

            var externalLogins = new List<ExternalLogin>();

            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = this._userService.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || this._userService.HasLocalAccount(this._userService.GetUserId(HttpContext.User.Identity.Name));

            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }

        #region Helpers

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            return RedirectToAction("Index", "Home");
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            private readonly IUserService _userService;

            public ExternalLoginResult(IUserService userService, string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
                this._userService = userService;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                this._userService.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        #endregion
    }
}