﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using Ideaworx.Cgpamatter.Web.ViewModels.Account;


namespace Ideaworx.Cgpamatter.Controllers.Account
{
    public class SettingsController : BaseController
    {
        private readonly IUserService _userService;
        public SettingsController(
            IUserService userService)
        {
            this._userService = userService;
        }

        public ActionResult Index()
        {
            var user = this._userService.Load(this.User.Key);

            return View(user);

        }

        [HttpPost]
        public ActionResult Index(long key, User model)
        {
            this.TryUpdateModel(model);

            if (ModelState.IsValid)
            {
                try
                {

                    var user = this._userService.Load(key);

                    user.UserName = model.UserName;
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Email = model.Email;
                    user.IsActive = model.IsActive;


                    this._userService.Update(user);

                    this.FlashInfo("Profile updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }

        //
        // GET: /Account/Manage

        public ActionResult Settings(AccountController.ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == AccountController.ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == AccountController.ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == AccountController.ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";

            ViewBag.HasLocalPassword = this._userService.HasLocalAccount(this._userService.GetUserId(HttpContext.User.Identity.Name));

            ViewBag.ReturnUrl = Url.Action("Settings");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Settings(LocalPasswordModel model)
        {
            var hasLocalAccount = this._userService.HasLocalAccount(this._userService.GetUserId(HttpContext.User.Identity.Name));

            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Settings");

            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;

                    try
                    {
                        changePasswordSucceeded = this._userService.ChangePassword(HttpContext.User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                        return RedirectToAction("Settings", new { Message = AccountController.ManageMessageId.ChangePasswordSuccess });

                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];

                if (state != null)
                    state.Errors.Clear();

                if (ModelState.IsValid)
                {
                    try
                    {
                        this._userService.CreateAccount(HttpContext.User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Settings", new { Message = AccountController.ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

    }
}
