﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Common.Configuration;
using Ideaworx.Cgpamatter.Common.Validation;
using Ideaworx.Cgpamatter.Controllers.Filters;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Web.Extensions;
using Ideaworx.Cgpamatter.Web.State;

namespace Ideaworx.Cgpamatter.Controllers
{
    [HandleErrorLog(View = "Errors/500")]
    public class BaseController : Controller
    {                                    
        #region Props

        #region State management

        private SessionWrapper _session;

        public new SessionWrapper Session
        {
            get { return _session ?? (_session = new SessionWrapper(this.ControllerContext.HttpContext.Session)); }
            set { _session = value; }
        }

        private CacheWrapper _cache;
        public CacheWrapper Cache
        {
            get { return _cache ?? (_cache = new CacheWrapper(this.ControllerContext.HttpContext.Cache)); }
            set { _cache = value; }
        }

        /// <summary>
        /// Gets the current user in session
        /// </summary>
        public new User User { get { return Session.User; } }

        #endregion

        #region Config

        public SiteConfiguration Config { get { return SiteConfiguration.Current; } }

        #endregion

        #region Uri

        public Uri Uri { get { return Request.Url; } }

        #endregion

        #region Domain
        /// <summary>
        /// Gets the application current domain (Host) including Protocol and delimiter. Example: http://www.cgpamatter.com (without slash).
        /// </summary>
        public string Domain
        {
            get
            {
                if (this.HttpContext == null)
                {
                    return "http://www.cgpamatter.com";
                }
                return this.Request.Url.Scheme + Uri.SchemeDelimiter + this.Request.Url.Host + (this.Request.Url.Port != 80 ? ":" + Request.Url.Port : String.Empty);
            }
        }
        #endregion

        #region ApplicationHome
        /// <summary>
        /// The application path url. It would be / or /forum/ depending on if its a main website or is a sub application
        /// </summary>
        private string ApplicationHomeUrl
        {
            get
            {
                return Url.Content("~/");
            }
        }
        #endregion

        #region Site Setup
        /// <summary>
        /// Determines if the site is correctly setup.
        /// </summary>
        public bool IsSiteSet
        {
            get
            {
                bool result = false;
                try
                {
                    //if (UsersService.GetTestUser() != null)
                    //{
                        result = true;
                    //}
                }
                catch
                {
                }
                return result;
            }
        }
        #endregion
        
        #endregion

        #region Init
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            this.Init();
        }

        protected virtual void Init()
        {                       
        }
        #endregion

        #region Model state errors
        /// <summary>
        /// Add the errors to the model state.
        /// </summary>
        protected void AddErrors(ModelStateDictionary modelState, ValidationException ex)
        {
            foreach (ValidationError error in ex.ValidationErrors)
            {
                modelState.AddModelError(error.FieldName, error);
            }
        }

        protected ModelErrorCollection GetErrors(ModelStateDictionary modelStateDictionary)
        {
            var errors = new ModelErrorCollection();

            foreach (KeyValuePair<string, ModelState> pair in modelStateDictionary)
            {
                foreach (ModelError error in pair.Value.Errors)
                {
                    errors.Add(error);
                }
            }

            return errors;
        }
        #endregion

        #region Templates & Master       

        protected virtual string GetDefaultMasterName()
        {
            var masterName = "_Layout";                       

            return masterName;
        }
        #endregion

        #region Action Results
        protected override ViewResult View(string viewName, string masterName, object model)
        {
            if (masterName == null)
            {
                masterName = GetDefaultMasterName();
            }
            return base.View(viewName, masterName, model);
        }

        protected virtual ViewResult View(bool useMaster, object model)
        {
            return View(useMaster, null, model);
        }

        protected virtual ViewResult View(bool useMaster, string viewName, object model)
        {
            return !useMaster 
                ? View(viewName, string.Empty, model) 
                : View(viewName, model);
        }

        protected virtual ActionResult Static(string key, bool useMaster)
        {
            return View(useMaster, "~/Views/Static/" + key + ".cshtml", null);
        }

        public ActionResult Captcha()
        {
            return CaptchaHelper.CaptchaResult(Session);
        }

        /// <summary>
        /// Only local Urls. Creates a RedirectResult object that redirects to the specified URL. Checks that the URL is a local url.
        /// If the url is null, it redirects to the application home.
        /// </summary>
        protected override RedirectResult Redirect(string url)
        {
            if ((!String.IsNullOrEmpty(url)) && Url != null && Url.IsLocalUrl(url))
            {
                return base.Redirect(url);
            }
            
            return base.Redirect(this.ApplicationHomeUrl);
        }

        #endregion
    }
}
