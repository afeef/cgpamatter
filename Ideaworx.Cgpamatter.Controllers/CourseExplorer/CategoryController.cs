﻿using System.Web.Mvc;
using Ideaworx.Cgpamatter.Controllers.Filters;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Controllers.CourseExplorer
{
    [RequireAuthorization]
    public class CategoryController: BaseController
    {
        private readonly ICategoryService _categoryService;


        public CategoryController(ICategoryService categoryService)
        {
            this._categoryService = categoryService;            
        }
        
        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._categoryService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = this.ViewData;
            items.AddFilter("searchWord", searchWord, a => a.CategoryName.Contains(searchWord));            
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        public ActionResult Create()
        {           
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Category category)
        {
            this.UpdateModel(category);

            if (this.ModelState.IsValid)
            {
                try
                {
                    this._categoryService.Save(category);

                    this.FlashInfo("Category saved...");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        }

        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            return View(this._categoryService.FindBy(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, Category category)
        {
            this.TryUpdateModel(category);

            if (ModelState.IsValid)
            {
                try
                {
                    category.AccessUserId = this.User.Key;
                    this._categoryService.Update(category);
                    this.FlashInfo("Category updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {
            return View(this._categoryService.FindBy(id));
        }

        public ActionResult Delete(int id)
        {
            return View(this._categoryService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._categoryService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult CategoryPartial()
        {
            var items = this._categoryService.FindAll();

            return PartialView("_categoryPartial", items);
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult CategoryCataloguePartial()
        {
            var items = this._categoryService.FindAll();

            return PartialView("_categoryCataloguePartial", items);
        }
    }
}
