﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ideaworx.Cgpamatter.Controllers.CourseExplorer
{
    public class CourseCatalogueController : BaseController
    {

        public ActionResult Index(long? categoryId)
        {
            ViewBag.CategoryId = categoryId;
            return View();
        }

 
    }
}
