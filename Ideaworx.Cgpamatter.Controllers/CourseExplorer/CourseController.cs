﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Controllers.Filters;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Controllers.CourseExplorer
{
    [RequireAuthorization]
    public class CourseController : BaseController
    {
        private readonly ICourseService _courseService;
        private readonly ICategoryService _categoryService;

        public CourseController(ICourseService courseService, ICategoryService categoryService)
        {
            this._courseService = courseService;
            this._categoryService = categoryService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._courseService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = this.ViewData;
            items.AddFilter("searchWord", searchWord, a => a.CourseName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        public ActionResult Create()
        {
            var categories = this._categoryService.FindAll();

            var model = new CourseViewModel(categories, new Course());

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CourseViewModel model)
        {
            var course = model.Course;
            course.Category = this._categoryService.Load(model.CategoryId);

            this.UpdateModel(course);

            if (this.ModelState.IsValid)
            {
                try
                {
                    this._courseService.Save(course);

                    this.FlashInfo("Course saved...");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        }

        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            var categories = this._categoryService.FindAll();
            var course = this._courseService.Load(id);

            var model = new CourseViewModel(categories, course);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, CourseViewModel model)
        {            
            var course = this._courseService.Load(model.Course.Key);

            course.CourseCode = model.Course.CourseCode;
            course.CourseName = model.Course.CourseName;
            course.CourseDescription = model.Course.CourseDescription;
            course.CourseLevel = model.Course.CourseLevel;
            course.CreditHours = model.Course.CreditHours;
            course.Instructor = model.Course.Instructor;
            course.IsActive = model.Course.IsActive;

            course.Category = this._categoryService.Get(model.CategoryId);

            this.TryUpdateModel(course);

            if (ModelState.IsValid)
            {
                try
                {
                    course.AccessUserId = this.User.Key;
                    this._courseService.Update(course);

                    this.FlashInfo("Course updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");

                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {
            var categories = this._categoryService.FindAll();
            var course = this._courseService.Load(id);

            var model = new CourseViewModel(categories, course);


            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var categories = this._categoryService.FindAll();
            var course = this._courseService.Load(id);

            var model = new CourseViewModel(categories, course);


            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._courseService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult CourseDetails(long? id)
        {

            var model = this._courseService.FindCourseDetail(Convert.ToInt32(id));

            return View("CourseDetails", model);
        }


        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult CoursePartial()
        {
            var items = this._courseService.FindByRecentCourses(6);

            return PartialView("_coursePartial", items);
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult CourseCataloguePartial(long? categoryId)
        {

             
            var items = categoryId == null
                    ? this._courseService.FindAll()
                    : this._courseService.FindByCategoryId(Convert.ToInt32(categoryId));

            return PartialView("_courseCataloguePartial", items);


        }
    }
}
