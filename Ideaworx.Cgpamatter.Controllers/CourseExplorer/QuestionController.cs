﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Controllers.CourseExplorer
{
    public class QuestionController : BaseController
    {
        private readonly IQuestionService _questionService;
        private readonly IQuestionTypeService _questionTypeService;
        private readonly ISyllabusService _syllabusService;
        private readonly ICourseService _courseService;

        public QuestionController(
            IQuestionService questionService,
            IQuestionTypeService questionTypeService,
            ISyllabusService syllabusService,
            ICourseService courseService)
        {
            this._questionService = questionService;
            this._questionTypeService = questionTypeService;
            this._syllabusService = syllabusService;
            this._courseService = courseService;
        }

        public ActionResult Index(long courseId, long syllabusId, GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var pagedViewModel = this._questionService.FindBySyllabusId(syllabusId, gridSortOptions, searchWord, page, pageSize, this.ViewData);

            if (Request.IsAjaxRequest())
                return PartialView("_grid", pagedViewModel);

            var syllabusList = this._syllabusService.FindByCourseId(courseId);
            var courses = this._courseService.FindAll();

            var question = new Question() { Syllabus = new Syllabus() { Key = syllabusId, Course = new Course() { Key = courseId } } };

            var model = new QuestionListModel(pagedViewModel, courses, syllabusList, question);

            return View(model);
        }

        public ActionResult Create(long courseId, long syllabusId)
        {
            var types = this._questionTypeService.FindAll();
            var syllabusList = this._syllabusService.FindByCourseId(courseId);
            var question = new Question();
            var answers = new List<Answer> { new Answer(), new Answer(), new Answer(), new Answer() };
            var model = new QuestionModel(types, syllabusList, question, answers) { CourseId = courseId };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuestionModel model)
        {
            var question = model.Question;

            question.QuestionType = this._questionTypeService.Load(model.QuestionTypeId);
            question.Syllabus = this._syllabusService.Load(model.SyllabusId);

            //this.UpdateModel(question); // -- this gives me a headache with nhibernate 

            if (!this.ModelState.IsValid) 
                return this.Create(model.CourseId, model.SyllabusId);

            try
            {
                this._questionService.Save(question, model.Answers);

                this.FlashInfo("Question saved...");

                return RedirectToAction("Index", new { courseId = model.CourseId, syllabusId = model.SyllabusId });
            }
            catch
            {
                this.FlashError("There was an error saving this record");
                return this.Create(model.CourseId, model.SyllabusId);
            }
        }


        public ActionResult Edit(long id, long courseId, long syllabusId)
        {
            var types = this._questionTypeService.FindAll();
            var syllabusList = this._syllabusService.FindByCourseId(courseId);

            var question = this._questionService.FindQuestionAndAnswersBy(id);

            var model = new QuestionModel(types, syllabusList, question, question.Answers) { CourseId = courseId };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(long id, QuestionModel model)
        {
            var question = model.Question;

            question.QuestionType = this._questionTypeService.FindBy(model.QuestionTypeId);
            question.Syllabus = this._syllabusService.FindBy(model.SyllabusId);

            //this.UpdateModel(question); // -- this gives me a headache with nhibernate 

            if (this.ModelState.IsValid)
            {
                try
                {
                    this._questionService.Save(question, model.Answers);

                    this.FlashInfo("Question updated...");

                    return RedirectToAction("Index", new { courseId = model.CourseId, syllabusId = model.SyllabusId });
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id, model.CourseId, model.SyllabusId);
                }
            }

            return this.Edit(id, model.CourseId, model.SyllabusId);
        }

        public ActionResult Details(long id, long courseId, long syllabusId)
        {
            this.ViewBag.CourseId = courseId;
            
            var question = this._questionService.Load(id);

            return View(question);
        }

        public ActionResult Delete(long id, long courseId, long syllabusId)
        {
            ViewBag.CourseId = courseId;
            ViewBag.SyllabusId = syllabusId;

            var question = this._questionService.Load(id);

            return View(question);
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._questionService.Delete(id);

                return RedirectToAction("Index", new { courseId = ViewBag.CourseId, syllabusId = ViewBag.SyllabusId });
            }
            catch
            {
                return View();
            }
        }
    }
}