﻿using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Controllers.CourseExplorer
{
    public class QuestionTypeController: BaseController
    {
        private readonly IQuestionTypeService _questionTypeService;

        public QuestionTypeController(IQuestionTypeService questionTypeService)
        {
            this._questionTypeService = questionTypeService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._questionTypeService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = this.ViewData;
            items.AddFilter("searchWord", searchWord, a => a.Name.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuestionType questionType)
        {
            this.UpdateModel(questionType);

            if (this.ModelState.IsValid)
            {
                try
                {
                    this._questionTypeService.Save(questionType);

                    this.FlashInfo("Question Type saved...");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        }

        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            return View(this._questionTypeService.FindBy(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, QuestionType questionType)
        {
            this.TryUpdateModel(questionType);

            if (ModelState.IsValid)
            {
                try
                {
                    questionType.AccessUserId = this.User.Key;
                    this._questionTypeService.Update(questionType);
                    this.FlashInfo("Question Type updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {
            return View(this._questionTypeService.FindBy(id));
        }

        public ActionResult Delete(int id)
        {
            return View(this._questionTypeService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._questionTypeService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }    
    }
}
