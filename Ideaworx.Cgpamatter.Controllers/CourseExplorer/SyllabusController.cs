﻿using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Controllers.CourseExplorer
{
    public class SyllabusController : BaseController
    {
        private readonly ISyllabusService _syllabusService;
        private readonly ICourseService _courseService;

        public SyllabusController(ISyllabusService syllabusService, ICourseService courseService)
        {
            this._syllabusService = syllabusService;
            this._courseService = courseService;
        }

        public ActionResult Index(long courseId, GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._syllabusService.FindByCourseId(courseId, gridSortOptions, searchWord, page, pageSize, this.ViewData);
            
            ViewBag.CourseId = courseId;

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        public ActionResult Create(long courseId)
        {

            var courses = this._courseService.FindAll();

            var model = new SyllabusViewModel(courses, new Syllabus(), courseId);

            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(SyllabusViewModel model)
        {
            var syllabus = model.Syllabus;
            syllabus.Course = this._courseService.FindBy(model.CourseId);

            this.UpdateModel(syllabus);

            if (this.ModelState.IsValid)
            {
                try
                {
                    this._syllabusService.Save(syllabus);

                    this.FlashInfo("syllabus saved...");

                    return RedirectToAction("Index", new { courseId = model.CourseId });
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create(model.CourseId);
                }
            }

            return this.Create(model.CourseId);
        }

        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(long id)
        {
            var courses = this._courseService.FindAll();
            var syllabus = this._syllabusService.FindBy(id);

            var model = new SyllabusViewModel(courses, syllabus);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(long id, SyllabusViewModel model)
        {
            var syllabus = model.Syllabus;
            syllabus.Course = this._courseService.FindBy(model.CourseId);

            this.TryUpdateModel(syllabus);

            if (ModelState.IsValid)
            {
                try
                {
                    syllabus.AccessUserId = this.User.Key;
                    this._syllabusService.Update(syllabus);
                    this.FlashInfo("syllabus updated successfully!");

                    return RedirectToAction("Index", new { courseId = model.CourseId });
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(long id)
        {
            var courses = this._courseService.FindAll();
            var syllabus = this._syllabusService.FindBy(id);

            var model = new SyllabusViewModel(courses, syllabus);

            return View(model);
        }

        public ActionResult Delete(long id)
        {
            var courses = this._courseService.FindAll();
            var syllabus = this._syllabusService.FindBy(id);

            var model = new SyllabusViewModel(courses, syllabus);

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(long id, FormCollection collection)
        {
            try
            {
                this._syllabusService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}