﻿using System.Linq;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Controllers.Filters;
using Ideaworx.Cgpamatter.Entities.ExamEngine;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Services.ExamEngine.Contracts;
using Ideaworx.Cgpamatter.Services.Extentions;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.ViewModels.ExamEngine;

namespace Ideaworx.Cgpamatter.Controllers.ExamEngine
{
    public class ExamAttemptController : BaseController
    {
        private readonly IExamService _examService;
        private readonly IQuestionService _questionService;
        private readonly IUserService _userService;
        private readonly IExamAttemptService _examAttemptService;

        public ExamAttemptController(
            IExamService examService,
            IQuestionService questionService,
            IUserService userService,
            IExamAttemptService examAttemptService)
        {
            this._examService = examService;
            this._questionService = questionService;
            this._userService = userService;
            this._examAttemptService = examAttemptService;
        }

        public ActionResult Index(long examId)
        {
            var exam = this._examService.Load(examId);
            var user = this._userService.Load(this.User.Key);

            var attempt = new ExamAttempt { Exam = exam, User = user };

            this._examAttemptService.Save(attempt);

            const int index = 1;

            this.TempData.SetItem(index);

            return this.RedirectToAction("Take", new { id = attempt.Key, index = index });
        }

        public ActionResult Take(long id, int index)
        {
            var attempt = this._examAttemptService.Load(id);
            var question = this._questionService.Load(attempt.Exam.Key, index - 1);

            var examQuestion = new ExamQuestionModel
            {
                ExamAttempt = attempt,
                Question = question,
                QuestionId = question.Key,
                QuestionIndex = index,
                TotalQuestions = attempt.Exam.TotalQuestions
            };

            return View("Take", examQuestion);
        }

        private void SaveAnswer(long id, ExamQuestionModel model)
        {
            var attempt = this._examAttemptService.Load(id);
            var question = this._questionService.Load(model.QuestionId);

            var answer = question.Answers.FirstOrDefault(a => a.Key == model.SelectedAnswer);

            var isfound = attempt.Answers.Intersect(question.Answers).Any();

            if (isfound)
            {
                var oldAnswer = attempt.Answers.Intersect(question.Answers).First();
                attempt.Answers.Remove(oldAnswer);
            }

            attempt.Answers.Add(answer);

            this._examAttemptService.Save(attempt);
        }

        #region Exam Navigation

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "next")]
        public ActionResult Next(long id, ExamQuestionModel model)
        {
            this.SaveAnswer(id, model);

            var index = this.TempData.GetItem<int>() + 1;

            this.TempData.SetItem(index);

            return this.RedirectToAction("Take", new { id = id, index = index });
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "previous")]
        public ActionResult Previous(long id, ExamQuestionModel model)
        {
            this.SaveAnswer(id, model);

            var index = this.TempData.GetItem<int>() - 1;

            this.TempData.SetItem(index);

            return this.RedirectToAction("Take", new { id = id, index = index });
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "first")]
        public ActionResult First(long id, ExamQuestionModel model)
        {
            this.SaveAnswer(id, model);

            const int index = 1;

            this.TempData.SetItem(index);

            return this.RedirectToAction("Take", new { id = id, index = index });
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "last")]
        public ActionResult Last(long id, ExamQuestionModel model)
        {
            this.SaveAnswer(id, model);

            var index = model.TotalQuestions;

            this.TempData.SetItem(index);

            return this.RedirectToAction("Take", new { id = id, index = index });
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "finish")]
        public ActionResult Finish(long id, ExamQuestionModel model)
        {
            if (model.SelectedAnswer > 0)
                this.SaveAnswer(id, model);

            var attempt = this._examAttemptService.Load(id);

            attempt.IsComplete = true;

            this._examAttemptService.Save(attempt);

            return this.RedirectToAction("ShowResult", "ExamResult", new { attemptId = id });
        }

        #endregion
    }
}
