﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Controllers.Filters;
using Ideaworx.Cgpamatter.Entities.ExamEngine;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Services.ExamEngine.Contracts;
using Ideaworx.Cgpamatter.Services.Extentions;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using Ideaworx.Cgpamatter.Web.ViewModels.ExamEngine;

namespace Ideaworx.Cgpamatter.Controllers.ExamEngine
{
    [RequireAuthorization]
    public class ExamController : BaseController
    {
        private readonly ICourseService _courseService;
        private readonly IExamService _examService;
        private readonly IQuestionService _questionService;
        private readonly IUserService _userService;

        public ExamController(ICourseService courseService,
            IExamService examService,
            IQuestionService questionService,
            IUserService userService)
        {
            this._courseService = courseService;
            this._examService = examService;
            this._questionService = questionService;
            this._userService = userService;
        }

        public ActionResult Index(long? courseId)
        {
            this.ViewBag.CourseId = courseId;
            var course = this._courseService.Load(Convert.ToInt64(courseId));

            return View(course);
        }

        public ActionResult Create(long? courseId)
        {            
            var exam = new ExamModel(){CourseId = courseId, Exam = new Exam()};

            return View(exam);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ExamModel examModel)
        {
            var model = new Exam();

            const int totalQuestions = 10;
            
            var courseId = Convert.ToInt64(examModel.CourseId);

            var course = this._courseService.Load(courseId);
            //var questions = this._questionService.GetRandomQuestions(totalQuestions);
            var questions = this._questionService.GetRandomQuestions(totalQuestions, courseId);
            var user = this._userService.Load(this.User.Key);

            model.Course = course;
            model.Questions = questions;
            model.User =  user;

            model.ExamType = ExamType.Custom;
            model.ExamName = "Custom";
            model.PassingScore = PassingScore.Seventy;
            model.CalculationType = CalculationType.Percentage;
            model.Duration = 60;
            model.KnowledgePoints = 0;
            model.TotalQuestions = totalQuestions;
            model.StartTime = DateTime.Now;
            model.EndTime = DateTime.Now.Add(new TimeSpan(1, 0, 0));
            model.ExamDate = DateTime.Now;
            model.Status = 0;

            this.UpdateModel(model);

            if (!this.ModelState.IsValid)
                return this.Create(courseId);

            try
            {
                this._examService.Save(model);

                return RedirectToAction("Index", "ExamAttempt", new { examId = model.Key });
            }
            catch
            {
                this.FlashError("There was an error saving this record");
                return this.Create(courseId);
            }
        }


    }
}
