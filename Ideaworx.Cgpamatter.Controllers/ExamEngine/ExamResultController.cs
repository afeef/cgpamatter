﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Services.ExamEngine.Contracts;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Controllers.ExamEngine
{
    public class ExamResultController: BaseController
    {
        private readonly IExamService _examService;
        private readonly IQuestionService _questionService;
        private readonly IUserService _userService;
        private readonly IExamAttemptService _examAttemptService;

        public ExamResultController(
            IExamService examService,
            IQuestionService questionService,
            IUserService userService,
            IExamAttemptService examAttemptService)
        {
            this._examService = examService;
            this._questionService = questionService;
            this._userService = userService;
            this._examAttemptService = examAttemptService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._examAttemptService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = this.ViewData;            
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        public ActionResult ShowResult(long attemptId)
        {
            var attempt = this._examAttemptService.GetAttemptResult(attemptId);


            return this.View(attempt);
        }
    }
}
