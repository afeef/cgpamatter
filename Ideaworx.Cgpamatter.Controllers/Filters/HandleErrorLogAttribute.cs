﻿using System.Web.Mvc;

namespace Ideaworx.Cgpamatter.Controllers.Filters
{
	public class HandleErrorLogAttribute : HandleErrorAttribute
	{
		public override void OnException(ExceptionContext filterContext)
		{
			if ((!filterContext.ExceptionHandled) && filterContext.HttpContext.IsCustomErrorEnabled)
			{
				//LoggerService.LogError(filterContext.Exception);
			}
			base.OnException(filterContext);
		}
	}
}
