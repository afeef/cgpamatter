﻿using System.Security.Authentication;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Web.Extensions;

namespace Ideaworx.Cgpamatter.Controllers.Filters
{
    public class PermissionsAttribute: BaseActionFilterAttribute
    {
        private readonly Permissions required;

        public PermissionsAttribute(Permissions required)
        {
            this.required = required;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = filterContext.HttpContext.Session["User"] as User;            
            
            if (user == null)
            {
                //send them off to the login page
                var url = new UrlHelper(filterContext.RequestContext);
                var loginUrl = url.Content("~/Account/Login");
                filterContext.HttpContext.Response.Redirect(loginUrl, true);
            }
            else
            {
                if (!user.HasPermissions(required))
                    throw new AuthenticationException("You do not have the necessary permission to perform this action");
            }
        }
    }    
}
