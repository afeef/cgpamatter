﻿using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using Ideaworx.Cgpamatter.Web.Mailers;
using System.Web.Mvc;

namespace Ideaworx.Cgpamatter.Controllers
{
    //[RequireAuthorization]
    //[Permissions(Permissions.View)]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult ExamDetail()
        {
            return View();
        }

        public ActionResult ExamRoom()
        {
            return View();
        }
    }
}
