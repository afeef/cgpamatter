﻿using System.Web.Mvc;
using Ideaworx.Cgpamatter.Controllers.Filters;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Controllers.Security
{
    [RequireAuthorization]
    public class PermissionController: BaseController
    {
        private readonly IPermissionService _permissionService;

        public PermissionController(IPermissionService permissionService)
        {
            this._permissionService = permissionService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._permissionService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = this.ViewData;
            items.AddFilter("searchWord", searchWord, a => a.PermissionName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Permission role)
        {
            this.UpdateModel(role);

            if (this.ModelState.IsValid)
            {
                try
                {
                    this._permissionService.Save(role);

                    this.FlashInfo("Role saved...");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        }

        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            return View(this._permissionService.FindBy(id));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, Permission role)
        {
            this.TryUpdateModel(role);

            if (ModelState.IsValid)
            {
                try
                {
                    role.AccessUserId = this.User.Key;
                    this._permissionService.Update(role);
                    this.FlashInfo("Role updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {
            return View(this._permissionService.FindBy(id));
        }

        public ActionResult Delete(int id)
        {
            return View(this._permissionService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._permissionService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }               
    }
}