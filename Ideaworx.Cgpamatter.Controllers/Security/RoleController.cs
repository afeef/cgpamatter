﻿using System.Web.Mvc;
using Ideaworx.Cgpamatter.Controllers.Filters;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Controllers.Security
{
    [RequireAuthorization]
    public class RoleController : BaseController
    {
        private readonly IRoleService _roleService;


        public RoleController(IRoleService roleService)
        {
            this._roleService = roleService;            
        }
        
        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._roleService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = this.ViewData;
            items.AddFilter("searchWord", searchWord, a => a.RoleName.Contains(searchWord));            
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        public ActionResult Create()
        {           
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Role role)
        {
            this.UpdateModel(role);

            if (this.ModelState.IsValid)
            {
                try
                {
                    this._roleService.Save(role);

                    this.FlashInfo("Role saved...");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        }

        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            return View(this._roleService.FindBy(id));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, Role role)
        {
            this.TryUpdateModel(role);

            if (ModelState.IsValid)
            {
                try
                {
                    role.AccessUserId = this.User.Key;
                    this._roleService.Update(role);
                    this.FlashInfo("Role updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {
            return View(this._roleService.FindBy(id));
        }

        public ActionResult Delete(int id)
        {
            return View(this._roleService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._roleService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }               
        
        public ViewResult UsersInRole(int id)
        {
            var model = this._roleService.GetUsersInRole(id);

            return View(model);
        }
    }
}
