﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Net.Mail;
using Ideaworx.Cgpamatter.Controllers.Filters;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.Extensions;
using Ideaworx.Cgpamatter.Web.ViewModels.Security;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Controllers.Security
{
    [RequireAuthorization] // allows access if you're the only user, only validates role if role provider is enabled
    public class UserController : BaseController
    {
        private const string ResetPasswordBody = "Your new password is: ";
        private const string ResetPasswordSubject = "Your New Password";
        private readonly IRoleService _roleService;
        private readonly ISmtpClientService _smtpClient;
        private readonly IUserService _userService;

        public UserController(
            IUserService userService,
            IRoleService roleService,
            ISmtpClientService smtpClient)
        {
            this._userService = userService;
            this._roleService = roleService;
            this._smtpClient = smtpClient;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._userService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = this.ViewData;
            items.AddFilter("searchWord", searchWord, a => a.UserName.Contains(searchWord));
            items.AddFilter("searchWord", searchWord, a => a.Email.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        public ViewResult Details(int id)
        {
            var user = this._userService.FindBy(id);
            var userRoles = _roleService.FindByUser(user);
            var roles = _roleService.FindAll().ToDictionary(role => role, role => userRoles.Contains(role.LoweredRoleName));

            var detailsViewModel = this._userService.UserDetails(user, roles);

            return View(detailsViewModel);
        }
        
        public ActionResult Edit(int id)
        {
            return View(this._userService.FindBy(id));
        }

        [HttpPost]        
        public ActionResult Edit(int id, User user)
        {
            this.TryUpdateModel(user);

            if (ModelState.IsValid)
            {
                try
                {
                    user.AccessUserId = this.User.Key;

                    this._userService.Update(user);

                    this.FlashInfo("User updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }        

        public ActionResult Delete(int id)
        {
            return View(this._userService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._userService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ViewResult Password(int id)
        {
            var user = _userService.FindBy(id);
            var userRoles = _roleService.FindByUser(user);
            var roles = _roleService.FindAll().ToDictionary(role => role, role => userRoles.Contains(role.LoweredRoleName));

            var detailsViewModel = this._userService.UserDetails(user, roles);

            return View(detailsViewModel);
        }

        public ViewResult UsersRoles(int id)
        {
            var user = _userService.FindBy(id);
            var userRoles = _roleService.FindByUser(user);

            var roles = _roleService.FindAll().ToDictionary(role => role, role => userRoles.Contains(role.LoweredRoleName));

            var detailsViewModel = this._userService.UserDetails(user, roles);

            return View(detailsViewModel);
        }

        // TODO: remove this and use the Create instead
        public ViewResult Create()
        {
            var model = new CreateUserViewModel
            {
                InitialRoles = _roleService.FindAll().Select(r => r.LoweredRoleName).ToDictionary(k => k, v => false)
            };
            return View(model);
        }        

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(CreateUserViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                if (model.Password != model.ConfirmPassword)
                    throw new ArgumentException("Passwords do not match.");

                var user = _userService.Create(model);

                if (model.InitialRoles != null)
                {
                    var initialRoles = model.InitialRoles.Where(x => x.Value).Select(x => x.Key);

                    foreach (var role in initialRoles)
                        this._roleService.AddUserToRole(user, role);
                }

                return RedirectToAction("Details", new { id = user.Key });
            }
            catch (Exception e)
            {
                this.FlashError(e.Message);
                return View(model);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult Details(int id, string email)
        {
            var user = _userService.FindBy(id);
            user.Email = email;
            user.IsApproved = true;

            _userService.Update(user);
            return RedirectToAction("Details", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult DeleteUser(int id)
        {
            var user = _userService.FindBy(id);

            if (_roleService.Enabled)
                _roleService.RemoveFromAllRoles(user);

            _userService.Delete(user.Key);

            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult ChangeApproval(int id, bool isApproved)
        {
            var user = _userService.FindBy(id);
            user.IsApproved = isApproved;

            _userService.Update(user);

            return RedirectToAction("Details", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult Unlock(int id)
        {
            this._userService.Unlock(id);

            return RedirectToAction("Details", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult ResetPassword(int id)
        {
            var user = _userService.FindBy(id);
            var newPassword = "";// this._userService.ResetPassword(user.UserName, user.Password);

            var body = ResetPasswordBody + newPassword;
            var msg = new MailMessage();
            msg.To.Add(user.Email);
            msg.Subject = ResetPasswordSubject;
            msg.Body = body;
            _smtpClient.Send(msg);

            return RedirectToAction("Password", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult ResetPasswordWithAnswer(int id, string answer)
        {
            var user = _userService.FindBy(id);
            var newPassword = "";// this._userService.ResetPassword(user.UserName, answer);

            var body = ResetPasswordBody + newPassword;
            var msg = new MailMessage();
            msg.To.Add(user.Email);
            msg.Subject = ResetPasswordSubject;
            msg.Body = body;
            _smtpClient.Send(msg);

            return RedirectToAction("Password", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult SetPassword(int id, string password)
        {
            var user = _userService.FindBy(id);
            //this._userService.ChangePassword(user.UserName, password);

            var body = ResetPasswordBody + password;
            var msg = new MailMessage();
            msg.To.Add(user.Email);
            msg.Subject = ResetPasswordSubject;
            msg.Body = body;
            _smtpClient.Send(msg);

            return RedirectToAction("Password", new { id });
        }

        // TODO: move this to the roles controller
        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult AddToRole(int id, string role)
        {
            this._roleService.AddUserToRole(_userService.FindBy(id), role);

            return RedirectToAction("UsersRoles", new { id });
        }

        // TODO: move this to the roles controller
        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult RemoveFromRole(int id, string role)
        {
            this._roleService.RemoveFromRole(_userService.FindBy(id), role);

            return RedirectToAction("UsersRoles", new { id });
        }
    }
}
