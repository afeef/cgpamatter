﻿CREATE TABLE [examengine].[Answers] (
    [AnswerId]         BIGINT          IDENTITY (1, 1) NOT NULL,
    [QuestionId]       BIGINT          NOT NULL,
    [AnswerText]       NVARCHAR (500)  NOT NULL,
    [AnswerImage]      VARBINARY (MAX) NULL,
    [AnswerAlphaOrder] INT             NULL,
    [Description]      NVARCHAR (500)  NULL,
    [IsCorrect]        BIT             NULL,
    [DateCreated]      DATETIME        NULL,
    [DateModified]     DATETIME        NULL,
    [AccessUserId]     BIGINT          NULL,
    [ModifiedUserId]   BIGINT          NULL,
    [IsActive]         BIT             NULL,
    [TimeStamp]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Answers] PRIMARY KEY CLUSTERED ([AnswerId] ASC),
    CONSTRAINT [FK_Answers_Questions] FOREIGN KEY ([QuestionId]) REFERENCES [examengine].[Questions] ([QuestionId])
);



