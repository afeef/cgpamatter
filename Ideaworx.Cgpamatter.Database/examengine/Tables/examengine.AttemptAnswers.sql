﻿CREATE TABLE [examengine].[AttemptAnswers] (
    [AttemptAnswerId] BIGINT IDENTITY (1, 1) NOT NULL,
    [ExamAttemptId]   BIGINT NOT NULL,
    [AnswerId]        BIGINT NOT NULL,
    CONSTRAINT [PK_AttemptAnswers] PRIMARY KEY CLUSTERED ([AttemptAnswerId] ASC),
    CONSTRAINT [FK_AttemptAnswers_Answers] FOREIGN KEY ([AnswerId]) REFERENCES [examengine].[Answers] ([AnswerId]),
    CONSTRAINT [FK_AttemptAnswers_ExamAttempts] FOREIGN KEY ([ExamAttemptId]) REFERENCES [examengine].[ExamAttempts] ([ExamAttemptId])
);

