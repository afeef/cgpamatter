﻿CREATE TABLE [examengine].[Categories] (
    [CategoryId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [CategoryName]        NVARCHAR (50)  NOT NULL,
    [LoweredCategoryName] NVARCHAR (50)  NULL,
    [Description]         NVARCHAR (250) NULL,
    [DateCreated]         DATETIME       NULL,
    [DateModified]        DATETIME       NULL,
    [AccessUserId]        BIGINT         NULL,
    [ModifiedUserId]      BIGINT         NULL,
    [IsActive]            BIT            NULL,
    [TimeStamp]           ROWVERSION     NOT NULL,
    CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED ([CategoryId] ASC)
);

