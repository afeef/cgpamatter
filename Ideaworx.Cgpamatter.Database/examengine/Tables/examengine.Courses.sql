﻿CREATE TABLE [examengine].[Courses] (
    [CourseId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [CategoryId]        BIGINT         NOT NULL,
    [CourseCode]        NVARCHAR (50)  NOT NULL,
    [CourseName]        NVARCHAR (250) NOT NULL,
    [CourseDescription] NVARCHAR (MAX) NULL,
    [CourseLevel]       NVARCHAR (250) NULL,
    [CreditHours]       INT            NULL,
    [Instructor]        NVARCHAR (250) NULL,
    [DateCreated]       DATETIME       NULL,
    [DateModified]      DATETIME       NULL,
    [AccessUserId]      BIGINT         NULL,
    [ModifiedUserId]    BIGINT         NULL,
    [IsActive]          BIT            NULL,
    [TimeStamp]         ROWVERSION     NOT NULL,
    CONSTRAINT [PK_Courses] PRIMARY KEY CLUSTERED ([CourseId] ASC),
    CONSTRAINT [FK_Courses_Categories] FOREIGN KEY ([CategoryId]) REFERENCES [examengine].[Categories] ([CategoryId])
);







