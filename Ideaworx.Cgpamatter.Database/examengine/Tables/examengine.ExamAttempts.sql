﻿CREATE TABLE [examengine].[ExamAttempts] (
    [ExamAttemptId]  BIGINT     IDENTITY (1, 1) NOT NULL,
    [UserId]         BIGINT     NOT NULL,
    [ExamId]         BIGINT     NOT NULL,
    [IsComplete]     BIT        NOT NULL,
    [DateCreated]    DATETIME   NULL,
    [DateModified]   DATETIME   NULL,
    [AccessUserId]   BIGINT     NULL,
    [ModifiedUserId] BIGINT     NULL,
    [IsActive]       BIT        NULL,
    [TimeStamp]      ROWVERSION NOT NULL,
    CONSTRAINT [PK_UserExams] PRIMARY KEY CLUSTERED ([ExamAttemptId] ASC),
    CONSTRAINT [FK_ExamAttempts_Exams] FOREIGN KEY ([ExamId]) REFERENCES [examengine].[Exams] ([ExamId]),
    CONSTRAINT [FK_ExamAttempts_Users] FOREIGN KEY ([UserId]) REFERENCES [security].[Users] ([UserId])
);



