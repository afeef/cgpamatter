﻿CREATE TABLE [examengine].[ExamQuestions] (
    [ExamQuestionId] BIGINT IDENTITY (1, 1) NOT NULL,
    [ExamId]         BIGINT NOT NULL,
    [QuestionId]     BIGINT NOT NULL,
    CONSTRAINT [PK_ExamQuestions] PRIMARY KEY CLUSTERED ([ExamQuestionId] ASC),
    CONSTRAINT [FK_ExamQuestions_Exams] FOREIGN KEY ([ExamId]) REFERENCES [examengine].[Exams] ([ExamId]),
    CONSTRAINT [FK_ExamQuestions_Questions] FOREIGN KEY ([QuestionId]) REFERENCES [examengine].[Questions] ([QuestionId])
);

