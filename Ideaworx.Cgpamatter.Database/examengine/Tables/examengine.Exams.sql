﻿CREATE TABLE [examengine].[Exams] (
    [ExamId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [CourseId]        BIGINT         NOT NULL,
    [UserId]          BIGINT         NOT NULL,
    [ExamType]        TINYINT        NULL,
    [ExamName]        NVARCHAR (50)  NULL,
    [Description]     NVARCHAR (250) NULL,
    [PassingScore]    TINYINT        NULL,
    [CalculationType] TINYINT        NULL,
    [Duration]        TINYINT        NULL,
    [KnowledgePoints] TINYINT        NULL,
    [TotalQuestions]  INT            NULL,
    [ExamDate]        DATETIME       NULL,
    [StartTime]       DATETIME       NULL,
    [EndTime]         DATETIME       NULL,
    [Status]          TINYINT        NULL,
    [DateCreated]     DATETIME       NULL,
    [DateModified]    DATETIME       NULL,
    [AccessUserId]    BIGINT         NULL,
    [ModifiedUserId]  BIGINT         NULL,
    [IsActive]        BIT            NULL,
    [TimeStamp]       ROWVERSION     NOT NULL,
    CONSTRAINT [PK_Exams] PRIMARY KEY CLUSTERED ([ExamId] ASC),
    CONSTRAINT [FK_Exams_Courses] FOREIGN KEY ([CourseId]) REFERENCES [examengine].[Courses] ([CourseId]),
    CONSTRAINT [FK_Exams_Users] FOREIGN KEY ([UserId]) REFERENCES [security].[Users] ([UserId])
);





