﻿CREATE TABLE [examengine].[QuestionTypes] (
    [QuestionTypeId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [Type]           INT            NOT NULL,
    [Name]           NVARCHAR (50)  NOT NULL,
    [Description]    NVARCHAR (250) NULL,
    [DateCreated]    DATETIME       NULL,
    [DateModified]   DATETIME       NULL,
    [AccessUserId]   BIGINT         NULL,
    [ModifiedUserId] BIGINT         NULL,
    [IsActive]       BIT            NULL,
    [TimeStamp]      ROWVERSION     NOT NULL,
    CONSTRAINT [PK_QuestionTypes] PRIMARY KEY CLUSTERED ([QuestionTypeId] ASC)
);

