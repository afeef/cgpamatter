﻿CREATE TABLE [examengine].[Questions] (
    [QuestionId]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [QuestionTypeId] BIGINT          NOT NULL,
    [SyllabusId]     BIGINT          NOT NULL,
    [Text]           NVARCHAR (250)  NOT NULL,
    [Description]    NVARCHAR (MAX)  NULL,
    [Image]          VARBINARY (MAX) NULL,
    [VideoLink]      NVARCHAR (250)  NULL,
    [DateCreated]    DATETIME        NULL,
    [DateModified]   DATETIME        NULL,
    [AccessUserId]   BIGINT          NULL,
    [ModifiedUserId] BIGINT          NULL,
    [IsActive]       BIT             NULL,
    [TimeStamp]      ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Questions] PRIMARY KEY CLUSTERED ([QuestionId] ASC),
    CONSTRAINT [FK_Questions_QuestionTypes] FOREIGN KEY ([QuestionTypeId]) REFERENCES [examengine].[QuestionTypes] ([QuestionTypeId]),
    CONSTRAINT [FK_Questions_Syllabus] FOREIGN KEY ([SyllabusId]) REFERENCES [examengine].[Syllabus] ([SyllabusId])
);





