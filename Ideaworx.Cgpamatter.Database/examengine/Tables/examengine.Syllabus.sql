﻿CREATE TABLE [examengine].[Syllabus] (
    [SyllabusId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [CourseId]            BIGINT         NOT NULL,
    [SyllabusText]        NVARCHAR (50)  NOT NULL,
    [SyllabusDescription] NVARCHAR (250) NULL,
    [DateCreated]         DATETIME       NULL,
    [DateModified]        DATETIME       NULL,
    [AccessUserId]        BIGINT         NULL,
    [ModifiedUserId]      BIGINT         NULL,
    [IsActive]            BIT            NULL,
    [TimeStamp]           ROWVERSION     NOT NULL,
    CONSTRAINT [PK_Syllabus] PRIMARY KEY CLUSTERED ([SyllabusId] ASC),
    CONSTRAINT [FK_Syllabus_Courses] FOREIGN KEY ([CourseId]) REFERENCES [examengine].[Courses] ([CourseId])
);



