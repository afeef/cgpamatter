﻿CREATE TABLE [examengine].[UserCourses] (
    [UserCourseId] BIGINT IDENTITY (1, 1) NOT NULL,
    [UserId]       BIGINT NOT NULL,
    [CourseId]     BIGINT NOT NULL,
    CONSTRAINT [PK_UserCourses] PRIMARY KEY CLUSTERED ([UserCourseId] ASC),
    CONSTRAINT [FK_UserCourses_Courses] FOREIGN KEY ([CourseId]) REFERENCES [examengine].[Courses] ([CourseId]),
    CONSTRAINT [FK_UserCourses_Users] FOREIGN KEY ([UserId]) REFERENCES [security].[Users] ([UserId])
);

