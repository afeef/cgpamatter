﻿CREATE TABLE [security].[OAuthAccounts] (
    [OAuthAccountId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [UserId]         BIGINT         NOT NULL,
    [Provider]       NVARCHAR (250) NOT NULL,
    [ProviderUserId] NVARCHAR (250) NOT NULL,
    [DateCreated]    DATETIME       NULL,
    [DateModified]   DATETIME       NULL,
    [AccessUserId]   BIGINT         NULL,
    [ModifiedUserId] BIGINT         NULL,
    [IsActive]       BIT            NULL,
    [TimeStamp]      ROWVERSION     NOT NULL,
    CONSTRAINT [PK_OAuthAccounts] PRIMARY KEY CLUSTERED ([OAuthAccountId] ASC),
    CONSTRAINT [FK_OAuthAccounts_Users] FOREIGN KEY ([UserId]) REFERENCES [security].[Users] ([UserId])
);

