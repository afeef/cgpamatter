﻿CREATE TABLE [security].[OAuthToken] (
    [OAuthTokenId]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [Token]          NVARCHAR (50) NOT NULL,
    [Secret]         NVARCHAR (50) NOT NULL,
    [DateCreated]    DATETIME      NULL,
    [DateModified]   DATETIME      NULL,
    [AccessUserId]   NCHAR (10)    NULL,
    [ModifiedUserId] NCHAR (10)    NULL,
    [IsActive]       NCHAR (10)    NULL,
    [TimeStamp]      ROWVERSION    NOT NULL,
    CONSTRAINT [PK_OAuthToken] PRIMARY KEY CLUSTERED ([OAuthTokenId] ASC)
);

