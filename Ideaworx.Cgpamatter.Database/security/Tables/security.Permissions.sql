﻿CREATE TABLE [security].[Permissions] (
    [PermissionId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [PermissionName]        NVARCHAR (50)  NOT NULL,
    [PermissionFlag]        BIT            NULL,
    [PermissionDescription] NVARCHAR (250) NULL,
    [DateCreated]           DATETIME       NULL,
    [DateModified]          DATETIME       NULL,
    [AccessUserId]          BIGINT         NULL,
    [ModifiedUserId]        BIGINT         NULL,
    [IsActive]              BIT            NULL,
    [TimeStamp]             ROWVERSION     NOT NULL,
    CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED ([PermissionId] ASC)
);

