﻿CREATE TABLE [security].[Roles] (
    [RoleId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [RoleName]        NVARCHAR (50)  NOT NULL,
    [LoweredRoleName] NVARCHAR (50)  NULL,
    [Description]     NVARCHAR (250) NULL,
    [DateCreated]     DATETIME       NULL,
    [DateModified]    DATETIME       NULL,
    [AccessUserId]    BIGINT         NULL,
    [ModifiedUserId]  BIGINT         NULL,
    [IsActive]        BIT            NULL,
    [TimeStamp]       ROWVERSION     NOT NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([RoleId] ASC)
);

