﻿CREATE TABLE [security].[Users] (
    [UserId]                                  BIGINT         IDENTITY (1, 1) NOT NULL,
    [UserName]                                NVARCHAR (50)  NOT NULL,
    [Password]                                NVARCHAR (128) NULL,
    [Email]                                   NVARCHAR (250) NULL,
    [PasswordQuestion]                        NVARCHAR (250) NULL,
    [PasswordAnswer]                          NVARCHAR (250) NULL,
    [FirstName]                               NVARCHAR (50)  NULL,
    [LastName]                                NVARCHAR (50)  NULL,
    [FatherName]                              NVARCHAR (50)  NULL,
    [Cnic]                                    BIGINT         NULL,
    [City]                                    NVARCHAR (50)  NULL,
    [Photo]                                   NVARCHAR (50)  NULL,
    [ConfirmationToken]                       NVARCHAR (50)  NULL,
    [PasswordSalt]                            NVARCHAR (50)  NULL,
    [PasswordVerificationToken]               NVARCHAR (50)  NULL,
    [PasswordVerificationTokenExpirationDate] DATETIME       NULL,
    [PasswordChangedDate]                     DATETIME       NULL,
    [LastPasswordChangedDate]                 DATETIME       NULL,
    [LastPasswordFailureDate]                 DATETIME       NULL,
    [LastLockoutDate]                         DATETIME       NULL,
    [LastActivityDate]                        DATETIME       NULL,
    [LastLoginDate]                           DATETIME       NULL,
    [DateCreated]                             DATETIME       NULL,
    [DateModified]                            DATETIME       NULL,
    [IsOnline]                                BIT            NULL,
    [IsApproved]                              BIT            NULL,
    [IsLockedOut]                             BIT            NULL,
    [IsConfirmed]                             BIT            NULL,
    [IsActive]                                BIT            NULL,
    [PasswordFailuresSinceLastSuccess]        INT            NULL,
    [AccessUserId]                            BIGINT         NULL,
    [ModifiedUserId]                          BIGINT         NULL,
    [TimeStamp]                               ROWVERSION     NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserId] ASC)
);





