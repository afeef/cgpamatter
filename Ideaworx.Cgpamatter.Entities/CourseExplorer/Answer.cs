﻿namespace Ideaworx.Cgpamatter.Entities.CourseExplorer
{
    public class Answer : EntityBase
    {
        public virtual Question Question { get; set; }
        public virtual string AnswerText { get; set; }
        public virtual byte[] AnswerImage { get; set; }
        public virtual int AnswerAlphaOrder { get; set; }
        public virtual string Description { get; set; }
        public virtual bool IsCorrect { get; set; }
    }
}
