﻿using System.Collections.Generic;

namespace Ideaworx.Cgpamatter.Entities.CourseExplorer
{
    public class Category: EntityBase
    {
        public virtual string CategoryName { get; set; }
        public virtual string LoweredCategoryName { get; set; }
        public virtual string Description { get; set; }
        public virtual IList<Course> Courses { get; set; }
    }
}
