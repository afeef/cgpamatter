﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ideaworx.Cgpamatter.Entities.CourseExplorer
{
    public class Course : EntityBase
    {

        [Required]
        [Display(Name = "Course code")]
        public virtual string CourseCode { get; set; }
        [Required]
        [Display(Name = "Course name")]
        public virtual string CourseName { get; set; }

        [Display(Name = "Course description")]
        public virtual string CourseDescription { get; set; }

        [Display(Name = "Course level")]
        public virtual string CourseLevel { get; set; }

        [Display(Name = "Credit hours")]
        public virtual int CreditHours { get; set; }

        [Display(Name = "Instructor name")]
        public virtual string Instructor { get; set; }
        
        public virtual Category Category { get; set; }
        public virtual IList<Syllabus> SyllabusList { get; set; }
    }
}
