﻿using System.Collections.Generic;
using Ideaworx.Cgpamatter.Entities.ExamEngine;

namespace Ideaworx.Cgpamatter.Entities.CourseExplorer
{
    public class Question: EntityBase
    {        
        public virtual QuestionType QuestionType { get; set; }
        public virtual Syllabus Syllabus { get; set; }
        public virtual string Text { get; set; }
        public virtual string Description { get; set; }
        public virtual byte[] Image { get; set; }
        public virtual string VideoLink { get; set; }
        public virtual IList<Answer> Answers { get; set; }
        public virtual IList<Exam> Exams { get; set; } 
    }
}