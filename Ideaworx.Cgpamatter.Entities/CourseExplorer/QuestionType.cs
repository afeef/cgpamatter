﻿namespace Ideaworx.Cgpamatter.Entities.CourseExplorer
{
    public class QuestionType: EntityBase
    {
        public virtual int Type { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
    }
}
