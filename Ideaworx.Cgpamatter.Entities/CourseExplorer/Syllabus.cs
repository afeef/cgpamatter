﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ideaworx.Cgpamatter.Entities.CourseExplorer
{
    public class Syllabus: EntityBase
    {
        public virtual Course Course { get; set; }
        public virtual string SyllabusText { get; set; }
        public virtual string SyllabusDescription { get; set; }
    }
}
