﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ideaworx.Cgpamatter.Entities
{
    public abstract class EntityBase : IEntity
    {                               
        public virtual long Key { get; set; }

        [Display(Name = "Date created")]
        public virtual DateTime? DateCreated { get; set; }

        [Display(Name = "Date modified")]
        public virtual DateTime? DateModified { get; set; }        
        public virtual long AccessUserId { get; set; }
        public virtual long ModifiedUserId { get; set; }

        [Display(Name = "Is active")]
        public virtual bool IsActive { get; set; }        
        public virtual byte[] TimeStamp { get; set; }        
    }
}