﻿using System;
using System.Collections.Generic;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Entities.Security;

namespace Ideaworx.Cgpamatter.Entities.ExamEngine
{
    public class Exam : EntityBase
    {
        public virtual Course Course { get; set; }
        public virtual User User { get; set; }
        public virtual ExamType ExamType { get; set; }
        public virtual string ExamName { get; set; }
        public virtual string Description { get; set; }                
        public virtual PassingScore PassingScore { get; set; }
        public virtual CalculationType CalculationType { get; set; }
        public virtual int Duration { get; set; }
        public virtual int KnowledgePoints { get; set; }
        public virtual int TotalQuestions { get; set; }
        public virtual DateTime ExamDate { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual DateTime EndTime { get; set; }
        public virtual int Status { get; set; }
        public virtual IList<ExamAttempt> ExamAttempts { get; set; } 
        public virtual IList<Question> Questions { get; set; } 
    }
}
