﻿using System.Collections.Generic;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Entities.Security;

namespace Ideaworx.Cgpamatter.Entities.ExamEngine
{
    public class ExamAttempt : EntityBase
    {
        public virtual User User { get; set; }
        public virtual Exam Exam { get; set; }
        public virtual bool IsComplete { get; set; }
        public virtual IList<Answer> Answers { get; set; } 
    }
}
