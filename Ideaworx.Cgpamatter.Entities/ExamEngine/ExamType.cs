﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ideaworx.Cgpamatter.Entities.ExamEngine
{
    public enum ExamType
    {
        MidTerm = 0,
        FinalTerm = 1,
        Custom = 2
    }
}
