﻿using System;

namespace Ideaworx.Cgpamatter.Entities
{
    public interface IEntity
    {
        long Key { get; }
        
        DateTime? DateCreated { get; set; }
        DateTime? DateModified { get; set; }        
        long AccessUserId { get; set; }
        long ModifiedUserId { get; set; }
        bool IsActive { get; set; }
        byte[] TimeStamp { get; set; }
    }
}
