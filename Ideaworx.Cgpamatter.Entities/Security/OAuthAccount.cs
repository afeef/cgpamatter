﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ideaworx.Cgpamatter.Entities.Security
{
    public class OAuthAccount: EntityBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OAuthAccount"/> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="providerUserId">The provider user id.</param>
        public OAuthAccount(string provider, string providerUserId)
        {
            if (String.IsNullOrEmpty(provider))
                throw new ArgumentException(
                    String.Format(CultureInfo.CurrentCulture, "Argument_Cannot_Be_Null_Or_Empty", "provider"),
                    "provider");

            if (String.IsNullOrEmpty(providerUserId))
                throw new ArgumentException(
                    String.Format(CultureInfo.CurrentCulture, "Argument_Cannot_Be_Null_Or_Empty", "providerUserId"),
                    "providerUserId");

            Provider = provider;
            ProviderUserId = providerUserId;
        }

        public OAuthAccount()
        {
            
        }

        public virtual User User { get; set; }
        public virtual string Provider { get; set; }
        public virtual string ProviderUserId { get; set; }
    }
}
