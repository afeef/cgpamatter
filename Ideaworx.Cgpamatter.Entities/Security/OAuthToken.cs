﻿namespace Ideaworx.Cgpamatter.Entities.Security
{
    public class OAuthToken: EntityBase
    {                
        public virtual string Token { get; set; }
        public virtual string Secret { get; set; }
    }
}
