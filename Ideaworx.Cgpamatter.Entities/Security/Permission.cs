﻿using System.Collections.Generic;

namespace Ideaworx.Cgpamatter.Entities.Security
{
    public class Permission : EntityBase
    {
        public virtual string PermissionName { get; set; }
        public virtual Permissions PermissionFlag { get; set; }
        public virtual string PermissionDescription { get; set; }        
        public virtual IList<Role> Roles { get; set; } 
    }
}
