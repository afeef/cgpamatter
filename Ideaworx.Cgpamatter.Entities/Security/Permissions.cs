﻿using System;

namespace Ideaworx.Cgpamatter.Entities.Security
{
    [Flags]
    public enum Permissions
    {
        View = (1 << 0),
        Add = (1 << 1),
        Edit = (1 << 2),
        Delete = (1 << 3),
        Admin = (View | Add | Edit | Delete)
    }
}
