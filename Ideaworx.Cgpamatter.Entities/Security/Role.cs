﻿using System.Collections.Generic;

namespace Ideaworx.Cgpamatter.Entities.Security
{
    public class Role : EntityBase
    {
        public virtual string RoleName { get; set; }        
        public virtual string LoweredRoleName { get; set; }
        public virtual string Description { get; set; }        
        public virtual IList<User> Users { get; set; }
        public virtual IList<Permission> Permissions { get; set; }        
    }
}
