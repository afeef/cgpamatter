﻿using System;
using System.Collections.Generic;
using Ideaworx.Cgpamatter.Entities.ExamEngine;

namespace Ideaworx.Cgpamatter.Entities.Security
{
    public class User : EntityBase
    {        
        public virtual string FullName { get { return string.Format("{0} {1}", this.FirstName, this.LastName); } }
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }
        public virtual string Email { get; set; }
        public virtual string PasswordQuestion { get; set; }
        public virtual string PasswordAnswer { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string FatherName { get; set; }
        public virtual long Cnic { get; set; }
        public virtual string City { get; set; }
        public virtual string Photo { get; set; }
        public virtual string ConfirmationToken { get; set; }        
        public virtual string PasswordSalt { get; set; }
        public virtual string PasswordVerificationToken { get; set; }
        public virtual DateTime? PasswordVerificationTokenExpirationDate { get; set; }
        public virtual DateTime? PasswordChangedDate { get; set; }
        public virtual DateTime? LastPasswordChangedDate { get; set; }
        public virtual DateTime? LastPasswordFailureDate { get; set; }
        public virtual DateTime? LastLockoutDate { get; set; }
        public virtual DateTime? LastActivityDate { get; set; }
        public virtual DateTime? LastLoginDate { get; set; }
        public virtual bool IsOnline { get; set; }
        public virtual bool IsApproved { get; set; }
        public virtual bool IsLockedOut { get; set; }
        public virtual bool IsConfirmed { get; set; }
        public virtual int PasswordFailuresSinceLastSuccess { get; set; }        
        public virtual IList<Role> Roles { get; set; }
        public virtual IList<OAuthAccount> Accounts { get; set; }
        public virtual IList<Exam> Exams { get; set; }
        public virtual IList<ExamAttempt> ExamAttempts { get; set; } 
    }
}
