﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ideaworx.Cgpamatter.Entities;
using Ideaworx.Cgpamatter.Web.ViewModels;
using MvcContrib.UI.Grid;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using SortDirection = MvcContrib.Sorting.SortDirection;

namespace Ideaworx.Cgpamatter.Services
{
    public class BaseService<T> : IService<T> where T: EntityBase
    {
        protected readonly ISession Session;
        protected ITransaction Transaction;
        public int Count { get; protected set; }

        public BaseService(ISession session)
        {
            this.Session = session;
        }
        
        public T FindBy(long key) 
        {
            return this.Session.QueryOver<T>().Where(x => x.Key == key).SingleOrDefault();
        }

        public T Load(long key)
        {
            return this.Session.Load<T>(key);            
        }

        public T Get(long key)
        {
            return this.Session.Get<T>(key);
        }

        public IList<T> FindAll()
        {
            return this.Session.QueryOver<T>().List();
        }

        public IList<T> FindAll(int pageIndex, int pageSize)
        {
            var query = QueryOver.Of<T>().OrderBy(c => c.Key).Asc;

            var colors = query.GetExecutableQueryOver(this.Session)
                .Skip(pageIndex)
                .Take(pageSize)
                .List();

            this.Count = query.GetExecutableQueryOver(this.Session).RowCount();

            return colors;
        }

        public IQueryable<T> GetQueryable()
        {
            var query = from c in Session.Query<T>()
                        select c;

            return query;
        }

        public IList<T> FindAll(string sortExpression, SortDirection sortDirection, int startRowIndex, int pageSize)
        {
            var query = QueryOver.Of<T>()
                //.Where(c => c.Column == sortExpression)
                //.OrderBy(c => c.CourseCode).Asc
                .Skip(startRowIndex)
                .Take(pageSize);

            var courses = query.GetExecutableQueryOver(this.Session).List();
            this.Count = query.GetExecutableQueryOver(this.Session).RowCount();

            return courses;
        }

        public PagedViewModel<T> FindAll(GridSortOptions options, string searchWord, int page, int pageSize)
        {
            var query = from c in Session.Query<T>()
                        select c;

            var pagedViewModel = new PagedViewModel<T>
            {
                Query = query,
                GridSortOptions = options,
                Page = page,
                PageSize = pageSize
            };

            return pagedViewModel;
        }

        public void Save(T entity)
        {
            using (this.Transaction = this.Session.BeginTransaction())
            {
                entity.DateCreated = DateTime.Now;
                entity.DateModified = DateTime.Now;
                //entity.AccessUserId = this.User.Key;
                entity.IsActive = true;

                this.Session.SaveOrUpdate(entity);

                this.Transaction.Commit();
            }
        }

        public void Update(T entity)
        {
            using (this.Transaction = this.Session.BeginTransaction())
            {
                //branch.DateCreated = DateTime.Now;
                entity.DateModified = DateTime.Now;
                //branch.AccessUserId = this.User.Key;
                entity.IsActive = true;
                this.Session.Update(entity);

                this.Transaction.Commit();
            }
        }

        public bool Delete(long key)
        {
            using (this.Transaction = this.Session.BeginTransaction())
            {
                var dp = this.Load(key);
                
                this.Session.Delete(dp);

                this.Transaction.Commit();

                return true;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }        
    }
}
