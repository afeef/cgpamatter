﻿using System;
using System.Collections.Generic;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using NHibernate;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer
{
    public class AnswerService : BaseService<Answer>, IAnswerService
    {
        public AnswerService(ISession session)
            : base(session)
        {
        }

        
    }
}
