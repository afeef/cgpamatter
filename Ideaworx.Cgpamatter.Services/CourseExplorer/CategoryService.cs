﻿using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using NHibernate;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer
{
    public class CategoryService : BaseService<Category>, ICategoryService
    {
        public CategoryService(ISession session) 
            : base(session)
        {
        }
    }
}
