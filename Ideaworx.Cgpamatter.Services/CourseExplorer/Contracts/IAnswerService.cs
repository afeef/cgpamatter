﻿using System.Collections.Generic;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts
{
    public interface IAnswerService: IService<Answer>
    {
        
    }
}
