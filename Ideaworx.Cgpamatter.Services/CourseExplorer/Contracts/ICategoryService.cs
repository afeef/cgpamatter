﻿using Ideaworx.Cgpamatter.Entities.CourseExplorer;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts
{
    public interface ICategoryService: IService<Category>
    {
    }
}
