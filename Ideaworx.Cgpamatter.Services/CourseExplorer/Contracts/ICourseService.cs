﻿using System.Collections.Generic;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts
{
    public interface ICourseService : IService<Course>
    {
        IList<Course> FindByCategoryId(long categoryId);
        IList<Course> FindByRecentCourses(int items);
        Course FindCourseDetail(long key);
    }
}
