﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Web.ViewModels;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts
{
    public interface IQuestionService: IService<Question>
    {
        IList<Question> FindBySyllabusId(long syllabusId);
        PagedViewModel<Question> FindBySyllabusId(long syllabusId, GridSortOptions options, string searchWord, int page, int pageSize, ViewDataDictionary viewData);
        void Save(Question question, IList<Answer> answers);
        void Update(Question question, IList<Answer> answers);
        Question FindQuestionAndAnswersBy(long key);
        IList<Question> GetRandomQuestions(int count);
        IList<Question> GetRandomQuestions(int count, long courseId);
        Question Load(long examId, int index);
    }
}
