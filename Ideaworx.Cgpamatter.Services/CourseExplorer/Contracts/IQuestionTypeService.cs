﻿using Ideaworx.Cgpamatter.Entities.CourseExplorer;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts
{
    public interface IQuestionTypeService : IService<QuestionType>
    {
    }
}
