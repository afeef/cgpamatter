﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Web.ViewModels;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts
{
    public interface ISyllabusService: IService<Syllabus>
    {
        IList<Syllabus> FindByCourseId(long courseId);
        PagedViewModel<Syllabus> FindByCourseId(long courseId, GridSortOptions options, string searchWord, int page, int pageSize, ViewDataDictionary viewData);
    }
}
