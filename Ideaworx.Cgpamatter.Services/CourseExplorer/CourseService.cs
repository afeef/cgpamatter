﻿using System.Collections.Generic;
using System.Linq;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using NHibernate;
using NHibernate.Linq;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer
{
    public class CourseService : BaseService<Course>, ICourseService
    {
        public CourseService(ISession session)
            : base(session)
        {
        }

        public IList<Course> FindByCategoryId(long categoryId)
        {
            return this.Session.QueryOver<Course>().Where(x => x.Category.Key == categoryId).List();
        }

        public IList<Course> FindByRecentCourses (int items)
        {
            return this.Session.Query<Course>().Select(c => c).Take(items).ToList();
        }

        public Course FindCourseDetail(long key)
        {            
            var course = this.Session.Query<Course>()
                .FetchMany(c => c.SyllabusList)
                .SingleOrDefault(c => c.Key == key);

            return course;
        }
    }
}
