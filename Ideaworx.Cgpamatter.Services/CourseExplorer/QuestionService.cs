﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Entities.ExamEngine;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Services.Extentions;
using Ideaworx.Cgpamatter.Web.ViewModels;
using Ideaworx.Cgpamatter.Web.ViewModels.ExamEngine;
using MvcContrib.UI.Grid;
using NHibernate;
using NHibernate.Linq;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer
{
    public class QuestionService : BaseService<Question>, IQuestionService
    {
        public QuestionService(ISession session)
            : base(session)
        {
        }

        public IList<Question> FindBySyllabusId(long syllabusId)
        {
            var query = from s in this.Session.QueryOver<Question>()
                        where s.Syllabus.Key == syllabusId
                        select s;

            return query.List();
        }

        public PagedViewModel<Question> FindBySyllabusId(long syllabusId, GridSortOptions options, string searchWord, int page, int pageSize, ViewDataDictionary viewData)
        {
            var query = this.GetQueryable();

            var pagedViewModel = new PagedViewModel<Question>
            {
                Query = query,
                GridSortOptions = options,
                Page = page,
                PageSize = pageSize,
                ViewData = viewData
            };

            if (syllabusId > 0)
                pagedViewModel.AddFilter(a => a.Syllabus.Key == syllabusId);

            pagedViewModel.AddFilter("searchWord", searchWord, a => a.Text.Contains(searchWord));
            pagedViewModel.Setup();

            return pagedViewModel;
        }

        public void Save(Question question, IList<Answer> answers)
        {
            using (this.Transaction = this.Session.BeginTransaction())
            {
                question.DateCreated = DateTime.Now;
                question.DateModified = DateTime.Now;

                this.Session.SaveOrUpdate(question);

                foreach (var answer in answers)
                {
                    answer.Question = question;
                    answer.DateCreated = DateTime.Now;
                    answer.DateModified = DateTime.Now;

                    this.Session.SaveOrUpdate(answer);
                }

                this.Transaction.Commit();
            }
        }


        public void Update(Question question, IList<Answer> answers)
        {
            using (this.Transaction = this.Session.BeginTransaction())
            {
                //question.DateCreated = DateTime.Now;
                question.DateModified = DateTime.Now;

                this.Session.SaveOrUpdate(question);

                foreach (var answer in answers)
                {
                    answer.Question = question;
                    //answer.DateCreated = DateTime.Now;
                    answer.DateModified = DateTime.Now;

                    this.Session.Update(answer);
                }

                this.Transaction.Commit();
            }
        }


        public Question FindQuestionAndAnswersBy(long key)
        {
            var question = this.Session.Query<Question>()
                .Fetch(q => q.Answers)
                .SingleOrDefault(q => q.Key == key);

            return question;
        }

        public IList<Question> GetRandomQuestions(int count)
        {
            return Session
              .QueryOver<Question>()
              .OrderByRandom()
              .Take(count)
              .List();
        }

        public IList<Question> GetRandomQuestions(int count, long courseId)
        {
            Question q = null;
            Syllabus s = null;
            Course c = null;

            return Session.QueryOver<Question>(() => q)
                .JoinAlias(() => q.Syllabus, () => s)
                .JoinAlias(() => s.Course, () => c)
                .Where(() => c.Key == courseId)
                .OrderByRandom()
                .Take(count)
                .List();


        }


        public Question Load(long examId, int index)
        {
            Question q = null;
            Exam e = null;

            var question = this.Session.QueryOver<Question>(() => q)
                .JoinAlias(() => q.Exams, () => e)
                .Where(() => e.Key == examId)
                .Skip(index)
                .Take(1)
                .SingleOrDefault();

            return question;
        }
    }
}
