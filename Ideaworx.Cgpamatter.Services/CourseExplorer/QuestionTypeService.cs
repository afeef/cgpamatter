﻿using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using NHibernate;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer
{
    public class QuestionTypeService: BaseService<QuestionType>, IQuestionTypeService
    {
        public QuestionTypeService(ISession session) 
            : base(session)
        {

        }
    }
}
