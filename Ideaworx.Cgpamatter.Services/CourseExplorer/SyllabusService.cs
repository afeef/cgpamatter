﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Web.ViewModels;
using MvcContrib.UI.Grid;
using NHibernate;

namespace Ideaworx.Cgpamatter.Services.CourseExplorer
{
    public class SyllabusService : BaseService<Syllabus>, ISyllabusService
    {
        public SyllabusService(ISession session)
            : base(session)
        {
        }

        public IList<Syllabus> FindByCourseId(long courseId)
        {
            var query = from s in this.Session.QueryOver<Syllabus>()
                        where s.Course.Key == courseId
                        select s;

            return query.List();
        }

        public PagedViewModel<Syllabus> FindByCourseId(long courseId, GridSortOptions options, string searchWord, int page, int pageSize, ViewDataDictionary viewData)
        {
            var query = this.GetQueryable();

            var pagedViewModel = new PagedViewModel<Syllabus>
            {
                Query = query,
                GridSortOptions = options,
                Page = page,
                PageSize = pageSize,
                ViewData = viewData
            };
           
            if (courseId > 0)
                pagedViewModel.AddFilter(a => a.Course.Key == courseId);

            pagedViewModel.AddFilter("searchWord", searchWord, a => a.SyllabusText.Contains(searchWord));
            pagedViewModel.Setup();

            return pagedViewModel;
        }
    }
}