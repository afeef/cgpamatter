﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ideaworx.Cgpamatter.Entities.ExamEngine;

namespace Ideaworx.Cgpamatter.Services.ExamEngine.Contracts
{
    public interface IExamAttemptService : IService<ExamAttempt>
    {

        ExamAttempt GetAttemptResult(long attemptId);
    }
}
