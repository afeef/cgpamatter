﻿using System.Collections.Generic;
using System.Linq;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Entities.ExamEngine;
using Ideaworx.Cgpamatter.Services.ExamEngine.Contracts;
using NHibernate;
using NHibernate.Linq;

namespace Ideaworx.Cgpamatter.Services.ExamEngine
{
    public class ExamAttemptService: BaseService<ExamAttempt>, IExamAttemptService
    {
        public ExamAttemptService(ISession session)
            : base(session)
        {

        }

        public ExamAttempt GetAttemptResult(long attemptId)
        {
            var attempt = this.Session.Query<ExamAttempt>()
                .FetchMany(ea => ea.Answers).SingleOrDefault(ea => ea.Key == attemptId);

            return attempt;
        }        
    }
}
