﻿using Ideaworx.Cgpamatter.Entities.ExamEngine;
using Ideaworx.Cgpamatter.Services.ExamEngine.Contracts;
using NHibernate;

namespace Ideaworx.Cgpamatter.Services.ExamEngine
{
    public class ExamService: BaseService<Exam>, IExamService
    {
        public ExamService(ISession session)
            : base(session)
        {
        }
    }
}
