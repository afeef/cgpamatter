﻿namespace Ideaworx.Cgpamatter.Services
{
    /// <summary>
    /// Base interface for services that are instantiated per unit of work (i.e. web request).
    /// </summary>
    public interface IDependency
    {
    }
}
