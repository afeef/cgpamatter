﻿// Copyright (c) Microsoft Open Technologies, Inc. All rights reserved. See License.txt in the project root for license information.

using System.ComponentModel;

namespace Ideaworx.Cgpamatter.Services.OAuth
{
    /// <summary>
    /// Defines Start() method that gets executed when this assembly is loaded by ASP.NET
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class PreApplicationStartCode
    {
        /// <summary>
        /// Register global namepace imports for this assembly 
        /// </summary>
        public static void Start()
        {            
            // Disable the "calls home" feature of DNOA
            DotNetOpenAuth.Reporting.Enabled = false;
        }
    }
}