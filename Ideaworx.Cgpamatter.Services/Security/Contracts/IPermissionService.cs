﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ideaworx.Cgpamatter.Entities.Security;

namespace Ideaworx.Cgpamatter.Services.Security.Contracts
{
    public interface IPermissionService:  IService<Permission>
    {
    }
}
