﻿using System.Collections.Generic;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Web.ViewModels.Security;

namespace Ideaworx.Cgpamatter.Services.Security.Contracts
{
    public interface IRoleService: IService<Role>
    {        
        void AddUsersToRoles(IList<User> users, IList<Role> roles);
        void AddUserToRole(User user, string roleName);
        void CreateRole(string roleName);
        bool DeleteRole(string roleName, bool throwOnPopulatedRole);       
        IList<Role> GetRolesForUser(string userName);
        IList<User> GetUsersInRole(string roleName);
        IList<User> GetUsersInRole(string roleName, string usernameToMatch);
        RoleViewModel GetUsersInRole(int roleId);
        bool IsUserInRole(string userName, string roleName);
        void RemoveUsersFromRoles(string[] userNames, string[] roleNames);
        bool RoleExists(string roleName);                        
        Role FindBy(string roleName);                        
        bool Enabled { get;}
        new void Save(Role role);
        new void Update(Role role);
        IEnumerable<string> FindUserNamesByRole(int key);
        IEnumerable<string> FindByUser(User user);
        void RemoveFromAllRoles(User user);
        void RemoveFromRole(User user, string roleName);
    }
}
