﻿using System.Net.Mail;

namespace Ideaworx.Cgpamatter.Services.Security.Contracts
{
    public interface ISmtpClientService
    {
        void Send(MailMessage mailMessage);
    }
}
