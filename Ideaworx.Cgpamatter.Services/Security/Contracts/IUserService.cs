﻿using System.Collections.Generic;
using DotNetOpenAuth.AspNet;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Services.OAuth;
using Ideaworx.Cgpamatter.Web.ViewModels.Security;

namespace Ideaworx.Cgpamatter.Services.Security.Contracts
{
    public interface IUserService : IService<User>
    {
        DetailsViewModel UserDetails(User user, IDictionary<Role, bool> roles);

        User FindBy(string userName);

        User FindBy(string userName, string password);

        User FindBy(long userId, string password);

        IList<User> FindByUserName(string search, int i, int pageSize);

        User FindByEmail(string email);

        IList<User> FindByEmail(string search, int i, int pageSize);

        User FindByToken(string token);
        User FindByPasswordVerificationToken(string token);
        
        int GetNumberOfUsersOnline();


        string GetUserNameByEmail(string email);

        bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion,
                                             string newPasswordAnswer);


        User Create(CreateUserViewModel model);

        bool DeleteUser(string username, bool deleteAllRelatedData);

        bool UnlockUser(string userName);

        void Unlock(int id);

        bool ValidateUser(string username, string password);

        bool ResetPassword(string newPassword, string confirmPassword, string token);

        User SignIn(string userName, string password, bool createPersistentCookie = false);
        User SignInOAuth(string providerName, string providerUserId, bool createPersistentCookie);
        void SignOut();

        string CreateAccount(string userName, string password, bool requireConfirmationToken = false);

        string CreateUserAndAccount(string userName, string password, object propertyValues = null,
                                    bool requireConfirmationToken = false);

        bool Login(string userName, string password, bool persistCookie = false);
        
        bool HasLocalAccount(long p);

        ICollection<OAuthAccount> GetAccountsFromUserName(string userName);
        ICollection<OAuthAccount> GetAccountsForUser(string userName);

        void DeleteOAuthAccount(string provider, string providerUserId);

        bool ChangePassword(string username, string oldPassword, string newPassword);

        string PasswordToken();

        bool ValidateEmail(string emailString);
        
        long GetUserId(string userName);

        long GetUserIdFromOAuth(string openAuthProvider, string openAuthId);

        string GetUserNameFromId(long userId);

        string GetUserNameFromOpenAuth(string openAuthProvider, string openAuthId);

        string GetUserName(string provider, string providerUserId);

        void CreateOrUpdateAccount(string provider, string providerUserId, string name);

        AuthenticationResult VerifyAuthentication(string returnUrl);

        string SerializeProviderUserId(string providerName, string providerUserId);

        AuthenticationClientData GetOAuthClientData(string providerName);

        bool TryDeserializeProviderUserId(string data, out string providerName, out string providerUserId);

        ICollection<AuthenticationClientData> RegisteredClientData { get; }

        void RequestAuthentication(string provider, string returnUrl);

        string GetOAuthTokenSecret(string token);

        void ReplaceOAuthRequestTokenWithAccessToken(string requestToken, string accessToken, string accessTokenSecret);

        void StoreOAuthRequestToken(string requestToken, string requestTokenSecret);

        void RegisterMicrosoftClient(string clientId, string clientSecret);

        void RegisterTwitterClient(string consumerKey, string consumerSecret);

        void RegisterFacebookClient(string appId, string appSecret);

        void RegisterGoogleClient();
    }
}
