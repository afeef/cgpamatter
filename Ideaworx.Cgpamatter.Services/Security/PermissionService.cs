﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using NHibernate;

namespace Ideaworx.Cgpamatter.Services.Security
{
    public class PermissionService: BaseService<Permission>, IPermissionService
    {
        public PermissionService(ISession session) 
            : base(session)
        {
        }
    }
}
