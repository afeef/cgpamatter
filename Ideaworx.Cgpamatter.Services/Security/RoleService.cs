﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.ViewModels.Security;
using NHibernate;
using NHibernate.Linq;

namespace Ideaworx.Cgpamatter.Services.Security
{
    public class RoleService : BaseService<Role>, IRoleService
    {
        private readonly IUserService _userService;

        public RoleService(IUserService userService, ISession session)
            : base(session)
        {
            this._userService = userService;
        }

        public void AddUsersToRoles(IList<User> users, IList<Role> roles)
        {
            foreach (var role in roles)
            {
                var r = role;

                foreach (var user in users.Where(user => !this.IsUserInRole(user.UserName, r.LoweredRoleName)))
                    role.Users.Add(user);

                this.Save(role);
            }
        }

        public void AddUserToRole(User user, string roleName)
        {
            var role = this.FindBy(roleName);

            role.Users.Add(user);

            this.Save(role);
        }

        public void CreateRole(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
                throw new Exception("role name cannot be empty or null.");

            if (this.RoleExists(roleName))
                throw new Exception("role name already exists.");

            var role = new Role()
            {
                RoleName = roleName,
                LoweredRoleName = roleName.ToLower()
            };

            this.Save(role);
        }

        public bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if (throwOnPopulatedRole && this.GetUsersInRole(roleName).Count > 0)
                throw new Exception("Cannot delete a populated role.");

            var role = this.FindBy(roleName);

            if (role != null)
                return this.Delete(role.Key);

            throw new Exception("role does not exist.");
        }

        public IList<User> GetUsersInRole(string roleName, string usernameToMatch)
        {
            var users = from u in Session.Query<User>()
                        where u.UserName == usernameToMatch
                              && u.Roles.Contains((from r in Session.Query<Role>()
                                                   where r.LoweredRoleName == roleName
                                                   select r).SingleOrDefault())
                        select u;

            return users.ToList();
        }

        public IList<Role> GetRolesForUser(string userName)
        {
            var roles = from r in Session.Query<Role>()
                        where r.Users.Contains((from u in Session.Query<User>()
                                                where u.UserName == userName
                                                select u).SingleOrDefault())
                        select r;

            return roles.ToList();
        }

        public IList<User> GetUsersInRole(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
                throw new HttpException("role name cannot be empty or null.");

            if (!RoleExists(roleName))
                throw new HttpException("role name not found.");

            var users = from u in Session.Query<User>()
                        where u.Roles.Contains((from r in Session.Query<Role>()
                                                where r.LoweredRoleName == roleName
                                                select r).SingleOrDefault())
                        select u;

            return users.ToList();
        }

        public RoleViewModel GetUsersInRole(int roleId)
        {
            var role = this.FindBy(roleId).LoweredRoleName;
            var users = this.FindUserNamesByRole(roleId).ToDictionary(k => k, v => _userService.FindBy(v));

            var roleViewModel = new RoleViewModel { Role = role, Users = users };

            return roleViewModel;
        }

        public bool IsUserInRole(string userName, string roleName)
        {
            var users = from u in Session.Query<User>()
                        where u.Roles.Contains((from r in Session.Query<Role>()
                                                where r.LoweredRoleName == roleName
                                                select r).SingleOrDefault())
                        select u;

            return users.Any();
        }

        public void RemoveUsersFromRoles(string[] userNames, string[] roleNames)
        {

            foreach (var rolename in roleNames)
            {
                if (string.IsNullOrEmpty(rolename))
                    throw new HttpException("role name cannot be empty or null.");
                if (!RoleExists(rolename))
                    throw new HttpException("role name not found.");
            }

            foreach (var username in userNames)
            {
                if (string.IsNullOrEmpty(username))
                    throw new HttpException("User name cannot be empty or null.");
                var username1 = username;
                if (roleNames.Any(rolename => !IsUserInRole(username1, rolename)))
                    throw new HttpException("User is not in role.");
            }

            foreach (var userName in userNames)
            {
                var user = _userService.FindBy(userName);
                foreach (var roleName in roleNames)
                {
                    var role = this.FindBy(roleName);
                    //var userRole = new UserRole { PersonId = user.PersonId, RoleId = role.Key };
                    //UserRoleService.Delete(userRole);
                }
            }

            //UserRoleService.Commit();
        }

        public bool RoleExists(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
                throw new HttpException("role name cannot be empty or null.");

            return this.FindBy(roleName) != null;
        }

        public Role FindBy(string roleName)
        {
            return this.Session.QueryOver<Role>()
                .Where(x => x.LoweredRoleName == roleName).SingleOrDefault();
        }

        public bool Enabled
        {
            get { return true; }
        }

        public new void Save(Role role)
        {
            role.LoweredRoleName = role.RoleName.ToLower();

            base.Save(role);
        }

        public new void Update(Role role)
        {
            role.LoweredRoleName = role.RoleName.ToLower();

            base.Update(role);
        }

        public IEnumerable<string> FindUserNamesByRole(int id)
        {
            var users = from u in Session.Query<User>()
                        where u.Roles.Contains((from r in Session.Query<Role>()
                                                where r.Key == id
                                                select r).SingleOrDefault())
                        select u;

            return users.Select(u => u.UserName);
        }

        public IEnumerable<string> FindByUser(User user)
        {
            return this.GetRolesForUser(user.UserName).Select(r => r.LoweredRoleName);
        }

        public void RemoveFromAllRoles(User user)
        {
            throw new NotImplementedException();
        }

        public void RemoveFromRole(User user, string roleName)
        {
            var role = this.FindBy(roleName);

            role.Users.Remove(user);

            this.Save(role);
        }
    }
}
