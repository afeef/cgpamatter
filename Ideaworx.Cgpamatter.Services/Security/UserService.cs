﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.WebPages;
using DotNetOpenAuth.AspNet;
using DotNetOpenAuth.AspNet.Clients;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Services.OAuth;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.ViewModels.Security;
using NHibernate;
using NHibernate.Linq;

namespace Ideaworx.Cgpamatter.Services.Security
{
    /// <summary>
    /// Contains APIs to manage authentication against OAuth &amp; OpenID service providers
    /// </summary>
    public class UserService : BaseService<User>, IUserService, IOpenAuthDataProvider, IOAuthTokenManager
    {

        // contains all registered authentication clients
        private readonly Dictionary<string, AuthenticationClientData> _authenticationClients =
            new Dictionary<string, AuthenticationClientData>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Gets a value indicating whether the current user is authenticated by an OAuth provider.
        /// </summary>
        //public static bool IsAuthenticatedWithOAuth
        //{
        //    get
        //    {
        //        if (HttpContext.Current == null)
        //        {
        //            throw new InvalidOperationException("HttpContextNotAvailable");
        //        }

        //        return GetIsAuthenticatedWithOAuthCore(new HttpContextWrapper(HttpContext.Current));
        //    }
        //}

        public UserService(ISession session)
            : base(session)
        {
            this.RegisterMicrosoftClient(
                clientId: "000000004C0E2909",
                clientSecret: "7bsz-OGU7bWNDd4QqxRCC5hakQaSHyue");

            this.RegisterTwitterClient(
                consumerKey: "Q1dz1MwFULNbwJAMZRUIUg",
                consumerSecret: "1TsL4nJrcHCNNWhodvnWMLMp144rp3MzCNiQCxFw");

            this.RegisterFacebookClient(
                appId: "349637575139038",
                appSecret: "3a87bf3258358284a6953afb35743f6a");

            this.RegisterLinkedInClient(
                consumerKey: "xz0uih9i1v3r",
                consumerSecret: "9OIU3t3bqLqmIJOi");

            this.RegisterYahooClient();
            this.RegisterGoogleClient();
        }

        public User FindBy(string userName)
        {
            return (from u in this.Session.Query<User>()
                    where u.UserName == userName
                    select u).SingleOrDefault();
        }

        public User FindBy(long userId, string password)
        {
            //var user = this.Session.Query<User>()
            //              .FetchMany(u => u.Roles)
            //              .ThenFetchMany(r => r.Permissions)
            //              .Where(u => u.Key == userId && u.Password == password)
            //              .SingleOrDefault(u => u.Key == userId);

            var user = (from u in this.Session.Query<User>()
                        where u.Key == userId && u.Password == password
                        select u).SingleOrDefault();

            return user;
        }

        public User FindBy(string userName, string password)
        {
            var user = this.Session.Query<User>()
                           .FetchMany(u => u.Roles)
                           .ThenFetchMany(r => r.Permissions)
                           .Where(u => u.UserName == userName)
                           .SingleOrDefault(u => u.UserName == userName);

            if (user == null)
                return null;

            var isCorrectPwd = Crypto.VerifyHashedPassword(user.Password, password);

            return isCorrectPwd ? user : null;
        }

        public User FindByToken(string token)
        {
            var user = this.Session.Query<User>().SingleOrDefault(u => u.ConfirmationToken == token);

            return user;
        }

        public User FindByPasswordVerificationToken(string token)
        {
            var user = this.Session.Query<User>().SingleOrDefault(u => u.PasswordVerificationToken == token);

            return user;
        }

        public User FindByOAuthAccount(string providerName, string providerUserId)
        {

            var p1 = this.Session.Query<OAuthAccount>()
                             .Where(a => a.Provider == providerName && a.ProviderUserId == providerUserId);

            p1.Fetch(a => a.User).ToFuture();

            this.Session.Query<User>()
                .FetchMany(a => a.Roles)
                .ThenFetchMany(a => a.Permissions).ToFuture();

            var account = p1.ToFuture().SingleOrDefault();

            //int projectId = 1;
            //var p1 = sess.Query<Project>().Where(x => x.ProjectId == projectId);


            //p1.FetchMany(x => x.Partners).ToFuture();

            //sess.Query<Partner>()
            //.Where(x => x.Project.ProjectId == projectId)
            //.FetchMany(x => x.Costs)
            //    .ThenFetch(x => x.Total)
            //.ToFuture();

            //sess.Query<Partner>()
            //.Where(x => x.Project.ProjectId == projectId)
            //.FetchMany(x => x.Addresses)
            //.ToFuture();


            //Project p = p1.ToFuture().Single();

            //var user = this.Session.Query<OAuthAccount>()
            //                 .Where(a => a.Provider == providerName && a.ProviderUserId == providerUserId)
            //                 .Select(a => a.User)
            //                 .FetchMany(a => a.Roles)
            //                 .ThenFetchMany(a => a.Permissions)

            //                 .SingleOrDefault();

            //var query = from a in this.Session.Query<OAuthAccount>()                        
            //            where a.Provider == providerName && a.ProviderUserId == providerUserId

            //            select a.User;

            //var user = query.FetchMany(u => u.Roles)
            //                .ThenFetchMany(r => r.Permissions)
            //                .SingleOrDefault();

            return account != null
                ? account.User
                : null;
        }

        public IList<User> FindByUserName(string search, int startRowIndex, int pageSize)
        {
            return this.Session.Query<User>()
                .Where(u => u.UserName == search)
                .Skip(startRowIndex)
                .Take(pageSize)
                .ToList();
        }

        public User FindByEmail(string email)
        {
            return this.Session.Query<User>().SingleOrDefault(u => u.Email == email);
        }

        public IList<User> FindByEmail(string search, int startRowIndex, int pageSize)
        {
            return this.Session.Query<User>()
                .Where(u => u.Email == search)
                .Skip(startRowIndex)
                .Take(pageSize)
                .ToList();
        }

        #region MembershipService Implementation



        public bool ChangePasswordQuestionAndAnswer(string username, string password, string passwordQuestion,
                                                    string passwordAnswer)
        {
            if (string.IsNullOrEmpty(passwordAnswer))
                throw new ArgumentException("New Password cannot be null or empty");

            var user = this.FindBy(username, password);

            if (user == null)
                return false;

            user.PasswordQuestion = passwordQuestion;
            user.PasswordAnswer = passwordAnswer;

            this.Update(user);

            return true;
        }


        public bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            var user = this.FindBy(username);

            if (user == null)
                throw new Exception("username does not exist.");

            return this.Delete(user.Key);
        }



        public int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }




        public string GetUserNameByEmail(string email)
        {
            var user = this.FindByEmail(email);

            return user != null ? user.UserName : null;
        }

        public bool UnlockUser(string userName)
        {
            var user = this.FindBy(userName);

            if (user == null)
                return false;

            user.IsActive = true;
            this.Update(user);

            return user.IsActive;
        }

        public bool ValidateUser(string username, string password)
        {
            var user = this.FindBy(username, password);
            return user != null && user.IsActive;
        }

        public User Create(CreateUserViewModel model)
        {
            var user = new User()
                           {
                               UserName = model.Username,
                               Password = model.Password,
                               Email = model.Email,
                               PasswordQuestion = model.PasswordQuestion,
                               PasswordAnswer = model.PasswordAnswer,
                               IsActive = true,
                               IsApproved = true,
                               LastActivityDate = DateTime.Now,
                               LastLockoutDate = DateTime.Now,
                               LastLoginDate = DateTime.Now,
                               LastPasswordChangedDate = DateTime.Now
                           };

            this.Save(user);

            return user;
        }

        public void Unlock(int id)
        {
            var user = this.FindBy(id);

            user.IsLockedOut = false;

            this.Save(user);
        }

        #endregion





        public User SignIn(string userName, string password, bool createPersistentCookie = false)
        {
            var user = this.FindBy(userName, password);

            if (user != null && user.IsActive)
            {
                FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);

                // Copying the username into the email
                user.Email = user.UserName;

                user.LastActivityDate = DateTime.Now;

                this.Save(user);

                return user;
            }

            return null;
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        public DetailsViewModel UserDetails(User user, IDictionary<Role, bool> roles)
        {
            var status = user.IsOnline
                             ? DetailsViewModel.StatusEnum.Online
                             : !user.IsApproved
                                   ? DetailsViewModel.StatusEnum.Unapproved
                                   : user.IsLockedOut
                                         ? DetailsViewModel.StatusEnum.LockedOut
                                         : DetailsViewModel.StatusEnum.Offline;

            var detailsViewModel = new DetailsViewModel
                                       {
                                           //CanResetPassword = this.EnablePasswordRetrieval,
                                           //RequirePasswordQuestionAnswerToResetPassword = this.RequiresQuestionAndAnswer,
                                           DisplayName = user.UserName,
                                           User = user,
                                           Roles = roles,
                                           IsRolesEnabled = true,
                                           Status = status
                                       };

            return detailsViewModel;
        }

        public string PasswordToken()
        {
            string token = null;
            token = GenerateToken();

            //this.Save(user);
            return token;
        }


        public bool ResetPassword(string newPassword, string confirmPassword, string token)
        {
            if (newPassword.IsEmpty())
                throw new ArgumentException("Argument_Cannot_Be_Null_Or_Empty", "newPassword");

            if (confirmPassword.IsEmpty())
                throw new ArgumentException("Argument_Cannot_Be_Null_Or_Empty", "confirmPassword");


            var hashedPassword = Crypto.HashPassword(newPassword);

            if (hashedPassword.Length > 128)
                throw new ArgumentException("SimpleMembership_PasswordTooLong");

            // Update new password

            var user = this.FindByPasswordVerificationToken(token);

            user.Password = hashedPassword;
            user.PasswordChangedDate = DateTime.UtcNow;
            user.PasswordVerificationToken = "";
            this.Save(user);

            return true;

        }


        public string CreateUserAndAccount(string userName, string password, object propertyValues = null,
                                           bool requireConfirmationToken = false)
        {
            IDictionary<string, object> values = null;

            if (propertyValues != null)
                values = new RouteValueDictionary(propertyValues);

            return this.CreateUserAndAccount(userName, password, requireConfirmationToken, values);
        }

        public string CreateUserAndAccount(string userName, string password, bool requireConfirmation,
                                           IDictionary<string, object> values)
        {

            // Make sure user doesn't exist            
            //var userId = this.GetUserId(userName);

            //if (userId != -1)
            //    throw new MembershipCreateUserException(MembershipCreateStatus.DuplicateUserName);

            //var user = new User { UserName = userName };

            //this.Save(user);

            //if (values != null)
            //    // save these values into the user table as well
            //    throw new ArgumentException("AJ: what to do with these values?");

            return this.CreateAccount(userName, password, requireConfirmation);

        }

        public string CreateAccount(string userName, string password, bool requireConfirmationToken = false)
        {
            var hashedPassword = Crypto.HashPassword(password);

            if (hashedPassword.Length > 128)
                throw new MembershipCreateUserException(MembershipCreateStatus.InvalidPassword);

            // Step 1: Check if the user exists in the Users table
            var userId = this.GetUserId(userName);

            if (userId != -1)
                throw new MembershipCreateUserException(MembershipCreateStatus.DuplicateUserName);

            // Step 3: Create user in Membership table
            string token = null;
            object dbtoken = DBNull.Value;

            if (requireConfirmationToken)
            {
                token = GenerateToken();
                dbtoken = token;
            }

            const int defaultNumPasswordFailures = 0;

            var user = new User()
                           {
                               UserName = userName,
                               Password = hashedPassword,
                               PasswordSalt = string.Empty,
                               IsConfirmed = !requireConfirmationToken,
                               ConfirmationToken = dbtoken.ToString(),
                               PasswordChangedDate = DateTime.UtcNow,
                               PasswordFailuresSinceLastSuccess = defaultNumPasswordFailures,
                               IsActive = true,
                               IsApproved = true,
                               LastActivityDate = DateTime.Now,
                               LastLockoutDate = DateTime.Now,
                               LastLoginDate = DateTime.Now,
                               LastPasswordChangedDate = DateTime.Now
                           };

            this.Save(user);

            return token;
        }

        // Email Verification Regular Expression

        public bool ValidateEmail(string emailString)
        {
            if (Regex.IsMatch(emailString, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z"))
            {
                return true;
            }

            return false;
        }

        private string GenerateToken()
        {
            using (var prng = new RNGCryptoServiceProvider())
            {
                return GenerateToken(prng);
            }
        }

        private const int TokenSizeInBytes = 16;

        internal static string GenerateToken(RandomNumberGenerator generator)
        {
            var tokenBytes = new byte[TokenSizeInBytes];
            generator.GetBytes(tokenBytes);
            return HttpServerUtility.UrlTokenEncode(tokenBytes);
        }

        /// <summary>
        /// Determines whether there exists a local account (as opposed to OAuth account) with the specified userId.
        /// </summary>
        /// <param name="userId">The user id to check for local account.</param>
        /// <returns>
        ///   <c>true</c> if there is a local account with the specified user id]; otherwise, <c>false</c>.
        /// </returns>
        public bool HasLocalAccount(long userId)
        {
            var user = this.FindBy(userId);

            return user != null;
        }



        public void DeleteOAuthAccount(string provider, string providerUserId)
        {
            using (this.Transaction = this.Session.BeginTransaction())
            {
                var query = from a in this.Session.Query<OAuthAccount>()
                            where a.Provider == provider && a.ProviderUserId == providerUserId
                            select a;

                var dp = query.SingleOrDefault();

                this.Session.Delete(dp);

                this.Transaction.Commit();
            }
        }

        public long GetUserId(string userName)
        {
            var user = this.FindBy(userName);

            return user != null ? user.Key : -1;
        }

        // Inherited from MembershipProvider ==> Forwarded to previous provider if this provider hasn't been initialized
        public bool ChangePassword(string username, string oldPassword, string newPassword)
        {

            // REVIEW: are commas special in the password?
            if (username.IsEmpty())
                throw new ArgumentException("Argument_Cannot_Be_Null_Or_Empty", "username");

            if (oldPassword.IsEmpty())
                throw new ArgumentException("Argument_Cannot_Be_Null_Or_Empty", "oldPassword");

            if (newPassword.IsEmpty())
                throw new ArgumentException("Argument_Cannot_Be_Null_Or_Empty", "newPassword");

            var userId = GetUserId(username);

            if (userId == -1)
                return false; // User not found

            // First check that the old credentials match
            if (!this.CheckPassword(userId, oldPassword))
                return false;

            return this.SetPassword(userId, newPassword);

        }

        private bool CheckPassword(long userId, string password)
        {
            var hashedPassword = GetHashedPassword(userId);
            var verificationSucceeded = (hashedPassword != null && Crypto.VerifyHashedPassword(hashedPassword, password));

            if (verificationSucceeded)
            // Reset password failure count on successful credential check
            {
                var user = this.FindBy(userId);

                user.PasswordFailuresSinceLastSuccess = 0;

                this.Save(user);
            }
            else
            {
                var failures = this.GetPasswordFailuresSinceLastSuccess(userId);

                if (failures != -1)
                {
                    var user = this.FindBy(userId);

                    user.PasswordFailuresSinceLastSuccess = failures + 1;
                    user.LastPasswordFailureDate = DateTime.UtcNow;

                    this.Save(user);
                }
            }

            return verificationSucceeded;
        }

        private int GetPasswordFailuresSinceLastSuccess(long userId)
        {
            var query = from u in this.Session.Query<User>()
                        where u.Key == userId
                        select u.PasswordFailuresSinceLastSuccess;

            return query.SingleOrDefault();
        }

        private string GetHashedPassword(long userId)
        {
            var query = from u in this.Session.Query<User>()
                        where u.Key == userId
                        select u.Password;

            return query.SingleOrDefault();
        }

        private bool SetPassword(long userId, string newPassword)
        {
            var hashedPassword = Crypto.HashPassword(newPassword);

            if (hashedPassword.Length > 128)
                throw new ArgumentException("SimpleMembership_PasswordTooLong");

            // Update new password

            var user = this.FindBy(userId);

            user.Password = hashedPassword;
            user.PasswordSalt = string.Empty;
            user.PasswordChangedDate = DateTime.UtcNow;

            this.Save(user);

            return true;
        }














        /// <summary>
        /// Gets the collection of all registered authentication client;
        /// </summary>
        /// <returns></returns>
        public ICollection<AuthenticationClientData> RegisteredClientData
        {
            get
            {
                // the Values property returns a read-only collection.
                // so we don't need to worry about clients of this method modifying our internal collection.
                return _authenticationClients.Values;
            }
        }

        /// <summary>
        /// Registers the Facebook client.
        /// </summary>
        /// <param name="appId">The app id.</param>
        /// <param name="appSecret">The app secret.</param>
        public void RegisterFacebookClient(string appId, string appSecret)
        {
            RegisterFacebookClient(appId, appSecret, displayName: "Facebook");
        }

        /// <summary>
        /// Registers the Facebook client.
        /// </summary>
        /// <param name="appId">The app id.</param>
        /// <param name="appSecret">The app secret.</param>
        /// <param name="displayName">The display name of the client.</param>
        public void RegisterFacebookClient(string appId, string appSecret, string displayName)
        {
            RegisterFacebookClient(appId, appSecret, displayName, extraData: new Dictionary<string, object>());
        }

        /// <summary>
        /// Registers the Facebook client.
        /// </summary>
        /// <param name="appId">The app id.</param>
        /// <param name="appSecret">The app secret.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="extraData">The data bag used to store extra data about this client</param>
        public void RegisterFacebookClient(string appId, string appSecret, string displayName,
                                           IDictionary<string, object> extraData)
        {
            RegisterClient(new FacebookClient(appId, appSecret), displayName, extraData);
        }

        /// <summary>
        /// Registers the Microsoft account client.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="clientSecret">The client secret.</param>
        public void RegisterMicrosoftClient(string clientId, string clientSecret)
        {
            RegisterMicrosoftClient(clientId, clientSecret, displayName: "Microsoft");
        }

        /// <summary>
        /// Registers the Microsoft account client.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="clientSecret">The client secret.</param>
        /// <param name="displayName">The display name.</param>
        public void RegisterMicrosoftClient(string clientId, string clientSecret, string displayName)
        {
            RegisterMicrosoftClient(clientId, clientSecret, displayName, new Dictionary<string, object>());
        }

        /// <summary>
        /// Registers the Microsoft account client.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="clientSecret">The client secret.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="extraData">The data bag used to store extra data about this client</param>
        public void RegisterMicrosoftClient(string clientId, string clientSecret, string displayName,
                                            IDictionary<string, object> extraData)
        {
            RegisterClient(new MicrosoftClient(clientId, clientSecret), displayName, extraData);
        }

        /// <summary>
        /// Registers the Twitter client.
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        public void RegisterTwitterClient(string consumerKey, string consumerSecret)
        {
            RegisterTwitterClient(consumerKey, consumerSecret, displayName: "Twitter");
        }

        /// <summary>
        /// Registers the Twitter client.
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        /// <param name="displayName">The display name.</param>
        public void RegisterTwitterClient(string consumerKey, string consumerSecret, string displayName)
        {
            RegisterTwitterClient(consumerKey, consumerSecret, displayName, new Dictionary<string, object>());
        }

        /// <summary>
        /// Registers the Twitter client.
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="extraData">The data bag used to store extra data about this client</param>
        public void RegisterTwitterClient(string consumerKey, string consumerSecret, string displayName,
                                          IDictionary<string, object> extraData)
        {
            var twitterClient = new TwitterClient(consumerKey, consumerSecret);
            RegisterClient(twitterClient, displayName, extraData);
        }

        /// <summary>
        /// Registers the LinkedIn client.
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        public void RegisterLinkedInClient(string consumerKey, string consumerSecret)
        {
            RegisterLinkedInClient(consumerKey, consumerSecret, displayName: "LinkedIn");
        }

        /// <summary>
        /// Registers the LinkedIn client.
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        /// <param name="displayName">The display name.</param>
        public void RegisterLinkedInClient(string consumerKey, string consumerSecret, string displayName)
        {
            RegisterLinkedInClient(consumerKey, consumerSecret, displayName, new Dictionary<string, object>());
        }

        /// <summary>
        /// Registers the LinkedIn client.
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="extraData">The data bag used to store extra data about this client</param>
        public void RegisterLinkedInClient(string consumerKey, string consumerSecret, string displayName,
                                           IDictionary<string, object> extraData)
        {
            var linkedInClient = new LinkedInClient(consumerKey, consumerSecret);
            RegisterClient(linkedInClient, displayName, extraData);
        }

        /// <summary>
        /// Registers the Google client.
        /// </summary>
        public void RegisterGoogleClient()
        {
            RegisterGoogleClient(displayName: "Google");
        }

        /// <summary>
        /// Registers the Google client.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        public void RegisterGoogleClient(string displayName)
        {
            RegisterClient(new GoogleOpenIdClient(), displayName, new Dictionary<string, object>());
        }

        /// <summary>
        /// Registers the Google client.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="extraData">The data bag.</param>
        public void RegisterGoogleClient(string displayName, IDictionary<string, object> extraData)
        {
            RegisterClient(new GoogleOpenIdClient(), displayName, extraData);
        }

        /// <summary>
        /// Registers the Yahoo client.
        /// </summary>
        public void RegisterYahooClient()
        {
            RegisterYahooClient(displayName: "Yahoo");
        }

        /// <summary>
        /// Registers the Yahoo client.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        public void RegisterYahooClient(string displayName)
        {
            RegisterYahooClient(displayName, new Dictionary<string, object>());
        }

        /// <summary>
        /// Registers the Yahoo client.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="extraData">The data bag.</param>
        public void RegisterYahooClient(string displayName, IDictionary<string, object> extraData)
        {
            RegisterClient(new YahooOpenIdClient(), displayName, extraData);
        }

        /// <summary>
        /// Registers an authentication client.
        /// </summary>
        /// <param name="client">The client to be registered.</param>
        [CLSCompliant(false)]
        public void RegisterClient(IAuthenticationClient client)
        {
            RegisterClient(client, displayName: null, extraData: new Dictionary<string, object>());
        }

        /// <summary>
        /// Registers an authentication client.
        /// </summary>
        /// <param name="client">The client to be registered</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="extraData">The data bag used to store extra data about the specified client</param>
        [CLSCompliant(false)]
        public void RegisterClient(IAuthenticationClient client, string displayName,
                                   IDictionary<string, object> extraData)
        {
            if (client == null)
                throw new ArgumentNullException("client");

            if (String.IsNullOrEmpty(client.ProviderName))
                throw new ArgumentException("InvalidServiceProviderName", "client");

            if (_authenticationClients.ContainsKey(client.ProviderName))
                throw new ArgumentException("ServiceProviderNameExists", "client");

            var clientData = new AuthenticationClientData(client, displayName, extraData);

            _authenticationClients.Add(client.ProviderName, clientData);
        }

        /// <summary>
        /// Requests the specified provider to start the authentication by directing users to an external website
        /// </summary>
        /// <param name="provider">The provider.</param>
        public void RequestAuthentication(string provider)
        {
            RequestAuthentication(provider, returnUrl: null);
        }

        /// <summary>
        /// Requests the specified provider to start the authentication by directing users to an external website
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="returnUrl">The return url after user is authenticated.</param>        
        public void RequestAuthentication(string provider, string returnUrl)
        {
            if (HttpContext.Current == null)
            {
                throw new InvalidOperationException("HttpContextNotAvailable");
            }

            RequestAuthenticationCore(new HttpContextWrapper(HttpContext.Current), provider, returnUrl);
        }

        /// <summary>
        /// Requests the authentication core.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="provider">The provider.</param>
        /// <param name="returnUrl">The return URL.</param>
        internal void RequestAuthenticationCore(HttpContextBase context, string provider, string returnUrl)
        {
            IAuthenticationClient client = GetOAuthClient(provider);
            var securityManager = new OpenAuthSecurityManager(context, client, this);
            securityManager.RequestAuthentication(returnUrl);
        }

        /// <summary>
        /// Checks if user is successfully authenticated when user is redirected back to this user.
        /// </summary>
        [CLSCompliant(false)]
        public AuthenticationResult VerifyAuthentication()
        {
            return VerifyAuthentication(returnUrl: null);
        }

        /// <summary>
        /// Checks if user is successfully authenticated when user is redirected back to this user.
        /// </summary>
        /// <param name="returnUrl">The return URL which must match the one passed to RequestAuthentication earlier.</param>
        [CLSCompliant(false)]
        public AuthenticationResult VerifyAuthentication(string returnUrl)
        {
            if (HttpContext.Current == null)
                throw new InvalidOperationException("HttpContextNotAvailable");

            return VerifyAuthenticationCore(new HttpContextWrapper(HttpContext.Current), returnUrl);
        }

        internal AuthenticationResult VerifyAuthenticationCore(HttpContextBase context, string returnUrl)
        {
            var providerName = OpenAuthSecurityManager.GetProviderName(context);

            if (String.IsNullOrEmpty(providerName))
                return AuthenticationResult.Failed;

            IAuthenticationClient client;
            if (TryGetOAuthClient(providerName, out client))
            {
                var securityManager = new OpenAuthSecurityManager(context, client, this);
                return securityManager.VerifyAuthentication(returnUrl);
            }
            else
            {
                throw new InvalidOperationException("InvalidServiceProviderName");
            }
        }



        internal bool GetIsAuthenticatedWithOAuthCore(HttpContextBase context, string providerName)
        {
            var provider = GetOAuthClient(providerName);

            return new OpenAuthSecurityManager(context, provider, this).IsAuthenticatedWithOpenAuth;
        }





        /// <summary>
        /// Delete the specified OAuth &amp; OpenID account
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <param name="providerUserId">The provider user id.</param>
        public bool DeleteAccount(string providerName, string providerUserId)
        {
            var username = this.GetUserName(providerName, providerUserId);

            if (String.IsNullOrEmpty(username))
                // account doesn't exist
                return false;

            this.DeleteOAuthAccount(providerName, providerUserId);
            return true;
        }

        /// <summary>
        /// Gets the OAuth client data of the specified provider name.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns>The AuthenticationClientData of the specified provider name.</returns>
        public AuthenticationClientData GetOAuthClientData(string providerName)
        {
            if (providerName == null)
            {
                throw new ArgumentNullException("providerName");
            }

            return _authenticationClients[providerName];
        }

        /// <summary>
        /// Tries getting the OAuth client data of the specified provider name.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <param name="clientData">The client data of the specified provider name.</param>
        /// <returns><c>true</c> if the client data is found for the specified provider name. Otherwise, <c>false</c></returns>
        public bool TryGetOAuthClientData(string providerName, out AuthenticationClientData clientData)
        {
            if (providerName == null)
            {
                throw new ArgumentNullException("providerName");
            }

            return _authenticationClients.TryGetValue(providerName, out clientData);
        }

        internal IAuthenticationClient GetOAuthClient(string providerName)
        {
            if (!_authenticationClients.ContainsKey(providerName))
            {
                throw new ArgumentException("ServiceProviderNotFound", "providerName");
            }

            return _authenticationClients[providerName].AuthenticationClient;
        }

        internal bool TryGetOAuthClient(string provider, out IAuthenticationClient client)
        {
            if (_authenticationClients.ContainsKey(provider))
            {
                client = _authenticationClients[provider].AuthenticationClient;
                return true;
            }
            else
            {
                client = null;
                return false;
            }
        }

        /// <summary>
        /// for unit tests
        /// </summary>
        internal void ClearProviders()
        {
            _authenticationClients.Clear();
        }

        /// <summary>
        /// Securely serializes a providerName/providerUserId pair.
        /// </summary>
        /// <param name="providerName">The provider name.</param>
        /// <param name="providerUserId">The provider-specific user id.</param>
        /// <returns>A cryptographically protected serialization of the inputs which is suitable for round-tripping.</returns>
        /// <remarks>Do not persist the return value to permanent storage. This implementation is subject to change.</remarks>
        public string SerializeProviderUserId(string providerName, string providerUserId)
        {
            if (providerName == null)
            {
                throw new ArgumentNullException("providerName");
            }
            if (providerUserId == null)
            {
                throw new ArgumentNullException("providerUserId");
            }

            return ProviderUserIdSerializationHelper.ProtectData(providerName, providerUserId);
        }

        /// <summary>
        /// Deserializes a string obtained from <see cref="SerializeProviderUserId(string, string)"/> back into a 
        /// providerName/providerUserId pair.
        /// </summary>
        /// <param name="data">The input data.</param>
        /// <param name="providerName">Will contain the deserialized provider name.</param>
        /// <param name="providerUserId">Will contain the deserialized provider user id.</param>
        /// <returns><c>True</c> if successful.</returns>        
        public bool TryDeserializeProviderUserId(string data, out string providerName, out string providerUserId)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            return ProviderUserIdSerializationHelper.UnprotectData(data, out providerName, out providerUserId);
        }

        public bool Login(string userName, string password, bool persistCookie = false)
        {
            var user = this.FindBy(userName, password);

            if (user != null && user.IsActive)
            {
                FormsAuthentication.SetAuthCookie(userName, persistCookie);

                user.LastActivityDate = DateTime.Now;

                this.Save(user);
            }

            return user != null;
        }

        public User SignInOAuth(string providerName, string providerUserId, bool createPersistentCookie)
        {
            if (HttpContext.Current == null)
                throw new InvalidOperationException("HttpContextNotAvailable");

            var isValidOAuthAccount = LoginCore(new HttpContextWrapper(HttpContext.Current), providerName, providerUserId,
                                           createPersistentCookie);

            return isValidOAuthAccount
                ? this.FindByOAuthAccount(providerName, providerUserId)
                : null;
        }

        internal bool LoginCore(HttpContextBase context, string providerName, string providerUserId,
                                bool createPersistentCookie)
        {
            var provider = GetOAuthClient(providerName);

            var securityManager = new OpenAuthSecurityManager(context, provider, this);

            return securityManager.Login(providerUserId, createPersistentCookie);
        }


        /// <summary>
        /// Gets all OAuth &amp; OpenID accounts which are associted with the specified user name.
        /// </summary>
        /// <param name="userName">The user name.</param>
        public ICollection<OAuthAccount> GetAccountsFromUserName(string userName)
        {
            if (String.IsNullOrEmpty(userName))
                throw new ArgumentException(
                    String.Format(CultureInfo.CurrentCulture, "Argument_Cannot_Be_Null_Or_Empty", "userName"),
                    "userName");

            return
                this.GetAccountsForUser(userName).Select(p => new OAuthAccount(p.Provider, p.ProviderUserId)).ToList();
        }

        public ICollection<OAuthAccount> GetAccountsForUser(string userName)
        {

            var userId = GetUserId(userName);

            if (userId != -1)
            {
                var query = from a in this.Session.Query<OAuthAccount>()
                            where a.User.Key == userId
                            select a;

                return query.ToList();
            }

            return new OAuthAccount[0];
        }

        public string GetUserName(string providerName, string providerUserId)
        {
            return this.GetUserNameFromOpenAuth(providerName, providerUserId);
        }

        public string GetUserNameFromOpenAuth(string openAuthProvider, string openAuthId)
        {
            var userId = this.GetUserIdFromOAuth(openAuthProvider, openAuthId);

            return userId == -1 ? null : this.GetUserNameFromId(userId);
        }

        public long GetUserIdFromOAuth(string provider, string providerUserId)
        {
            var query = from a in this.Session.Query<OAuthAccount>()
                        where
                            a.Provider == provider.ToUpperInvariant() &&
                            a.ProviderUserId == providerUserId.ToUpperInvariant()
                        select a.User.Key;

            return query.SingleOrDefault();
        }

        public string GetUserNameFromId(long userId)
        {
            var query = from u in this.Session.Query<User>()
                        where u.Key == userId
                        select u.UserName;

            return query.SingleOrDefault();
        }

        /// <summary>
        /// Creates or update the account with the specified provider, provider user id and associate it with the specified user name.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <param name="providerUserId">The provider user id.</param>
        /// <param name="userName">The user name.</param>
        public void CreateOrUpdateAccount(string providerName, string providerUserId, string userName)
        {
            this.CreateOrUpdateOAuthAccount(providerName, providerUserId, userName);
        }

        public void CreateOrUpdateOAuthAccount(string provider, string providerUserId, string userName)
        {

            var user = this.FindBy(userName);

            if (user == null)
                throw new MembershipCreateUserException(MembershipCreateStatus.InvalidUserName);

            var oldUserId = this.GetUserIdFromOAuth(provider, providerUserId);


            if (oldUserId == 0)
            {

                var oAuthAccount = new OAuthAccount
                                       {
                                           Provider = provider,
                                           ProviderUserId = providerUserId,
                                           User = user
                                       };

                this.SaveOAuthAccount(oAuthAccount);
            }
            else
            {
                var query = from a in this.Session.Query<OAuthAccount>()
                            where
                                a.Provider == provider.ToUpperInvariant() &&
                                a.ProviderUserId == providerUserId.ToUpperInvariant()
                            select a;

                var oAuthAccount = query.SingleOrDefault();

                if (oAuthAccount != null)
                {
                    oAuthAccount.User = user;

                    this.SaveOAuthAccount(oAuthAccount);
                }
            }
        }

        private void SaveOAuthAccount(OAuthAccount account)
        {
            using (this.Transaction = this.Session.BeginTransaction())
            {
                account.DateCreated = DateTime.Now;
                account.DateModified = DateTime.Now;
                //entity.AccessUserId = this.User.Key;
                account.IsActive = true;

                this.Session.SaveOrUpdate(account);

                this.Transaction.Commit();
            }
        }

        /// <summary>
        /// Gets the token secret from the specified token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>
        /// The token's secret
        /// </returns>
        public string GetTokenSecret(string token)
        {
            if (String.IsNullOrEmpty(token))
                throw new ArgumentException("Argument_Cannot_Be_Null_Or_Empty", "token");

            return this.GetOAuthTokenSecret(token);
        }

        public string GetOAuthTokenSecret(string token)
        {

            var query = from t in this.Session.Query<OAuthToken>()
                        where t.Token == token
                        select t.Secret;

            return query.SingleOrDefault();
        }

        /// <summary>
        /// Replaces the request token with access token.
        /// </summary>
        /// <param name="requestToken">The request token.</param>
        /// <param name="accessToken">The access token.</param>
        /// <param name="accessTokenSecret">The access token secret.</param>
        public void ReplaceRequestTokenWithAccessToken(string requestToken, string accessToken, string accessTokenSecret)
        {
            if (String.IsNullOrEmpty(requestToken))
                throw new ArgumentException("Argument_Cannot_Be_Null_Or_Empty", "requestToken");

            if (String.IsNullOrEmpty(accessToken))
                throw new ArgumentException("Argument_Cannot_Be_Null_Or_Empty", "accessToken");

            if (String.IsNullOrEmpty(accessTokenSecret))
                throw new ArgumentException("Argument_Cannot_Be_Null_Or_Empty", "accessTokenSecret");

            this.ReplaceOAuthRequestTokenWithAccessToken(requestToken, accessToken, accessTokenSecret);
        }

        /// <summary>
        /// Replaces the request token with access token and secret.
        /// </summary>
        /// <param name="requestToken">The request token.</param>
        /// <param name="accessToken">The access token.</param>
        /// <param name="accessTokenSecret">The access token secret.</param>
        public void ReplaceOAuthRequestTokenWithAccessToken(string requestToken, string accessToken, string accessTokenSecret)
        {
            using (this.Transaction = this.Session.BeginTransaction())
            {
                var query = from t in this.Session.Query<OAuthToken>()
                            where t.Token == requestToken
                            select t;

                var dp = query.SingleOrDefault();

                this.Session.Delete(dp);

                this.Transaction.Commit();
            }

            // Although there are two different types of tokens, request token and access token,
            // we treat them the same in database records.
            this.StoreOAuthRequestToken(accessToken, accessTokenSecret);
        }

        /// <summary>
        /// Stores the request token together with its secret.
        /// </summary>
        /// <param name="requestToken">The request token.</param>
        /// <param name="requestTokenSecret">The request token secret.</param>
        public void StoreRequestToken(string requestToken, string requestTokenSecret)
        {
            if (String.IsNullOrEmpty(requestToken))
                throw new ArgumentException("Argument_Cannot_Be_Null_Or_Empty", "requestToken");

            this.StoreOAuthRequestToken(requestToken, requestTokenSecret);
        }

        public void StoreOAuthRequestToken(string requestToken, string requestTokenSecret)
        {

            var existingSecret = this.GetOAuthTokenSecret(requestToken);
            var token = new OAuthToken() { Secret = requestTokenSecret, Token = requestToken };

            if (existingSecret != null)
            {
                if (existingSecret == requestTokenSecret)
                    // the record already exists
                    return;

                // the token exists with old secret, update it to new secret                
                this.UpdateOAuthToken(token);
            }
            else
            {
                // insert new record
                this.SaveOAuthToken(token);
            }
        }

        private void SaveOAuthToken(OAuthToken entity)
        {
            using (this.Transaction = this.Session.BeginTransaction())
            {
                entity.DateCreated = DateTime.Now;
                entity.DateModified = DateTime.Now;
                //entity.AccessUserId = this.User.Key;
                entity.IsActive = true;

                this.Session.SaveOrUpdate(entity);

                this.Transaction.Commit();
            }
        }

        private void UpdateOAuthToken(OAuthToken entity)
        {
            using (this.Transaction = this.Session.BeginTransaction())
            {
                //branch.DateCreated = DateTime.Now;
                entity.DateModified = DateTime.Now;
                //branch.AccessUserId = this.User.Key;
                entity.IsActive = true;
                this.Session.Update(entity);

                this.Transaction.Commit();
            }
        }
    }
}
