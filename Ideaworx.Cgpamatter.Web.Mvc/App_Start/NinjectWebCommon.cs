using Ideaworx.Cgpamatter.Services.CourseExplorer;
using Ideaworx.Cgpamatter.Services.CourseExplorer.Contracts;
using Ideaworx.Cgpamatter.Services.ExamEngine;
using Ideaworx.Cgpamatter.Services.ExamEngine.Contracts;
using Ideaworx.Cgpamatter.Services.Security;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using NHibernate;

[assembly: WebActivator.PreApplicationStartMethod(typeof(Ideaworx.Cgpamatter.Web.Mvc.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(Ideaworx.Cgpamatter.Web.Mvc.App_Start.NinjectWebCommon), "Stop")]

namespace Ideaworx.Cgpamatter.Web.Mvc.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ISession>().ToMethod(x => MvcApplication.SessionFactory.GetCurrentSession());

            // Role, Membership and Froms Authentication providers            
            kernel.Bind<IRoleService>().To<RoleService>();
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IPermissionService>().To<PermissionService>();
            kernel.Bind<ISmtpClientService>().To<SmtpClientService>();

            // Course Explorer services
            kernel.Bind<ICategoryService>().To<CategoryService>();
            kernel.Bind<ICourseService>().To<CourseService>();
            kernel.Bind<ISyllabusService>().To<SyllabusService>();
            kernel.Bind<IQuestionService>().To<QuestionService>();
            kernel.Bind<IQuestionTypeService>().To<QuestionTypeService>();
            kernel.Bind<IAnswerService>().To<AnswerService>();

            // Exam Engine services
            kernel.Bind<IExamService>().To<ExamService>();
            kernel.Bind<IExamAttemptService>().To<ExamAttemptService>();
        }
    }
}
