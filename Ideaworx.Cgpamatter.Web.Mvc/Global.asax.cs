﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ideaworx.Cgpamatter.Services.Security.Contracts;
using Ideaworx.Cgpamatter.Web.Mvc.App_Start;
using NHibernate;
using NHibernate.Context;

namespace Ideaworx.Cgpamatter.Web.Mvc
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }

        public MvcApplication()
        {
            this.BeginRequest += new EventHandler(MvcApplication_BeginRequest);
            this.EndRequest += new EventHandler(MvcApplication_EndRequest);
            this.AuthenticateRequest += new EventHandler(MvcApplication_AuthenticateRequest);
        }

        void MvcApplication_EndRequest(object sender, EventArgs e)
        {
            CurrentSessionContext.Unbind(SessionFactory).Dispose();
        }

        void MvcApplication_BeginRequest(object sender, EventArgs e)
        {
            CurrentSessionContext.Bind(SessionFactory.OpenSession());
        }

        void MvcApplication_AuthenticateRequest(object sender, EventArgs e)
        {
            //if (SiteConfiguration.Current.AuthorizationProviders.FormsAuth.IsDefined == true &&
            //    HttpContext.Current.User != null)
            //    Membership.GetUser(true);
        }

        public static ISessionFactory SessionFactory = CreateSessionFactory();

        private static ISessionFactory CreateSessionFactory()
        {
            var cfg = new NHibernate.Cfg.Configuration().Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "nhibernate.config"));
            //cfg.SetProperty(NHibernate.Cfg.Environment.ConnectionStringName, System.Environment.MachineName);
            cfg.SetProperty(NHibernate.Cfg.Environment.ConnectionStringName, "cgpamatter_prod");
            //NHibernateProfiler.Initialize();

            return cfg.BuildSessionFactory();
        }
    }
}