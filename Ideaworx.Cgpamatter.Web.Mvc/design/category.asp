<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>CGPA Matters</title>
	
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!--<link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />-->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--Nav Start-->
<!--#include file = "include/nav-inside.asp"-->
<!--Nav End-->

<div class="container">
<!--Category Start-->
<div class="wrapper">
	<h1>Select Your Exam</h1>
	<div class="row">
		<div class="span3">
			<ul class="nav nav-tabs nav-stacked ">
				<li><a href="#">Arts <i class="icon-chevron-right"></i></a></li>
				<li><a href="#">Biology & Life Sciences <i class="icon-chevron-right"></i> </a></li>
				<li><a href="#">Business & Management <i class="icon-chevron-right"></i> </a></li>
				<li><a href="#">Chemistry <i class="icon-chevron-right"></i> </a></li>
				<li><a href="#">Artificial Intelligence <i class="icon-chevron-right"></i> </a></li>
				<li><a href="#">Software Engineering <i class="icon-chevron-right"></i> </a></li>
				<li><a href="#">Systems & Security <i class="icon-chevron-right"></i> </a></li>
				<li><a href="#">Computer Science: Theory <i class="icon-chevron-right"></i> </a></li>
				<li><a href="#">Economics & Finance <i class="icon-chevron-right"></i> </a></li>
				<li><a href="#">Education <i class="icon-chevron-right"></i> </a></li>
				<li><a href="#">Energy & Earth Sciences <i class="icon-chevron-right"></i> </a></li>
				<li><a href="#">Engineering <i class="icon-chevron-right"></i> </a></li>
			</ul>
		</div>
		<div class="span658">
			<div class="gray-box img-rounded">
				<!--Result Item Start-->
				<div class="result-item">
					<a href="exam-detail.asp">
					<div class="normal-cat-img fl thumbnail"><img src="img/course/normal/01.jpg" /></div>
					<div class="fl w375"><h5>Computational Neuroscience<small>University of Washington</small><p>with Rajesh P. N. Rao & Adrienne Fairhall</p></h3></div>
					<div class="fr w90 time-date">Apr 8th 2013<span>11 weeks long</span></div>
					<div class="cb"></div>
					</a>
				</div>
				<hr />
				<div class="result-item">
					<div class="normal-cat-img fl thumbnail"><img src="img/course/normal/01.jpg" /></div>
					<div class="fl w375"><h5>Coding the Matrix: Linear Algebra through Computer Science Applications<small>University of Washington</small><p>with Rajesh P. N. Rao & Adrienne Fairhall</p></h3></div>
					<div class="fr w90 time-date">Apr 8th 2013<span>11 weeks long</span></div>
					<div class="cb"></div>
				</div>
				<hr />
				<div class="result-item">
					<div class="normal-cat-img fl thumbnail"><img src="img/course/normal/01.jpg" /></div>
					<div class="fl w375"><h5>Computational Neuroscience<small>University of Washington</small><p>with Rajesh P. N. Rao & Adrienne Fairhall</p></h3></div>
					<div class="fr w90 time-date">Apr 8th 2013<span>11 weeks long</span></div>
					<div class="cb"></div>
				</div>		
				<hr />	
				<div class="result-item">
					<div class="normal-cat-img fl thumbnail"><img src="img/course/normal/01.jpg" /></div>
					<div class="fl w375"><h5>Computational Neuroscience<small>University of Washington</small><p>with Rajesh P. N. Rao & Adrienne Fairhall</p></h3></div>
					<div class="fr w90 time-date">Apr 8th 2013<span>11 weeks long</span></div>
					<div class="cb"></div>
				</div>
				<hr />
				<div class="result-item">
					<div class="normal-cat-img fl thumbnail"><img src="img/course/normal/01.jpg" /></div>
					<div class="fl w375"><h5>Computational Neuroscience<small>University of Washington</small><p>with Rajesh P. N. Rao & Adrienne Fairhall</p></h3></div>
					<div class="fr w90 time-date">Apr 8th 2013<span>11 weeks long</span></div>
					<div class="cb"></div>
				</div>
				<hr />
				<div class="result-item">
					<div class="normal-cat-img fl thumbnail"><img src="img/course/normal/01.jpg" /></div>
					<div class="fl w375"><h5>Computational Neuroscience<small>University of Washington</small><p>with Rajesh P. N. Rao & Adrienne Fairhall</p></h3></div>
					<div class="fr w90 time-date">Apr 8th 2013<span>11 weeks long</span></div>
					<div class="cb"></div>
				</div>
				<hr />
				<div class="result-item">
					<div class="normal-cat-img fl thumbnail"><img src="img/course/normal/01.jpg" /></div>
					<div class="fl w375"><h5>Computational Neuroscience<small>University of Washington</small><p>with Rajesh P. N. Rao & Adrienne Fairhall</p></h3></div>
					<div class="fr w90 time-date">Apr 8th 2013<span>11 weeks long</span></div>
					<div class="cb"></div>
				</div>
				<hr />
				<div class="result-item">
					<div class="normal-cat-img fl thumbnail"><img src="img/course/normal/01.jpg" /></div>
					<div class="fl w375"><h5>Computational Neuroscience<small>University of Washington</small><p>with Rajesh P. N. Rao & Adrienne Fairhall</p></h3></div>
					<div class="fr w90 time-date">Apr 8th 2013<span>11 weeks long</span></div>
					<div class="cb"></div>
				</div>
				<!--Result Item Start-->
			</div>
			<!--Pagination Start-->
			<div class="pagination pull-right">
				<ul>
					<li><a href="#">Prev</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">Next</a></li>
				</ul>
			</div>
			<!--Pagination End-->
		</div>
		
	</div>
</div>

<!--Category End-->

<!--Footer Start-->
<!--#include file = "footer.asp"-->
<!--Footer End-->
</div>
<!--Scripts Start-->
<!--#include file = "scripts.asp"-->
<!--Scripts End-->

</body>
</html>
