<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>CGPA Matters</title>
	
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!--<link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />-->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--Nav Start-->
<!--#include file = "include/nav-inside.asp"-->
<!--Nav End-->

<div class="container">
<!--Category Start-->
<div class="wrapper">
	<h1>Exam Details</h1>
	<div class="row">
		<div class="span658">
			<h5>Computational Neuroscience<small>with Rajesh P. N. Rao & Adrienne Fairhall</small></h5>
			<div class="mtb30"><p><strong>Pascal Van Hentenryck</strong><br />Tired of solving Sudokus by hand? This class teaches you how to solve complex search problems with discrete optimization, including constraint programming, local search, and mixed-integer programming.</p></div>
			<h1><small>Workload: 8-12 hours/week</small>About the Course</h1>
			<p>Optimization technology is ubiquitous in our society. It schedules planes and their crews, coordinates the production of steel, and organizes the transportation of iron ore from the mines to the ports. Optimization clears the day-ahead and real-time markets to deliver electricity to millions of people. It organizes kidney exchanges and cancer treatments and helps scientists understand the fundamental fabric of life, control complex chemical reactions, and design drugs that may benefit billions of individuals.</p>
			<p>This class is an introduction to discrete optimization and exposes students to some of the most fundamental concepts and algorithms in the field. It covers constraint programming, local search, and mixed-integer programming from their foundations to their applications for complex practical problems in areas such as scheduling, vehicle routing, supply-chain optimization, and resource allocation.</p>
			<h1>FAQ</h1>
			<!--Accordion Start-->
			<!--#include file = "include/faq-01.asp"-->
			<!--Accordion End-->
		</div>
		<div class="span3 pull-right right-sec img-rounded">
			<div class="bg-text img-rounded text-center">Feeling Prepared?</div>
			<div class="pd20">
				<a class="btn btn-large btn-block btn-success" href="exam-room.asp">Start your Exam</a>
				<div class="text-center mtb10 text-center">Not Yet?</div>
				
				<div class="cptext text-center">i Want to learn </div>
				<a class="btn btn-large btn-block" href="#">Course</a>
				
				<div class="mtb20">
					<div class="cptext text-center">i know but i need Practice</div>
					<a class="btn btn-large btn-block" href="#">Workshop</a>
				</div>
				<div class="f12">
					<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
					<p>Anim pariatur cliche reprehenderit, enim eiusmod</p>
				</div>
			</div>
		</div>		
	</div>
</div>

<!--Category End-->

<!--Footer Start-->
<!--#include file = "footer.asp"-->
<!--Footer End-->
</div>
<!--Scripts Start-->
<!--#include file = "scripts.asp"-->
<!--Scripts End-->

</body>
</html>
