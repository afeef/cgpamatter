<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>CGPA Matters</title>
	
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!--<link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />-->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--Nav Start-->
<!--#include file = "include/nav-inside.asp"-->
<!--Nav End-->

<div class="container">
<!--Category Start-->
<div class="wrapper">
	<h1><span>CS201 - </span>Introduction to Programming </h1>
	<div class="row">
		<div class="span658">
			<!--Question Start-->
			<div class="question img-rounded">
				<div class="question-label">
					<div class="fl">Question no: 1 of 52</div>
					<div class="fr">Marks: 1 (Budgeted Time 1 Min)</div>
					<div class="cb"></div>
				</div>
				<div class="question-detail img-rounded">Consider the following statements to initialize a two-dimensional array.</div>
			</div>
			<!--Options Start-->
			<div class="question-options">
				<p>i.       int arr[2][3] = {4, 8, 9, 2, 1, 6};</p>
				<p>ii.      int arr[3][3] = {4, 8, 9, 2, 1, 6};</p>
				<p>iii.     int arr[][3] = {4, 8, 9, 2, 1, 6};</p>
				<!--Options End-->
				<p class="mtb20">Which of the following options(s) are correct initialize a two-dimensional array with 3 rows and 2 columns?</p>
			</div>
			<!--Question End-->
			<!--Ans Start-->
			<div class="mtb20">
				<div class="bg-text img-rounded mtb20"><strong>Answer</strong> (Please Select your correct Option)</div>
				<ul class="nav nav-tabs nav-stacked">
					<li><a href="#"><label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>Option one is this and that—be sure to include why it's great</label></a></li>
					<li><a href="#"><label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" checked>Option one is this and that—be sure to include why it's great</label></a></li>
					<li><a href="#"><label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" checked>Option one is this and that—be sure to include why it's great</label></a></li>
					<li><a href="#"><label class="radio"><input type="radio" name="optionsRadios" id="optionsRadios4" value="option4" checked>Option one is this and that—be sure to include why it's great</label></a></li>
				</ul>
			</div>
			<div>
				<div class="fl">
					<a class="btn btn-large mr10"><i class="icon-chevron-left"></i> Prev</a>
					<a class="btn btn-large">Next <i class="icon-chevron-right"></i></a>
				</div>
				<div class="fr">
					<a class="btn btn-large"><i class="icon-fast-backward"></i></a>
					<a class="btn btn-large"><i class="icon-fast-forward"></i></a>
				</div>
				<div class="cb"></div>
			</div>
			<!--Ans  End-->
		</div>
		<div class="span3 pull-right">
			<!--Time word-->
			<div class="row f15">
				<div class="span2"><i class="icon-time"></i> Time Ramaing</div>
				<div class="span1 text-right">84:00</div>
			</div>
			<!--Time word End-->
			<!--Time Bar-->
			<div class="progress progress-striped active mt5">
				<div class="bar bar-success" style="width: 40%;"></div>
			</div>
			<a class="btn btn-large btn-block btn-danger"><i class="icon-thumbs-up icon-white"></i> Click To Finsh Exam</a>
			<!--Time Bar End-->
				
			<div class="img-rounded right-sec">
				<div class="pd20 mtb20">
					<div class="f15"><i class="icon-info-sign"></i> Exam Tip!</div>
					<div class="f11">
						<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,</p>
						<p>ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
					</div>
				</div>
			</div>
			
		</div>		
	</div>
</div>

<!--Category End-->

<!--Footer Start-->
<!--#include file = "footer.asp"-->
<!--Footer End-->
</div>
<!--Scripts Start-->
<!--#include file = "scripts.asp"-->
<!--Scripts End-->

</body>
</html>
