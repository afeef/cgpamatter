<div class="cat-select-bar">
	<div class="fl home-text-category">
		Our Courses
		<a href="#" class="active">All</a>
		<a href="#">Arts</a>
		<a href="#">Biology & Life Sciences</a>
		<a href="#">Business & Management</a>
		<a href="#">CS: Artificial Intelligence</a>
		<a href="#">CS: Software Engineering</a>			
	</div>
	<div class="fr">
		<div class="btn-group">
			<a class="btn"><i class="icon-chevron-left"></i></a>
			<a class="btn"><i class="icon-chevron-right"></i></a>
		</div>
	</div>
	<div class="cb"></div>
</div>
<!--Thumbs Start-->
<div class="home-thumbs">
	<a class="thumbnail" href="category.asp"><img alt="300x200"  src="img/course/01.png" /> <h4>Computational Neuroscience <small>University of Washington</small></h4></a>
	<a class="thumbnail" href="category.asp"><img alt="300x200"  src="img/course/02.png" /> <h4>Computational Neuroscience <small>University of Washington</small></h4></a>
	<a class="thumbnail" href="category.asp"><img alt="300x200"  src="img/course/03.png" /> <h4>Computational Neuroscience <small>University of Washington</small></h4></a>
	<a class="thumbnail" href="category.asp"><img alt="300x200"  src="img/course/04.png" /> <h4>Computational Neuroscience <small>University of Washington</small></h4></a>
	<a class="thumbnail" href="category.asp"><img alt="300x200"  src="img/course/05.png" /> <h4>Computational Neuroscience <small>University of Washington</small></h4></a>
	<a class="thumbnail" href="category.asp"><img alt="300x200"  src="img/course/06.png" /> <h4>Computational Neuroscience <small>University of Washington</small></h4></a>
	<div class="cl"></div>	
</div>
<!--Thumbs End-->
