<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<div class="fl w183"><a href="index.asp" class="brand"><img src="img/logo.png"  alt="CGPA Matters Logo" /></a></div>
			<div class="fr br-login dropdown">
				<div class="user-photo fl"><img src="img/user/afeef-xsmal.jpg" alt="" /></div>
				<div class="user-name fl">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Afeef Janjua <span class="caret"></span><small>Computer Science</small></a>
					<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
						<li><a href="#">Profile</a></li>
						<li><a href="#">History</a></li>
						<li><a href="#">Settings</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-off"></i> Logout</a></li>
					</ul>
				</div>
				<div class="cl"></div>
			</div>
			<div class="text-center sandbar-links"><a href="#">Dashboard</a><a href="#">Exam Room</a><a href="#">Results</a><a href="#">Friends</a></div>
			<div class="cb"></div>
		</div>
	</div>
</div>
