<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<div class="fl w183"><a href="index.asp" class="brand"><img src="img/logo.png"  alt="CGPA Matters Logo" /></a></div>
			<div class="nav-collapse collapse pull-right">
				<div class="fr"><a class="btn" href="login-home.asp"><i class="icon-user"> </i> Sign In</a></div>
				<div class="fr f11 lh30 mt5 mr15">Already a Member?</div>
				<div class="cr"></div>
			</div>
		</div>
	</div>
</div>
