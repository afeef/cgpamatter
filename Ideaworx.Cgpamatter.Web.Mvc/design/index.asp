<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>cgpamatter.com</title>
	
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!--<link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />-->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--Nav Start-->
<!--#include file = "include/nav.asp"-->
<!--Nav End-->

<!--Banner Start-->
<!--#include file = "include/banner.asp"-->
<!--Banner End-->
<div class="container">
<!--Home Category Start-->
<!--#include file = "homepage.asp"-->
<!--Home Category End-->

<!--Footer Start-->
<!--#include file = "footer.asp"-->
<!--Footer End-->
</div>
<!--Scripts Start-->
<!--#include file = "scripts.asp"-->
<!--Scripts End-->

</body>
</html>
