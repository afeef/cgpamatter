<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>CGPA Matters</title>
	
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!--<link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />-->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--Nav Start-->
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<div class="fl w183"><a href="index.asp" class="brand"><img src="img/logo.png"  alt="CGPA Matters Logo" /></a></div>
			<div class="login-sandbar pull-right form-inline">
				<input type="text"  placeholder="Email">
				<input type="password" class="span2" placeholder="Password">
				<button type="submit" class="btn"><i class="icon-user"> </i> Sign In</button>
			</div>
		</div>
	</div>
</div>
<!--Nav End-->
<!--Banner Start-->
<!--#include file = "include/banner.asp"-->
<!--Banner End-->
<div class="container">
<!--Home Category Start-->
<!--#include file = "homepage.asp"-->
<!--Home Category End-->

<!--Footer Start-->
<!--#include file = "footer.asp"-->
<!--Footer End-->
</div>
<!--Scripts Start-->
<!--#include file = "scripts.asp"-->
<!--Scripts End-->

</body>
</html>
