﻿using System;
using Ideaworx.Cgpamatter.Common.Configuration;

namespace Ideaworx.Cgpamatter.Web.Extensions
{
	public static class DateTimeExtensions
	{
		public static DateTime ToApplicationDateTime(this DateTime date)
		{
			if (date.Kind == DateTimeKind.Utc)
			{
				if (SiteConfiguration.Current.TimeZoneOffset.HasValue)
				{
					return DateTime.SpecifyKind(date.Add(SiteConfiguration.Current.TimeZoneOffset.Value), DateTimeKind.Local);
				}
				else
				{
					return date.ToLocalTime();
				}
			}
			return date;
		}

        public static string ToDateString(this DateTime? date, string format)
        {
            return date != null ? Convert.ToDateTime(date).ToString(format) : null;
        }
	}
}
