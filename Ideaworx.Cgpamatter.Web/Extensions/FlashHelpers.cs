﻿using System;
using System.Text;
using System.Web.Mvc;

namespace Ideaworx.Cgpamatter.Web.Extensions
{
    public static class FlashHelpers
    {

        public static void FlashSuccess(this Controller controller, string message)
        {
            controller.TempData["success"] = message;
        }

        public static void FlashInfo(this Controller controller, string message)
        {
            controller.TempData["info"] = message;
        }

        public static void FlashWarning(this Controller controller, string message)
        {
            controller.TempData["warning"] = message;
        }

        public static void FlashError(this Controller controller, string message)
        {
            controller.TempData["error"] = message;
        }

        public static MvcHtmlString Flash(this HtmlHelper helper)
        {

            var message = string.Empty;
            var title = string.Empty;
            var type = string.Empty;

            if (helper.ViewContext.TempData["success"] != null)
            {

                message = helper.ViewContext.TempData["success"].ToString();
                title = "Success";
                type = "success";
            }

            else if (helper.ViewContext.TempData["info"] != null)
            {

                message = helper.ViewContext.TempData["info"].ToString();
                title = "Info";
                type = "info";
            }

            else if (helper.ViewContext.TempData["warning"] != null)
            {

                message = helper.ViewContext.TempData["warning"].ToString();
                title = "Warning";
                type = "notice";
            }

            else if (helper.ViewContext.TempData["error"] != null)
            {

                message = helper.ViewContext.TempData["error"].ToString();
                title = "Error";
                type = "error";
            }

            var sb = new StringBuilder();

            if (!String.IsNullOrEmpty(message))
            {

                sb.AppendLine("<script type='text/javascript'>");
                sb.AppendLine("$(function(){");
                sb.AppendLine("$.pnotify.defaults.history = false;");
                sb.AppendLine("$.pnotify({ ");
                sb.AppendLine(string.Format("title: '{0}',text: '{1}',type: '{2}'", title, message, type));
                sb.AppendLine("});");
                sb.AppendLine("});");
                sb.AppendLine("</script>");
            }

            return MvcHtmlString.Create(sb.ToString());
        }

    }
}