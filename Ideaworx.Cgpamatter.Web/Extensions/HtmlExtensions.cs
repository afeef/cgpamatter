﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Mvc.Html;
using System.Text.RegularExpressions;
using System.Security;
using Ideaworx.Cgpamatter.Common.Configuration;

namespace Ideaworx.Cgpamatter.Web.Extensions
{
	public static class HtmlExtensions
	{
        public static MvcHtmlString MenuItem(this HtmlHelper helper,
            string linkText, string actionName, string controllerName)
        {
            string currentControllerName = (string)helper.ViewContext.RouteData.Values["controller"];
            string currentActionName = (string)helper.ViewContext.RouteData.Values["action"];

            var builder = new TagBuilder("li");
            if (currentControllerName.Equals(controllerName, StringComparison.CurrentCultureIgnoreCase)
                && currentActionName.Equals(actionName, StringComparison.CurrentCultureIgnoreCase))
                builder.AddCssClass("selected");
            builder.InnerHtml = helper.ActionLink(linkText, actionName, controllerName).ToHtmlString();
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString ActionQueryLink(this HtmlHelper htmlHelper,
            string linkText, string action, object routeValues)
        {
            var newRoute = routeValues == null
                ? htmlHelper.ViewContext.RouteData.Values
                : new RouteValueDictionary(routeValues);

            newRoute = htmlHelper.ViewContext.HttpContext.Request.QueryString
                .ToRouteDic(newRoute);

            return HtmlHelper.GenerateLink(htmlHelper.ViewContext.RequestContext,
                htmlHelper.RouteCollection, linkText, null,
                action, null, newRoute, null).ToMvcHtml();
        }

        public static MvcHtmlString ToMvcHtml(this string content)
        {
            return MvcHtmlString.Create(content);
        }

        public static RouteValueDictionary ToRouteDic(this NameValueCollection collection)
        {
            return collection.ToRouteDic(new RouteValueDictionary());
        }

        public static RouteValueDictionary ToRouteDic(this NameValueCollection collection,
            RouteValueDictionary routeDic)
        {
            foreach (string key in collection.Keys)
            {
                if (!routeDic.ContainsKey(key))
                    routeDic.Add(key, collection[key]);
            }
            return routeDic;
        }

		public static string GetModelAttemptedValue(this HtmlHelper htmlHelper, string key)
		{
			ModelState state;
			if (htmlHelper.ViewData.ModelState.TryGetValue(key, out state))
			{
				return state.Value.AttemptedValue;
			}
			return null;
		}

		public static string EvalString(this HtmlHelper htmlHelper, string key)
		{
			return Convert.ToString(htmlHelper.ViewData.Eval(key), CultureInfo.CurrentCulture);
		}

		public static MvcHtmlString MetaDescription(this HtmlHelper htmlHelper, string content)
		{
			if (content != null)
			{
				TagBuilder builder = new TagBuilder("meta");
				builder.Attributes.Add("name", "description");
				content = Regex.Replace(content, "\r|\n", "");
				content = Regex.Replace(content, "(&nbsp;)+|(\t+)", " ");
				builder.Attributes.Add("content", SecurityElement.Escape(content));
				return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
			}
			else
			{
				return null;
			}
		}

		public static MvcHtmlString DropDownListDefault(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, object defaultValue, string defaultText)
		{
			List<SelectListItem> list = new List<SelectListItem>(selectList);
			SelectListItem item = new SelectListItem();
			item.Text = defaultText;
			item.Value = defaultValue.ToString();
			list.Insert(0, item);
			return htmlHelper.DropDownList(name, list);
		}

		public static T GetStateValue<T>(this HtmlHelper htmlHelper, string key)
		{
			T value = default(T);
			ModelState state;
			if (htmlHelper.ViewData.ModelState.TryGetValue(key, out state))
			{
				value = (T)state.Value.ConvertTo(typeof(T), null);
			}
			else
			{
				value = (T)htmlHelper.ViewData.Eval(key);
			}
			return value;
		}

		/// <summary>
		/// Converts date to app datetime and applies the format
		/// </summary>
		public static MvcHtmlString Date(this HtmlHelper htmlHelper, DateTime date, string format)
		{
			DateTime appDate = date.ToApplicationDateTime();
			TagBuilder builder = new TagBuilder("span");
			builder.AddCssClass("date");
			builder.AddCssClass("d" + appDate.Year + "-" + appDate.Month + "-" + appDate.Day + "-" + appDate.Hour + "-" + appDate.Minute);
			builder.InnerHtml = appDate.ToString(format);
			return MvcHtmlString.Create(builder.ToString());
		}

		/// <summary>
		/// Converts date to app datetime and applies configuration defined date format
		/// </summary>
		public static MvcHtmlString Date(this HtmlHelper htmlHelper, DateTime date)
		{
			return htmlHelper.Date(date, SiteConfiguration.Current.DateFormat);
		}

		#region Link
		public static MvcHtmlString Link(this HtmlHelper htmlHelper, string url)
		{
			return htmlHelper.Link(url, null);
		}

		public static MvcHtmlString Link(this HtmlHelper htmlHelper, string url, object htmlAttributes)
		{
			return htmlHelper.Link(null, url, htmlAttributes);
		}

		public static MvcHtmlString Link(this HtmlHelper htmlHelper, string innerHtml, string url)
		{
			return htmlHelper.Link(innerHtml, url, null);
		}

		public static MvcHtmlString Link(this HtmlHelper htmlHelper, string innerHtml, string url, object htmlAttributes)
		{
			IDictionary<string, object> htmlAttributesDictionay = ((IDictionary<string, object>)new RouteValueDictionary(htmlAttributes));
			TagBuilder builder = new TagBuilder("a");
			builder.MergeAttributes<string, object>(htmlAttributesDictionay);
			if (innerHtml == null)
			{
				builder.InnerHtml = url.Replace("http://", "");
			}
			else
			{
				builder.InnerHtml = innerHtml;
			}
			builder.Attributes.Add("href", url);
			return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
		} 
		#endregion

		public static MvcHtmlString CheckBoxBit<T>(this HtmlHelper htmlHelper, string name, T value, T expectedBit) where T : struct, IConvertible
		{
			if (!typeof(T).IsEnum) 
			{
				throw new ArgumentException("T must be an enumerated type");
			}

			return htmlHelper.CheckBox(name, (Convert.ToInt32(value) & Convert.ToInt32(expectedBit)) > 0, new{value = Convert.ToInt32(expectedBit)});
		}


		#region Captcha
		/// <summary>
		/// Returns a formItem with the captcha image in case the user has to validate his "humanity".
		/// </summary>
		/// <param name="labelText">Text to be shown in the label of the captcha field</param>
		public static MvcHtmlString Captcha(this HtmlHelper htmlHelper, string labelText)
		{
			if (htmlHelper.ViewData.GetDefault<bool>("ShowCaptcha"))
			{
				var builder = new TagBuilder("div");
				builder.AddCssClass("formItem");
				builder.AddCssClass("floatContainer");
				builder.AddCssClass("captcha");
				builder.InnerHtml = htmlHelper.Label("captcha", labelText).ToString();
				builder.InnerHtml += htmlHelper.TextBox("captcha");
				var imageBuilder = new TagBuilder("img");
				builder.InnerHtml += "<img src=\"" + htmlHelper.GetUrl("Captcha", "Base", null) + "\" alt=\"\" />";
				return MvcHtmlString.Create(builder.ToString());
			}
			else
			{
				return null;
			}
		} 
		#endregion

		#region GetUrl
		/// <summary>
		/// Generates a url based on the routeValues
		/// </summary>
		public static string GetUrl(this HtmlHelper htmlHelper, string actionName, string controllerName, object routeValues)
		{
			return UrlHelper.GenerateUrl(null, actionName, controllerName, new RouteValueDictionary(routeValues), htmlHelper.RouteCollection, htmlHelper.ViewContext.RequestContext, true);
		} 
		#endregion

		#region Partial
		public static MvcHtmlString Partial(this HtmlHelper htmlHelper, string partialViewName, object viewDataValues, bool createViewData)
		{
			if (createViewData)
			{
				return htmlHelper.Partial(partialViewName, ViewDataExtensions.CreateViewData(viewDataValues));
			}
			else
			{
				return htmlHelper.Partial(partialViewName, htmlHelper.ViewData);
			}
		} 
		#endregion

		#region Script, style sheets and images
		/// <summary>
		/// Script tag, enforces to be app relative
		/// </summary>
		public static MvcHtmlString Script(this HtmlHelper htmlHelper, string url)
		{
			ValidateApplicationUrl(url);
			if (url[0] == '~')
			{
				url = url.ToLower();
			}
			
			TagBuilder builder = new TagBuilder("script");
			builder.Attributes.Add("type", "text/javascript");
			builder.Attributes.Add("src", UrlHelper.GenerateContentUrl(url, htmlHelper.ViewContext.HttpContext));
			return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
		}

		/// <summary>
		/// Jquery script tag (current version)
		/// </summary>
		public static MvcHtmlString ScriptjQuery(this HtmlHelper htmlHelper)
		{
			return htmlHelper.Script("~/scripts/jquery-1.5.2.min.js");
		}

		/// <summary>
		/// Add a link tag referencing the stylesheet, enforcing the url to be app relative.
		/// </summary>
		public static MvcHtmlString Stylesheet(this HtmlHelper htmlHelper, string url)
		{
			ValidateApplicationUrl(url);
			if (url[0] == '~')
			{
				url = url.ToLower();
			}

			TagBuilder builder = new TagBuilder("link");
			builder.Attributes.Add("type", "text/css");
			builder.Attributes.Add("rel", "stylesheet");
			builder.Attributes.Add("href", UrlHelper.GenerateContentUrl(url, htmlHelper.ViewContext.HttpContext));
			return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
		}

		/// <summary>
		/// Add a link tag referencing the stylesheet, enforcing the url to be app relative.
		/// </summary>
		public static MvcHtmlString Img(this HtmlHelper htmlHelper, string url, string alt)
		{
			ValidateApplicationUrl(url);
			if (url[0] == '~')
			{
				url = url.ToLower();
			}

			TagBuilder builder = new TagBuilder("img");
			builder.Attributes.Add("alt", alt);
			builder.Attributes.Add("src", UrlHelper.GenerateContentUrl(url, htmlHelper.ViewContext.HttpContext));
			return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
		}

		/// <summary>
		/// Validates that the url is absolute or starts with tilde (~) char.
		/// </summary>
		/// <param name="url"></param>
		private static void ValidateApplicationUrl(string url)
		{
			if (String.IsNullOrEmpty(url))
			{
				throw new ArgumentNullException("url");
			}
			if ((!url.StartsWith("http://")) && (!url.StartsWith("https://")) && url[0] != '~')
			{
				throw new ArgumentException("Url must start tilde character '~' or be absolute.", "url");
			}
		}
		#endregion
	}
}
