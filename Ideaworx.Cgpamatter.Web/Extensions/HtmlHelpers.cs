﻿using System;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace Ideaworx.Cgpamatter.Web.Extensions
{
    public static class HtmlHelpers
    {
        const string pubDir = "/public";
        const string cssDir = "css";
        const string imageDir = "images";
        const string scriptDir = "scripts";

        public static string DatePickerEnable(this HtmlHelper helper)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type='text/javascript'>$(document).ready(function() {$('.date-selector').datepicker();});</script>\n");
            return sb.ToString();
        }

        public static string Friendly(this HtmlHelper helper)
        {
            if (helper.ViewContext.HttpContext.Request.Cookies["friendly"] != null)
            {
                return XssHelper.H(helper, helper.ViewContext.HttpContext.Request.Cookies["friendly"].Value);
            }
            else
            {
                return "";
            }
        }

        //public static string Script(this HtmlHelper helper, string fileName) {
        //    if (!fileName.EndsWith(".js"))
        //        fileName += ".js";
        //    var jsPath = string.Format("<script src='{0}/{1}/{2}' ></script>\n", pubDir, scriptDir, helper.AttributeEncode(fileName));
        //    return jsPath;
        //}

        public static string Css(this HtmlHelper helper, string fileName)
        {
            return Css(helper, fileName, "screen");
        }
        public static string Css(this HtmlHelper helper, string fileName, string media)
        {
            if (!fileName.EndsWith(".css"))
                fileName += ".css";
            var jsPath = string.Format("<link rel='stylesheet' type='text/css' href='{0}/{1}/{2}'  media='" + media + "'/>\n", pubDir, cssDir, helper.AttributeEncode(fileName));
            return jsPath;
        }
        public static MvcHtmlString Image(this HtmlHelper helper, string fileName)
        {
            return Image(helper, fileName, "");
        }
        public static MvcHtmlString Image(this HtmlHelper helper, string fileName, string attributes)
        {
            fileName = string.Format("{0}/{1}/{2}", pubDir, imageDir, fileName);
            //return string.Format("<img src='{0}' '{1}' />", helper.AttributeEncode(fileName), helper.AttributeEncode(attributes));

            return MvcHtmlString.Create(string.Format("<img src='{0}' '{1}' />", helper.AttributeEncode(fileName), helper.AttributeEncode(attributes)));
        }

        public static MvcHtmlString ProviderImagePath(this HtmlHelper helper, string imagePath, string providerName, string ext)
        {

            var providerImagePath = string.Format("{0}/{1}.{2}", imagePath, providerName, ext);

            return MvcHtmlString.Create(providerImagePath);
        }

        /// <summary>
        /// ActionLinkUI.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="action">The action.</param>
        /// <returns>ActionLink string</returns>
        public static MvcHtmlString ActionLinkUi<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression, string action)
        {
            return ActionLinkUi(htmlHelper, expression, action, null);
        }

        /// <summary>
        /// ActionLinkUI.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="action">The action.</param>
        /// <param name="icon">The icon.</param>
        /// <returns>ActionLink string</returns>
        public static MvcHtmlString ActionLinkUi<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression, string action, string icon)
        {
            return ActionLinkUi(htmlHelper, expression, action, icon, null);
        }

        /// <summary>
        /// ActionLinkUI.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="action">The action.</param>
        /// <param name="icon">The icon.</param>
        /// <param name="htmlAttributes">The HTML attributes.</param>
        /// <returns>ActionLink string</returns>
        public static MvcHtmlString ActionLinkUi<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression, string action, string icon, object htmlAttributes)
        {
            var controllerName = htmlHelper.ViewContext.RouteData.Values["controller"].ToString();
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            var a = new TagBuilder("a");
            switch (action)
            {
                case "Index":
                    a.Attributes.Add("href", String.Format("/{0}", controllerName));
                    a.Attributes.Add("title", "Back to list");
                    break;

                case "Create":
                    a.Attributes.Add("href", String.Format("/{0}/{1}", controllerName, action));
                    a.Attributes.Add("title", "Create new");
                    break;
                default:
                    a.Attributes.Add("href", String.Format("/{0}/{1}/{2}", controllerName, action, metaData.Model));
                    a.Attributes.Add("title", action);
                    break;
            }


            a.AddCssClass("actionLinkUI");
            a.AddCssClass("ui-widget");
            a.AddCssClass("ui-state-default");
            a.AddCssClass("ui-corner-all");

            var span = new TagBuilder("span");
            span.AddCssClass("ui-icon");
            span.AddCssClass(icon);
            span.InnerHtml = action;
            a.InnerHtml = span.ToString(TagRenderMode.Normal);

            if (htmlAttributes != null)
            {
                a.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            }

            return MvcHtmlString.Create(a.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString ActionLinkUi<TModel>(this HtmlHelper<TModel> htmlHelper, string linkText, string actionName, string controllerName, string icon, object routeValues)
        {            
            var url = UrlHelper.GenerateUrl(null, actionName, controllerName, new RouteValueDictionary(routeValues), htmlHelper.RouteCollection, htmlHelper.ViewContext.RequestContext, true);
            var qs = htmlHelper.ViewContext.HttpContext.Request.QueryString.ToString();

            url = string.Format("{0}?{1}", url, qs);

            var li = new TagBuilder("li");
            var a = new TagBuilder("a");

            a.MergeAttribute("href", url);
           
            a.MergeAttribute("rel", "tooltip");
            a.Attributes.Add("title", actionName);

            var i = new TagBuilder("i");
            i.AddCssClass(icon);
            
            a.InnerHtml = i.ToString(TagRenderMode.Normal) + linkText;
            li.InnerHtml = a.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(li.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString ActionLinkUi<TModel>(this HtmlHelper<TModel> htmlHelper, string linkText, string actionName, string icon, object routeValues)
        {
            return ActionLinkUi(htmlHelper, linkText, actionName, null, icon, routeValues);
        }

        public static MvcHtmlString ActionLinkUi<TModel>(this HtmlHelper<TModel> htmlHelper, string linkText, string actionName, string icon)
        {
            return ActionLinkUi(htmlHelper, linkText, actionName, null, icon, null);
        }

        public static MvcHtmlString ActionLinkUi<TModel>(this HtmlHelper<TModel> htmlHelper, object routeValues)
        {
            var div = new TagBuilder("div");
            div.AddCssClass("btn-group");

            var btn = new TagBuilder("a");

            btn.AddCssClass("btn dropdown-toggle");
            btn.MergeAttribute("data-toggle", "dropdown");
            btn.MergeAttribute("href", "#");

            var span = new TagBuilder("span");
            span.AddCssClass("caret");                   
            
            var i = new TagBuilder("i");
            i.AddCssClass("icon-list");
            
            btn.InnerHtml = i.ToString(TagRenderMode.Normal) + span.ToString(TagRenderMode.Normal);
            
            var edit = ActionLinkUi(htmlHelper, "Edit", "Edit", "icon-edit", routeValues);
            var details = ActionLinkUi(htmlHelper, "Details", "Details", "icon-zoom-in", routeValues);
            var delete = ActionLinkUi(htmlHelper, "Delete", "Delete", "icon-trash", routeValues);
            var ul = new TagBuilder("ul") { InnerHtml = edit.ToString() + details + delete };

            ul.AddCssClass("dropdown-menu");    
       
            div.InnerHtml = btn.ToString(TagRenderMode.Normal) + ul.ToString(TagRenderMode.Normal);

            var mvcHtmlString = new MvcHtmlString(div.ToString(TagRenderMode.Normal));
            
            return mvcHtmlString;
        }

        public static MvcHtmlString AddEditDelLinkUi<TModel>(this HtmlHelper<TModel> htmlHelper,
            object routeValues,
            string editAction,
            string detailsAction,
            string deleteAction
            )
        {
            var edit = ActionLinkUi(htmlHelper, string.Empty, editAction, "ui-icon-pencil", routeValues);
            var details = ActionLinkUi(htmlHelper, string.Empty, detailsAction, "ui-icon-note", routeValues);
            var delete = ActionLinkUi(htmlHelper, string.Empty, deleteAction, "ui-icon-close", routeValues);
            var span = new TagBuilder("span") { InnerHtml = edit.ToString() + details + delete };
            var mvcHtmlString = new MvcHtmlString(span.ToString(TagRenderMode.Normal));
            return mvcHtmlString;
        }
    }
}
