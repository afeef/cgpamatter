﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Web.Extensions
{
    public static class QueryExtentions
    {
        public static SelectList ToSelectList<T>(this IQueryable<T> query, string dataValueField, string dataTextField, object selectedValue)
        {
            return new SelectList(query, dataValueField, dataTextField, selectedValue ?? -1);
        }
       
        public static string Property<T>(this GridSortOptions options)
        {
            return Property<T>(options, "Key");
        }

        public static string Property<T>(this GridSortOptions options, string defaultColumnName)
        {
            if (string.IsNullOrWhiteSpace(options.Column))
                options.Column = defaultColumnName;
            
            Type type = typeof(T);
            PropertyInfo property = type.GetProperty(options.Column, BindingFlags.IgnoreCase
                                                                            | BindingFlags.Public
                                                                            | BindingFlags.Instance);

            if (property == null)
                throw new InvalidOperationException(string.Format("Could not find a property called ‘{0}’ on type {1}", (object)options.Column, (object)type));
            
            options.Column = property.Name;

            return property.Name;
        }
    }
}
