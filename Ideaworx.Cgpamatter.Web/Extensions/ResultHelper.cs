﻿using System.Web.Mvc;

namespace Ideaworx.Cgpamatter.Web.Extensions
{
	public static class ResultHelper
	{
		/// <summary>
		/// Returns a status 404 to the client and the error 404 view.
		/// </summary>
		/// <param name="emptyBody">false: the response ends</param>
		public static ActionResult NotFoundResult(ControllerBase controller, bool emptyBody)
		{
			controller.ControllerContext.HttpContext.Response.StatusCode = 404;
			if (emptyBody)
			{
				controller.ControllerContext.HttpContext.Response.End();
			}

			ViewResult viewResult = new ViewResult();
			viewResult.ViewName = "Errors/404";
			return viewResult;
		}

		public static ActionResult NotFoundResult(ControllerBase controller)
		{
			return NotFoundResult(controller, false);
		}

		public static ActionResult ForbiddenResult(ControllerBase controller)
		{
			return ForbiddenResult(controller, false);
		}

		public static ActionResult ForbiddenResult(ControllerBase controller, bool emptyBody)
		{
			controller.ControllerContext.HttpContext.Response.StatusCode = 403;
			if (emptyBody)
			{
				controller.ControllerContext.HttpContext.Response.End();
			}

			ViewResult viewResult = new ViewResult();
			viewResult.ViewName = "Errors/403";
			return viewResult;
		}

		public static ActionResult MovedPermanentlyResult(ControllerBase controller, object routeValues)
		{
			return new MovedPermanentlyResult(routeValues);
		}

		/// <summary>
		/// Returns a ViewResult with content type set as xml
		/// </summary>
		public static ActionResult XmlViewResult(ControllerBase controller, object model, string viewName=null)
		{
			controller.ControllerContext.HttpContext.Response.ContentType = "text/xml; charset=UTF-8";
			if (model != null)
			{
				controller.ViewData.Model = model;
			}
			ViewResult result = new ViewResult();
			result.ViewName = viewName;
			result.ViewData = controller.ViewData;
			result.TempData = controller.TempData;
			return result;
		}
	}
}
