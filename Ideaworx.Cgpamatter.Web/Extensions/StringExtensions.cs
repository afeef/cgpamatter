﻿using System;

namespace Ideaworx.Cgpamatter.Web.Extensions
{
	public static class StringExtensions
	{				
		public static string FirstUpperCase(this string value)
		{
			if (!String.IsNullOrEmpty(value))
			{
				string result = value.Substring(0, 1);
				result = result.ToUpper();

				if (value.Length > 1)
				{
					result += value.Substring(1);
				}
				return result;
			}
			return value;
		}
	}
}
