﻿using System.Linq;
using Ideaworx.Cgpamatter.Entities.Security;

namespace Ideaworx.Cgpamatter.Web.Extensions
{
    public static class UserExtensions
    {
        public static bool HasPermissions(this User user, Permissions required)
        {

            return user.Roles.SelectMany(role => role.Permissions)
                .Any(permission => permission.PermissionFlag.Equals(required));
        }
    }
}
