﻿using System.Web.Mvc;
using Microsoft.Security.Application;

namespace Ideaworx.Cgpamatter.Web.Extensions {
    public static class XssHelper {
        public static string H(this HtmlHelper helper, string input) {
            return Encoder.HtmlEncode(input);
        }
        public static string Sanitize(this HtmlHelper helper, string input) {
            return Sanitizer.GetSafeHtml(input);          
        }
        /// <summary>
        /// Encodes Javascript
        /// </summary>
        public static string Hscript(this HtmlHelper helper, string input) {
            return Encoder.JavaScriptEncode(input);
        }
    }
}
