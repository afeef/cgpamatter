using Mvc.Mailer;

namespace Ideaworx.Cgpamatter.Web.Mailers
{ 
    public interface IUserMailer
    {
			MvcMailMessage Welcome();
			MvcMailMessage PasswordReset(string token, string email);
			MvcMailMessage VerifyAccount();
            MvcMailMessage ActivateAccount(string token, string email);
    }
}