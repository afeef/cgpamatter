using Mvc.Mailer;

namespace Ideaworx.Cgpamatter.Web.Mailers
{ 
    public class UserMailer : MailerBase, IUserMailer 	
	{
		public UserMailer()
		{
			MasterName="_Layout";
		}
		
		public virtual MvcMailMessage Welcome()
		{
			//ViewBag.Data = someObject;
			return Populate(x =>
			{
				x.Subject = "Welcome";
				x.ViewName = "Welcome";
				x.To.Add("janjua.afeef@gmail.com");
			});
		}
 
		public virtual MvcMailMessage PasswordReset(string token, string email)
		{
			ViewBag.Token = token;
			return Populate(x =>
			{
				x.Subject = "Password Reset";
				x.ViewName = "PasswordReset";
				x.To.Add(email);
			});
		}
 
		public virtual MvcMailMessage VerifyAccount()
		{
			//ViewBag.Data = someObject;
			return Populate(x =>
			{
				x.Subject = "VerifyAccount";
				x.ViewName = "VerifyAccount";
				x.To.Add("some-email@example.com");
			});
		}

        public virtual MvcMailMessage ActivateAccount(string token, string email)
        {
            ViewBag.Token = token;
            return Populate(x =>
            {
                x.Subject = "Account Activation";
                x.ViewName = "ActivateAccount";
                x.To.Add(email);
            });
        }
 	}
}