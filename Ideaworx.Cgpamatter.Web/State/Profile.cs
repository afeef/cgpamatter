﻿using Ideaworx.Cgpamatter.Entities.Security;

namespace Ideaworx.Cgpamatter.Web.State
{
    public class Profile
    {
        public Profile(User user)
        {
            this.Id = user.Key;
            this.UserName = user.UserName;

            if (user.Roles.Count > 0)
                this.Role = user.Roles[0].RoleName;

            this.Email = user.Email;
        }

        public long Id { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }

        public User ToUser()
        {
            return new User
            {
                Key = this.Id
            };
        }
    }
}
