﻿using System;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Common.Configuration;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Web.Extensions;
using Ideaworx.Cgpamatter.Web.State;

namespace Ideaworx.Cgpamatter.Web.UI
{
    public class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        #region Initialization

        public BaseViewPage()
        {            
        }

        protected override void InitializePage()
        {
            this.Session = new SessionWrapper(this.ViewContext.HttpContext.Session);
            this.Cache = new CacheWrapper(this.ViewContext.HttpContext.Cache);

            base.InitializePage();
        }

        #endregion

        #region Properties

        public new SessionWrapper Session { get; set; }
        public new CacheWrapper Cache { get; set; }
        
        public new User User
        {
            get
            {
                return Session.User; //Session.User.UserName = base.User.Identity.Name;

            }
        }

        public SiteConfiguration Config { get { return SiteConfiguration.Current; } }

        /// <summary>
        /// Current page index (ViewData["Page"])
        /// </summary>
        public int PageIndex
        {
            get
            {
                if (ViewData.ContainsKey("Page"))
                    return Convert.ToInt32(ViewData["Page"]);
                return 0;
            }
        }

        #region Domain
        /// <summary>
        /// Gets the application current domain (Host) including Protocol and delimiter. Example: http://www.contoso.com (without slash).
        /// </summary>
        public string Domain
        {
            get
            {
                if (this.ViewContext == null)
                    return "http://www.hpscloud.com";

                return this.ViewContext.HttpContext.Request.Url.Scheme + Uri.SchemeDelimiter + this.ViewContext.HttpContext.Request.Url.Host;
            }
        }
        #endregion

        /// <summary>
        /// Gets the current action name
        /// </summary>
        public string ActionName { get { return ViewContext.RouteData.Values["action"].ToString(); } }

        /// <summary>
        /// Determines if it is the current actionName
        /// </summary>
        /// <param name="actionName"></param>
        /// <returns></returns>
        public bool IsAction(string actionName)
        {
            return this.ActionName.ToUpper() == actionName.ToUpper();
        }
        #endregion

        #region Methods

        protected ViewDataDictionary CreateViewData(object values)
        {
            return ViewDataExtensions.CreateViewData(values);
        }

        public override void Execute()
        {

        }

        #endregion        
    }

    public class BaseViewPage : BaseViewPage<object>
    {

    }
}
