﻿using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer
{
    public class CategoryGridModel : GridModel<Category>
    {
        public CategoryGridModel()
        {
            this.Column.For(x => x.CategoryName);            
            //this.Column.For(x => x.LoweredCategoryName);
            this.Column.For(x => x.Description);
            
            //Column.Custom(x => GetCityLink(x.BlockDetail)).Named("BlockDetail").SortColumnName("BlockDetail");            
            this.Column.For(x => x.DateCreated).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified).Named("Modified").SortColumnName("DateModified");
            this.Column.For(x => x.IsActive);
            //Column.For(x => x.Fax);
            //Column.For(x => x.AccessUserId);
            //Column.For(x => x.SupportRepresentative).Named("Support Rep");

            RenderUsing(new HtmlTableGridThemedRenderer<Category>());
        }        
    }
}
