﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;

namespace Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer
{
    public class CourseDetailViewModel
    {
        public long CategoryId { get; set; }
        public IList<Syllabus> SyllabusList { get; set; }
        public Course Course { get; set; }

        public CourseDetailViewModel()
        {

        }

        public CourseDetailViewModel( Course course)
        {            
            this.Course = course;
        }
    }
}
