﻿using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer
{
    public class CourseGridModel : GridModel<Course>
    {
        public CourseGridModel()
        {
            this.Column.For(x => x.CourseCode);            
            this.Column.For(x => x.CourseName);
            this.Column.For(x => x.CourseLevel);
            
            //Column.Custom(x => GetCityLink(x.BlockDetail)).Named("BlockDetail").SortColumnName("BlockDetail");            
            this.Column.For(x => x.DateCreated).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified).Named("Modified").SortColumnName("DateModified");
            this.Column.For(x => x.IsActive);
            //Column.For(x => x.Fax);
            //Column.For(x => x.AccessUserId);
            //Column.For(x => x.SupportRepresentative).Named("Support Rep");

            RenderUsing(new HtmlTableGridThemedRenderer<Course>());
        }        
    }
}
