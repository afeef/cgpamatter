﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;

namespace Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer
{
    public class CourseViewModel
	{
        public long CategoryId { get; set; }
        public SelectList Categories { get; set; }
        public Course Course { get; set; }

        public CourseViewModel()
        {

        }        

        public CourseViewModel(IEnumerable<Category> categories, Course course)
        {
            this.Categories = new SelectList(categories, "Key", "CategoryName", this.CategoryId);
            this.Course = course;
            
            if (course.Category != null) 
                this.CategoryId = course.Category.Key;
        }

	}
}