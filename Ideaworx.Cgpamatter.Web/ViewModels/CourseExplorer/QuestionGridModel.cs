﻿using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer
{
    public class QuestionGridModel : GridModel<Question>
    {
        public QuestionGridModel()
        {
            this.Column.For(x => x.Text);                        
            this.Column.For(x => x.Image);
            this.Column.For(x => x.VideoLink);
            
            //Column.Custom(x => GetCityLink(x.BlockDetail)).Named("BlockDetail").SortColumnName("BlockDetail");            
            this.Column.For(x => x.DateCreated).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified).Named("Modified").SortColumnName("DateModified");
            this.Column.For(x => x.IsActive);
            //Column.For(x => x.Fax);
            //Column.For(x => x.AccessUserId);
            //Column.For(x => x.SupportRepresentative).Named("Support Rep");

            RenderUsing(new HtmlTableGridThemedRenderer<Question>());
        }        
    }
}
