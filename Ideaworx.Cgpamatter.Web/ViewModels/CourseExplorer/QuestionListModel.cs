﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;

namespace Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer
{
    public class QuestionListModel
    {
        public long SyllabusId { get; set; }
        public long CourseId { get; set; }

        public SelectList Courses { get; set; }
        public SelectList SyllabusList { get; set; }

        public PagedViewModel<Question> PagedViewModel { get; set; }

        public QuestionListModel()
        {

        }

        public QuestionListModel(PagedViewModel<Question> pagedViewModel, IEnumerable<Course> courses, IEnumerable<Syllabus> syllabusList, Question question)
        {
            this.PagedViewModel = pagedViewModel;

            if (question.Syllabus == null || question.Syllabus.Course == null)
                return;

            this.SyllabusId = question.Syllabus.Key;
            this.CourseId = question.Syllabus.Course.Key;

            this.Courses = new SelectList(courses, "Key", "CourseName", this.CourseId);
            this.SyllabusList = new SelectList(syllabusList, "Key", "SyllabusText", this.SyllabusId);
        }
    }
}