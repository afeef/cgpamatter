﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;

namespace Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer
{
    public class QuestionModel
    {
        public long CourseId { get; set; }
        public long SyllabusId { get; set; }
        public long QuestionTypeId { get; set; }

        public Question Question { get; set; }
        public SelectList QuestionTypes { get; set; }
        public SelectList SyllabusList { get; set; }
        public IList<Answer> Answers { get; set; }

        public QuestionModel()
        {

        }

        public QuestionModel(IEnumerable<QuestionType> questionTypes, IEnumerable<Syllabus> syllabusList, Question question, IList<Answer> answers )
        {
            this.QuestionTypes = new SelectList(questionTypes, "Key", "Name", this.QuestionTypeId);
            this.SyllabusList = new SelectList(syllabusList, "Key", "SyllabusText", this.SyllabusId);
            this.Answers = answers;
            this.Question = question;

            if (this.Question.Syllabus != null)
                this.SyllabusId = this.Question.Syllabus.Key;

            if (this.Question.QuestionType != null)
                this.QuestionTypeId = this.Question.QuestionType.Key;
        }

        public QuestionModel(IEnumerable<QuestionType> questionTypes, IEnumerable<Syllabus> syllabusList, Question question)
        {
            this.QuestionTypes = new SelectList(questionTypes, "Key", "Name", this.QuestionTypeId);
            this.SyllabusList = new SelectList(syllabusList, "Key", "SyllabusText", this.SyllabusId);            
            this.Question = question;
            this.Answers = question.Answers;

            if (this.Question.Syllabus != null)
                this.SyllabusId = this.Question.Syllabus.Key;

            if (this.Question.QuestionType != null)
                this.QuestionTypeId = this.Question.QuestionType.Key;
        }
    }
}