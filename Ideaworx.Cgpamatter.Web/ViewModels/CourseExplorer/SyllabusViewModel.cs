﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Entities.CourseExplorer;

namespace Ideaworx.Cgpamatter.Web.ViewModels.CourseExplorer
{
    public class SyllabusViewModel
	{
        public long CourseId { get; set; }
        public SelectList Courses { get; set; }
		public Syllabus Syllabus { get; set; }		

        public SyllabusViewModel()
        {

        }

        public SyllabusViewModel(IEnumerable<Course> courses, Syllabus syllabus)
        {
            if (syllabus.Course != null)
                this.CourseId = syllabus.Course.Key;

            this.Courses = new SelectList(courses, "Key", "CourseName", this.CourseId);
            this.Syllabus = syllabus;
        }

        public SyllabusViewModel(IEnumerable<Course> courses, Syllabus syllabus, long courseId)
        {
            this.CourseId = courseId;
            this.Courses = new SelectList(courses, "Key", "CourseName", this.CourseId);
            this.Syllabus = syllabus;

            if (syllabus.Course != null)
                this.CourseId = syllabus.Course.Key;
        }

	}
}