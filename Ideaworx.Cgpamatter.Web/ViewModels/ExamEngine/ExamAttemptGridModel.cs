﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ideaworx.Cgpamatter.Entities.ExamEngine;
using Ideaworx.Cgpamatter.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Web.ViewModels.ExamEngine
{
    public class ExamAttemptGridModel: GridModel<ExamAttempt>
    {
        public ExamAttemptGridModel()
        {
            this.Column.For(x => x.User.Key);
            this.Column.For(x => x.User.FullName);
            this.Column.For(x => x.Exam.ExamName);
            this.Column.For(x => x.IsComplete);                       
            this.Column.For(x => x.DateCreated).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified).Named("Modified").SortColumnName("DateModified");
            this.Column.For(x => x.IsActive);

            this.RenderUsing(new HtmlTableGridThemedRenderer<ExamAttempt>());
        }      
    }
}
