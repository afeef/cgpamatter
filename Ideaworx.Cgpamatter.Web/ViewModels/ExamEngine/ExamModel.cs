﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ideaworx.Cgpamatter.Entities.ExamEngine;

namespace Ideaworx.Cgpamatter.Web.ViewModels.ExamEngine
{
    public class ExamModel : BaseViewModel
    {
        public Exam Exam { get; set; }
        public long? CourseId { get; set; }
    }
}
