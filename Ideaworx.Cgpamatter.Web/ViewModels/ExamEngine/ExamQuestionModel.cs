﻿using Ideaworx.Cgpamatter.Entities.CourseExplorer;
using Ideaworx.Cgpamatter.Entities.ExamEngine;

namespace Ideaworx.Cgpamatter.Web.ViewModels.ExamEngine
{
    public class ExamQuestionModel : BaseViewModel
    {
        public Question Question { get; set; }
        public long QuestionId { get; set; }
        public ExamAttempt ExamAttempt { get; set; }
        public int SelectedAnswer { get; set; }
        public int QuestionIndex { get; set; }
        public int TotalQuestions { get; set; }

        public ExamQuestionModel()
        {

        }
    }
}
