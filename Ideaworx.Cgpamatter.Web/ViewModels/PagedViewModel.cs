﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Ideaworx.Cgpamatter.Web.Extensions;
using MvcContrib.Pagination;
using MvcContrib.UI.Grid;
using MvcContrib.Sorting;

namespace Ideaworx.Cgpamatter.Web.ViewModels
{
    public class PagedViewModel<T>
    {
        public ViewDataDictionary ViewData { get; set; }
        public IQueryable<T> Query { get; set; }
        public GridSortOptions GridSortOptions { get; set; }
        public string DefaultSortColumn { get; set; }
        public IPagination<T> PagedList { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public int Count { get; set; }

        public PagedViewModel<T> AddFilter(Expression<Func<T, bool>> predicate)
        {
            Query = Query.Where(predicate);
            return this;
        }

        public PagedViewModel<T> AddFilter<TValue>(string key, TValue value, Expression<Func<T, bool>> predicate)
        {
            ProcessQuery(value, predicate);
            ViewData[key] = value;
            return this;
        }

        public PagedViewModel<T> AddFilter<TValue>(string keyField, object value, Expression<Func<T, bool>> predicate,
            IQueryable<TValue> query, string textField)
        {
            ProcessQuery(value, predicate);
            var selectList = query.ToSelectList(keyField, textField, value);
            ViewData[keyField] = selectList;
            return this;
        }

        private void ProcessQuery<TValue>(TValue value, Expression<Func<T, bool>> predicate)
        {
            if (value == null) 
                return;

            if (typeof (TValue) == typeof (string))
                if (string.IsNullOrWhiteSpace(value as string)) 
                    return;

            Query = Query.Where(predicate);
        }

        public PagedViewModel<T> Setup()
        {            
           
            var column = this.GridSortOptions.Property<T>();
            this.Count = Query.Count();

            var startRowIndex = this.PageSize * (this.Page - 1);
            var query = Query.OrderBy(column, GridSortOptions.Direction)
                .Skip(startRowIndex ?? 0)
                .Take(this.PageSize ?? 10);

            PagedList = new CustomPagination<T>(query, Page ?? 1, PageSize ?? 10, this.Count);

            return this;
        }
    }
}
