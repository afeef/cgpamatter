﻿using System.Collections.Generic;
using Ideaworx.Cgpamatter.Entities.Security;

namespace Ideaworx.Cgpamatter.Web.ViewModels.Security
{
	public class IndexViewModel
	{
		public string Search { get; set; }
		public IList<User> Users { get; set; }
		public IList<Role> Roles { get; set; }
		public bool IsRolesEnabled { get; set; }
	}
}