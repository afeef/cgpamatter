﻿using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Web.ViewModels.Security
{
    public class PermissionGridModel : GridModel<Permission>
    {
        public PermissionGridModel()
        {
            this.Column.For(x => x.PermissionName);            
            this.Column.For(x => x.PermissionDescription);
            this.Column.For(x => x.PermissionFlag);
            
            //Column.Custom(x => GetCityLink(x.BlockDetail)).Named("BlockDetail").SortColumnName("BlockDetail");            
            this.Column.For(x => x.DateCreated).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified).Named("Modified").SortColumnName("DateModified");
            this.Column.For(x => x.IsActive);
            //Column.For(x => x.Fax);
            //Column.For(x => x.AccessUserId);
            //Column.For(x => x.SupportRepresentative).Named("Support Rep");

            RenderUsing(new HtmlTableGridThemedRenderer<Permission>());
        }        
    }
}
