﻿using System.Collections.Generic;
using Ideaworx.Cgpamatter.Entities.Security;

namespace Ideaworx.Cgpamatter.Web.ViewModels.Security
{
	public class RoleViewModel
	{
		public string Role { get; set; }
		public IDictionary<string, User> Users { get; set; }
	}
}