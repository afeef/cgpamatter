﻿using System;
using Ideaworx.Cgpamatter.Entities.Security;
using Ideaworx.Cgpamatter.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaworx.Cgpamatter.Web.ViewModels.Security
{
    public class UserGridModel : GridModel<User>
    {
        public UserGridModel()
        {
            this.Column.For(x => x.UserName);
            this.Column.For(x => OfflineFor(x.LastActivityDate)).Named("Offline for").Sortable(false);
            this.Column.For(x => x.Email);
            this.Column.For(x => x.FirstName);
            this.Column.For(x => x.LastName);
            //Column.Custom(x => GetCityLink(x.BlockDetail)).Named("BlockDetail").SortColumnName("BlockDetail");            
            //this.Column.For(x => x.DateCreated.ToShortDateString()).Named("Created").SortColumnName("DateCreated");
            //this.Column.For(x => x.DateModified.ToShortDateString()).Named("Modified").SortColumnName("DateModified");
            this.Column.For(x => x.IsActive);
            //Column.For(x => x.Fax);
            //Column.For(x => x.AccessUserId);
            //Column.For(x => x.SupportRepresentative).Named("Support Rep");

            RenderUsing(new HtmlTableGridThemedRenderer<User>());
        }

        private static object OfflineFor(DateTime? lastActivityDate)
        {
            if (lastActivityDate == null)
                return null;

            var offlineSince = (DateTime.Now - Convert.ToDateTime(lastActivityDate));

            if (offlineSince.TotalSeconds <= 60)
                return "1 minute.";

            if (offlineSince.TotalMinutes < 60)
                return string.Format("{0} minutes.", Math.Floor(offlineSince.TotalMinutes));

            if (offlineSince.TotalMinutes < 120)
                return "1 hour";

            if (offlineSince.TotalHours < 24)
                return string.Format("{0} hours.", Math.Floor(offlineSince.TotalHours));

            return offlineSince.TotalHours < 48 
                ? "1 day." 
                : string.Format("{0} days.", Math.Floor(offlineSince.TotalDays));
        }
    }
}
