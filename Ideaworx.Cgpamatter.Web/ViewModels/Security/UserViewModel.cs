﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Ideaworx.Cgpamatter.Entities.Security;

namespace Ideaworx.Cgpamatter.Web.ViewModels.Security
{
    public class UserViewModel
    {                
        public User User { get; set; }                

        [Display(Name = "Initial Roles")]
        public IDictionary<string, bool> InitialRoles { get; set; }

        public UserViewModel()
        {

        }

        public UserViewModel(User user, IDictionary<string, bool> initialRoles)
        {
            this.User = user;            
            this.InitialRoles = initialRoles;
        }        
    }
}
